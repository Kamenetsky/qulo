
using UnityEngine;

internal sealed class UserManager : MonoBehaviour
{
    private UserData _currentUserData;

    public void SaveUserData(UserData userData)
    {
        _currentUserData = userData;
        // Optionally, you can save the user data to PlayerPrefs or any other local storage
    }

    public UserData GetUserData() => _currentUserData;
}