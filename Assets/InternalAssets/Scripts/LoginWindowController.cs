
using Core;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace UI
{
    internal sealed class LoginWindowController : UIWindow
    {
        [Header("References")]
        [SerializeField] private InputField _emailInputField;
        [SerializeField] private InputField _passwordInputField;
        [SerializeField] private Button _loginButton;
        [SerializeField] private Button _goToRegistrationButton;
        [SerializeField] private TextMeshProUGUI _errorText;

        private IUserService _userService;
        private UserManager _userManager;

        private void Start()
        {
            _userService = Locator.Instance.Get<IUserService>();
            _userManager = Locator.Instance.Get<UserManager>();

            _loginButton.onClick.AddListener(HandleLoginButtonClick);
            _goToRegistrationButton.onClick.AddListener(HandleGoToRegistrationButtonClick);
        }

        private void OnDestroy()
        {
            _loginButton.onClick.RemoveListener(HandleLoginButtonClick);
            _goToRegistrationButton.onClick.RemoveListener(HandleGoToRegistrationButtonClick);
        }

        private async void HandleLoginButtonClick()
        {
            var email = _emailInputField.text;
            var password = _passwordInputField.text;

            if (!InputValidator.ValidateEmail(email))
            {
                _errorText.text = "Invalid email address.";
                return;
            }

            if (!InputValidator.ValidatePassword(password))
            {
                _errorText.text = "Invalid password. Password must be at least 8 characters, contain one uppercase, one lowercase, and one number.";
                return;
            }

            var userData = await _userService.LoginAsync(email, password);
            if (userData != null)
            {
                _userManager.SaveUserData(userData);
                // Perform actions after successful login, e.g., load next scene
                Debug.Log("Logged in successfully");
            }
            else
            {
                _errorText.text = "Login failed";
            }
        }

        private void HandleGoToRegistrationButtonClick()
        {
            // Open the registration window and close the login window
        }
    }
}