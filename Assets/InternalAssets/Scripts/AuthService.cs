
using Cysharp.Threading.Tasks;
using UnityEngine;
using UnityEngine.Networking;

internal sealed class AuthService : MonoBehaviour, IAuthService
{
    private const string _serverUrl = "https://your-server-url.com/api";

    public async UniTask<ProfileData> GetProfileDataAsync(string userId)
    {
        var profileDataEndpoint = $"{_serverUrl}/profile/{userId}";

        using var request = UnityWebRequest.Get(profileDataEndpoint);
        await request.SendWebRequest();

        if (request.result == UnityWebRequest.Result.Success)
        {
            var response = request.downloadHandler.text;
            // Deserialize the response to ProfileData object
            var profileData = JsonUtility.FromJson<ProfileData>(response);
            return profileData;
        }
        else
        {
            Debug.LogError($"GetProfileData failed: {request.error}");
            return null;
        }
    }

    public async UniTask<bool> SavePlayerProgressAsync(string userId, int hardCurrency, int softCurrency)
    {
        var saveProgressEndpoint = $"{_serverUrl}/progress/{userId}";
        var form = new WWWForm();
        form.AddField("hardCurrency", hardCurrency);
        form.AddField("softCurrency", softCurrency);

        using var request = UnityWebRequest.Post(saveProgressEndpoint, form);
        await request.SendWebRequest();

        if (request.result == UnityWebRequest.Result.Success)
        {
            return true;
        }
        else
        {
            Debug.LogError($"SavePlayerProgress failed: {request.error}");
            return false;
        }
    }
}