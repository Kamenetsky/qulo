﻿
using Core;
using UnityEngine;
using UnityEngine.UI;

namespace UI
{
    internal sealed class RegistrationWindowController : UIWindow
    {
        [Header("References")]
        [SerializeField] private InputField _emailInputField;
        [SerializeField] private InputField _passwordInputField;
        [SerializeField] private InputField _confirmPasswordInputField;
        [SerializeField] private Button _registerButton;
        [SerializeField] private Button _goToLoginButton;
        [SerializeField] private Text _errorText;

        private IUserService _userService;
        private UserManager _userManager;

        private void Start()
        {
            _userService = Locator.Instance.Get<IUserService>();
            _userManager = Locator.Instance.Get<UserManager>();

            _registerButton.onClick.AddListener(HandleRegisterButtonClick);
            _goToLoginButton.onClick.AddListener(HandleGoToLoginButtonClick);
        }

        private void OnDestroy()
        {
            _registerButton.onClick.RemoveListener(HandleRegisterButtonClick);
            _goToLoginButton.onClick.RemoveListener(HandleGoToLoginButtonClick);
        }

        private async void HandleRegisterButtonClick()
        {
            var email = _emailInputField.text;
            var password = _passwordInputField.text;
            var confirmPassword = _confirmPasswordInputField.text;

            if (!InputValidator.ValidateEmail(email))
            {
                _errorText.text = "Invalid email address.";
                return;
            }

            if (!InputValidator.ValidatePassword(password))
            {
                _errorText.text = "Invalid password. Password must be at least 8 characters, contain one uppercase, one lowercase, and one number.";
                return;
            }

            if (password != confirmPassword)
            {
                _errorText.text = "Passwords do not match.";
                return;
            }

            var userData = await _userService.RegisterAsync(email, password);
            if (userData != null)
            {
                _userManager.SaveUserData(userData);
                // Perform actions after successful registration, e.g., load next scene
                Debug.Log("Registered successfully");
            }
            else
            {
                _errorText.text = "Registration failed";
            }
        }

        private void HandleGoToLoginButtonClick()
        {
            // Open the login window and close the registration window
        }
    }
}