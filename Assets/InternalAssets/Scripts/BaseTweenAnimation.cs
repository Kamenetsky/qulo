﻿
using DG.Tweening;
using UnityEngine;

namespace Utilities.TweenAnimations
{
    public abstract class BaseTweenAnimation : MonoBehaviour
    {
        [SerializeField] private string _animationName;
        [SerializeField] private float _duration;
        [SerializeField] private Ease _easeType;
        
        public AnimationCurve customCurve;

        public string AnimationName => _animationName;
        public float Duration => _duration;
        public Ease EaseType => _easeType;

        public abstract void Play();
        
        public virtual Tween CreateTween(GameObject target)
        {
            Tween tween = CreateTweenImpl(target);
            if (tween != null)
            {
                //tween.SetDelay(_delay);
                //tween.SetLoops(_loopCount, _loopType);
            }
            
            return tween;
        }

        protected abstract Tween CreateTweenImpl(GameObject target);
        
        public void SetDuration(float duration)
        {
            _duration = duration;
        }

        public void SetAnimationCurve(AnimationCurve curve)
        {
            customCurve = curve;
        }
    }
}