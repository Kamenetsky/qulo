
using Cysharp.Threading.Tasks;

namespace UI
{
    internal interface IUserService
    {
        public UniTask<UserData> LoginAsync(string email, string password);
        public UniTask<UserData> RegisterAsync(string email, string password);
    }
}