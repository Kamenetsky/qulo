
using DG.Tweening;
using UnityEngine;

namespace Utilities.TweenAnimations
{
    internal sealed class RotateTweenAnimation : BaseTweenAnimation
    {
        [SerializeField] private Vector3 _startRotation = Vector3.zero;
        [SerializeField] private Vector3 _endRotation = new Vector3(0, 360, 0);

        public override void Play()
        {
            
        }

        public override Tween CreateTween(GameObject target)
        {
            return null; /*target.transform.DORotate(_endRotation, Duration).SetEase(AnimationCurve).From(_startRotation).SetRelative();*/
        }

        protected override Tween CreateTweenImpl(GameObject target)
        {
            throw new System.NotImplementedException();
        }
    }
}