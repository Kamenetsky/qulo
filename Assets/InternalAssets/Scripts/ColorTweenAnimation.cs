
using DG.Tweening;
using UnityEngine;

namespace Utilities.TweenAnimations
{
    internal sealed class ColorTweenAnimation : BaseTweenAnimation
    {
        [SerializeField] private Color _startColor = Color.white;
        [SerializeField] private Color _endColor = Color.red;

        public override void Play()
        {
            
        }

        public override Tween CreateTween(GameObject target)
        {
            Renderer renderer = target.GetComponent<Renderer>();
            if (renderer == null)
            {
                Debug.LogWarning("Не удалось найти Renderer на объекте для анимации цвета");
                return null;
            }

            return null; /*renderer.material.DOColor(_endColor, Duration).SetEase(AnimationCurve).From(_startColor);*/
        }

        protected override Tween CreateTweenImpl(GameObject target)
        {
            throw new System.NotImplementedException();
        }
    }
}