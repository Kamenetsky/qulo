
using UnityEngine;

namespace UI
{
    [RequireComponent(typeof(Canvas))]
    [DisallowMultipleComponent]
    internal abstract class UIElement : MonoBehaviour
    {
        private Canvas _ownCanvas;

        protected virtual void Awake() => _ownCanvas = GetComponent<Canvas>();

        public virtual void Show() => _ownCanvas.enabled = true;

        public virtual void Hide() => _ownCanvas.enabled = false;
        
        public abstract void Refresh();
    }
}