
using Cysharp.Threading.Tasks;
using UnityEngine;
using UnityEngine.Networking;

namespace UI
{
    internal sealed class UserService : IUserService
    {
        private const string _serverUrl = "https://your-server-url.com/api";

        public async UniTask<UserData> LoginAsync(string email, string password)
        {
            var loginEndpoint = $"{_serverUrl}/login";
            var form = new WWWForm();
            form.AddField("email", email);
            form.AddField("password", password);

            using var request = UnityWebRequest.Post(loginEndpoint, form);
            await request.SendWebRequest();

            if (request.result == UnityWebRequest.Result.Success)
            {
                var response = request.downloadHandler.text;
                // Deserialize the response to UserData object
                var userData = JsonUtility.FromJson<UserData>(response);
                return userData;
            }
            else
            {
                Debug.LogError($"Login failed: {request.error}");
                return null;
            }
        }

        public async UniTask<UserData> RegisterAsync(string email, string password)
        {
            var registerEndpoint = $"{_serverUrl}/register";
            var form = new WWWForm();
            form.AddField("email", email);
            form.AddField("password", password);

            using var request = UnityWebRequest.Post(registerEndpoint, form);
            await request.SendWebRequest();

            if (request.result == UnityWebRequest.Result.Success)
            {
                var response = request.downloadHandler.text;
                // Deserialize the response to UserData object
                var userData = JsonUtility.FromJson<UserData>(response);
                return userData;
            }
            else
            {
                Debug.LogError($"Registration failed: {request.error}");
                return null;
            }
        }
    }
}