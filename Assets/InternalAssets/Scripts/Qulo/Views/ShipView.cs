
using System.Collections.Generic;
using System.Linq;
using Cysharp.Threading.Tasks;
using DG.Tweening;
using Qulo.Handlers;
using UnityEngine;

namespace Qulo.Views
{
    public class ShipView : MonoBehaviour
    {
        private const float Speed = .05f;
        
        [SerializeField] private List<SpriteRenderer> lights = new();

        private void Start()
        {
            foreach (var lght in lights)
            {
                lght.DOFade(0f, 1f).OnComplete(() => lght.DOFade(1f, 1f)).SetLoops(-1);
            }
        }

        public async UniTask Warp(List<Vector3> path, Vector3 finalPos)
        {
            var distance = Vector2.Distance(path[0], finalPos);
            var duration = distance / Speed;
            if (path.Count > 2)
            {
                transform.DOPath(path.ToArray(), duration * .001f, PathType.Linear, PathMode.TopDown2D)
                    .SetLookAt(0.5f)
                    .SetEase(Ease.Linear);
            }
            else
            {
                transform.right = finalPos - transform.position;
                transform.DOMove(finalPos, duration * .001f)
                    .SetEase(Ease.Linear);
            }

            await UniTask.Delay((int)duration);
        }
    }
}
