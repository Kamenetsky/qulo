using Qulo.Data;
using UniRx;
using UnityEngine;
using UnityEngine.EventSystems;

namespace Qulo.Views
{
    public class OrbitObjectView : MonoBehaviour, IPointerClickHandler
    {
        public struct Context
        {
            public OrbitObject OrbitObject;
            public OrbitView ParentOrbit;
            public PlanetarySystemView ParentPlanetarySystem;
            public SpaceConfigSO SpaceConfig;
            public ReactiveCommand<OrbitObjectView> OnObjectClick;
        }

        [SerializeField] private SpriteRenderer icon;
        [SerializeField] private SpriteRenderer[] iconAdditional;
        
        private Context _ctx;

        public OrbitObject OrbitObject => _ctx.OrbitObject;

        public void SetContext(Context ctx)
        {
            _ctx = ctx;
            
            InitView();
        }

        private void InitView()
        {
            for (int i = 0; i < transform.childCount; ++i)
            {
                var child = transform.GetChild(i);
                child.gameObject.SetActive(false);
            }
            
            icon.sprite = _ctx.SpaceConfig.spaceObjectSprites[_ctx.OrbitObject.objectType];
            if (_ctx.OrbitObject.objectType == OrbitObjectType.WarpPoint)
            {
                Observable.EveryUpdate().Subscribe(_ => transform.Rotate(Vector3.forward, -10 * Time.deltaTime)).AddTo(this);
            }
            else if (_ctx.OrbitObject.objectType == OrbitObjectType.Planet)
            {
                transform.localScale = new Vector3(_ctx.OrbitObject.size, _ctx.OrbitObject.size, 1f);
                for (int i = 0; i < transform.childCount; ++i)
                {
                    var child = transform.GetChild(i);
                    child.gameObject.SetActive(true);
                    child.localScale = Vector3.one / _ctx.OrbitObject.size;
                }

                if (_ctx.OrbitObject.additionalParams["param1"])
                {
                    iconAdditional[0].sprite = _ctx.SpaceConfig.planetAdditionalSprite;
                }

                if (_ctx.OrbitObject.additionalParams["param2"])
                {
                    iconAdditional[1].sprite = _ctx.SpaceConfig.planetAdditionalSprite;
                }

                iconAdditional[1].transform.position = iconAdditional[0].transform.position + 0.25f * Vector3.right;
            }
        }
        
        public void OnPointerClick(PointerEventData eventData)
        {
            _ctx.OnObjectClick?.Execute(this);
        }
    }
}
