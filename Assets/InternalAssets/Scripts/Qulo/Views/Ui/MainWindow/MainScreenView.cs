﻿using System.Collections.Generic;
using UnityEngine;

namespace Qulo.Views.Ui.MainWindow
{
    public class MainScreenView : MonoBehaviour
    {
        [SerializeField] private GameObject backgroundImage;
        [SerializeField] private GameObject backgroundText;
        [SerializeField] private GameObject leftAdventuresPanel;
        [SerializeField] private Sprite normalHorizontalToggleSprite;
        [SerializeField] private Sprite selectedHorizontalToggleSprite;
        [SerializeField] private HorizontalToggleReferences adventuresToggle;
        [SerializeField] private List<HorizontalToggleReferences> togglesComingSoon;
        
        [SerializeField] private Color activeLabelColor = Color.black;
        [SerializeField] private Color inactiveLabelColor = Color.gray;
        
        [SerializeField] private Color activeIconColor = Color.black;
        [SerializeField] private Color inactiveIconColor = Color.white;

        [SerializeField] private GameObject bgFake;

        private readonly Vector2 _normalSize = new(100f, 40f);
        private readonly Vector2 _selectedSize = new(272f, 80f);
        
        private void Awake()
        {
            SubscribeAdventuresVerticalToggles();
            SubscribeComingSoonToggles();
        }

        private void OnDestroy()
        {
            UnsubscribeAdventuresVerticalToggles();
            UnsubscribeComingSoonToggles();
        }
        
        private void Start()
        {
            ShowPlanetarySystemAtStart();
        }

        private void ShowPlanetarySystemAtStart()
        {
            leftAdventuresPanel.SetActive(true);
            adventuresToggle.toggleComponent.isOn = true;
        }

        private void SubscribeAdventuresVerticalToggles()
        {
            //map Toggle
            adventuresToggle.verticalToggle[0].toggleComponent.onValueChanged.AddListener(OnAdventuresMapVerticalToggleValueChanged);
        }
        
        private void UnsubscribeAdventuresVerticalToggles()
        {
            //map Toggle
            adventuresToggle.verticalToggle[0].toggleComponent.onValueChanged.RemoveListener(OnAdventuresMapVerticalToggleValueChanged);
        }
        
        private void SubscribeComingSoonToggles()
        {
            foreach (var toggle in togglesComingSoon)
            {
                toggle.toggleComponent.onValueChanged.AddListener(_ => OnToggleComingSoonValueChanged(toggle));
            }
        }
        
        private void UnsubscribeComingSoonToggles()
        {
            foreach (var toggle in togglesComingSoon)
            {
                toggle.toggleComponent.onValueChanged.RemoveAllListeners();
            }
        }

        private void OnAdventuresMapVerticalToggleValueChanged(bool isOn)
        {
            backgroundImage.SetActive(!isOn);
        }

        private void OnToggleComingSoonValueChanged(HorizontalToggleReferences toggle)
        {
            if (!toggle.toggleComponent.isOn)
                return;

            leftAdventuresPanel.SetActive(toggle == adventuresToggle);
            EnableBackgroundElements(toggle != adventuresToggle);

            DeactivateOtherToggles(toggle);
            ActivateSelectedToggle(toggle);
        }

        private void DeactivateOtherToggles(HorizontalToggleReferences activeToggle)
        {
            foreach (var toggle in togglesComingSoon)
            {
                if (toggle == activeToggle)
                    continue;

                toggle.toggleComponent.isOn = false;
                toggle.toggleRect.sizeDelta = _normalSize;
                toggle.label.color = inactiveLabelColor;
                toggle.icon.color = inactiveIconColor;
                toggle.toggleComponent.image.sprite = normalHorizontalToggleSprite;
                toggle.toggleComponent.image.color = new Color(240f/255f, 240f/255f, 240f/255f);
            }
        }

        private void EnableBackgroundElements(bool value)
        {
            backgroundImage.SetActive(value);
            backgroundText.SetActive(value);
        }

        private void ActivateSelectedToggle(HorizontalToggleReferences activeToggle)
        {
            activeToggle.toggleRect.sizeDelta = _selectedSize;
            activeToggle.toggleComponent.image.sprite = selectedHorizontalToggleSprite;
            activeToggle.toggleComponent.image.color = Color.white;
            activeToggle.icon.color = activeIconColor;
            activeToggle.label.color = activeLabelColor;
        }

        public void HideFakeBack()
        {
            bgFake.SetActive(!bgFake.activeSelf);
            EnableBackgroundElements(!bgFake.activeSelf);
        }
    }
}