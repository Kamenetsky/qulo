﻿using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Qulo.Views.Ui.MainWindow
{
    public class HorizontalToggleReferences : MonoBehaviour
    {
        public RectTransform toggleRect;
        public Toggle toggleComponent;
        public TMP_Text label;
        public Image icon;
        public List<VerticalToggleReferences> verticalToggle;
    } 
}
