﻿using UnityEngine;
using UnityEngine.UI;

namespace Qulo.Views.Ui.MainWindow
{
    public class VerticalToggleReferences : MonoBehaviour
    {
        public Toggle toggleComponent;
        public Image icon;

        public void OnToggleChanged()
        {
            icon.color = toggleComponent.isOn ? Color.black : Color.white;
        }
    }
}