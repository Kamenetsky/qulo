using System.Collections.Generic;
using Qulo.Data;
using UniRx;
using UnityEngine;

namespace Qulo.Views
{
    public class PlanetarySystemView : MonoBehaviour
    {
        public struct Context
        {
            public PlanetarySystem PlanetarySystem;
            public SpaceConfigSO SpaceConfig;
            public ReactiveCommand<OrbitObjectView> OnObjectClick;
        }

        [SerializeField] private OrbitView orbitViewPrefab;

        private Context _ctx;

        private List<OrbitView> _orbitViews = new List<OrbitView>();
        private List<OrbitObjectView> _orbitObjectViews = new List<OrbitObjectView>();

        public List<OrbitObjectView> OrbitObjectViews => _orbitObjectViews;

        public void SetContext(Context ctx)
        {
            _ctx = ctx;

            Init();
        }

        private void Init()
        {
            foreach (var orbit in _ctx.PlanetarySystem.orbits)
            {
                var orbitView = Instantiate(orbitViewPrefab, transform);
                orbitView.gameObject.SetActive(true);
                orbitView.SetContext(new OrbitView.Context
                {
                    Orbit = orbit,
                    ParentPlanetarySystem = this,
                    SpaceConfig = _ctx.SpaceConfig,
                    OnObjectClick = _ctx.OnObjectClick
                });
                
                _orbitViews.Add(orbitView);
                _orbitObjectViews.AddRange(orbitView.OrbitObjectViews);
            }
        }
    }
}
