using System;
using System.Collections.Generic;
using UnityEngine;
using Utilities.Helpers;
using Random = UnityEngine.Random;

namespace Qulo.Views
{
    public class InterstellarPathsView : MonoBehaviour
    {
        private const int stepsCount = 32;
        
        public struct Context
        {
            public Vector2 point1;
            public Vector2 point2;
            public float distortion1;
            public float distortion2;
            public Action<List<Vector3>> OnPathCreated;
        }
        
        [SerializeField] private LineRenderer lineRender;

        private Context _ctx;

        public void SetContext(Context ctx)
        {
            _ctx = ctx;
            
            //lineRender.positionCount = 2;
            //lineRender.SetPosition(0, _ctx.point1);
            //lineRender.SetPosition(1, _ctx.point2);

            lineRender.positionCount = stepsCount;
            Vector2 half = 0.5f * (_ctx.point1 + _ctx.point2);
            Vector2 quarter1 = 0.5f * (_ctx.point1 + half) + Vector2.up * _ctx.distortion1 * 10 + Vector2.left * _ctx.distortion1 * 10;
            Vector2 quarter2 = 0.5f * (_ctx.point2 + half) + Vector2.up * _ctx.distortion2 * 10 + Vector2.left * _ctx.distortion2 * 10;
            List<Vector3> allPositions = new List<Vector3>();
            for (int i = 0; i < stepsCount; i++)
            {
                var t = i / (stepsCount - 1f);
                var position = Bezier.GetPoint(ctx.point1, quarter1, quarter2, ctx.point2, t);
                allPositions.Add(position);
                lineRender.SetPosition(i, position);
            }
            
            _ctx.OnPathCreated?.Invoke(allPositions);
        }
    }
}
