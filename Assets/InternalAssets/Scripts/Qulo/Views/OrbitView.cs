using System.Collections.Generic;
using Qulo.Data;
using UniRx;
using UnityEngine;
using Utilities.Helpers;

namespace Qulo.Views
{
    public class OrbitView : MonoBehaviour
    {
        public struct Context
        {
            public Orbit Orbit;
            public PlanetarySystemView ParentPlanetarySystem;
            public SpaceConfigSO SpaceConfig;
            public ReactiveCommand<OrbitObjectView> OnObjectClick;
        }
        
        [SerializeField] private LineRenderer lineRender;
        [SerializeField] private OrbitObjectView orbitObjectViewPrefab;

        private Context _ctx;
        private List<OrbitObjectView> _orbitObjectViews = new List<OrbitObjectView>();

        public List<OrbitObjectView> OrbitObjectViews => _orbitObjectViews;

        public void SetContext(Context ctx)
        {
            _ctx = ctx;
            
            InitOrbitView();
        }

        private void InitOrbitView()
        {
            DrawCircle();
            foreach (var oo in _ctx.Orbit.orbitObjects)
            {
                var ooView = Instantiate(orbitObjectViewPrefab, transform);
                ooView.gameObject.SetActive(true);
                var ooTransform = ooView.transform;
                ooTransform.localPosition = new Vector3(_ctx.Orbit.radius, 0f, 0f);
                ooView.transform.position = MathHelpers.RotatePointAroundPivot(ooTransform.position, 
                    transform.position, new Vector3(0f, 0f, oo.angle));
                ooView.SetContext(new OrbitObjectView.Context
                {
                    OrbitObject = oo,
                    ParentOrbit = this,
                    ParentPlanetarySystem = _ctx.ParentPlanetarySystem,
                    SpaceConfig = _ctx.SpaceConfig,
                    OnObjectClick = _ctx.OnObjectClick
                });
                _orbitObjectViews.Add(ooView);
            }
        }

        private void DrawCircle() 
        {
            float angle = 0f;
            float angleStep= 0.01f;
            float radius = _ctx.Orbit.radius;
            int size = (int)((1f / angleStep) + 1f);
            lineRender.positionCount = size;
            for (int i = 0; i < size; i++) 
            {
                angle += (2.0f * Mathf.PI * angleStep);
                float x = radius * Mathf.Cos(angle);
                float y = radius * Mathf.Sin(angle);
                lineRender.SetPosition(i, new Vector3(x, y, 0));
            }
        }
    }
}
