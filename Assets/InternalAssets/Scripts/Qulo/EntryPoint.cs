
using System.Threading.Tasks;
using Qulo.Channels;
using Qulo.Data;
using Qulo.GUI;
using Qulo.Handlers;
using Qulo.Net;
using UniRx;
using UnityEngine;
using UnityEngine.SceneManagement;
using Utilities.Reactive;

namespace Qulo
{
    public class EntryPoint : MonoBehaviour
    {
        private readonly AuthorizationChannel _authorizationChannel = new();

        private readonly ReactiveTrigger _onGoToGame = new();

        [SerializeField] private GUIManager guiManager;
        [SerializeField] private NetRestConfigSO netRestConfig;

        private PlayerHandler _playerHandler;
        private AuthorizationHandler _authorizationHandler;
        private INetworkClient<IResponseData, IRequestData> _networkClient;

        private void Awake()
        {
            TaskScheduler.UnobservedTaskException += (_, e) => Debug.LogException(e.Exception);
            
            _networkClient = new NetworkClientSimple(netRestConfig);

            _authorizationChannel.OnRejectAddCredentials.Subscribe(x => Debug.LogError($"Credentials rejected : {x}"))
                .AddTo(this);

            _playerHandler = new PlayerHandler(new PlayerHandler.Context
            {
                GuiManager = guiManager,
                NetworkClient = _networkClient,
                AuthorizationChannel = _authorizationChannel,
            });

            _authorizationHandler = new AuthorizationHandler(new AuthorizationHandler.Context
            {
                GuiManager = guiManager,
                AuthorizationChannel = _authorizationChannel,
                OnGoToGame = _onGoToGame,
                LastSavedEmail = _playerHandler.LastSavedEmail,
            });

            _onGoToGame.Subscribe(LoadQuestsScene).AddTo(this);
        }

        private void LoadQuestsScene()
        {
            SceneManager.LoadScene(1);
        }

        private void OnDestroy()
        {
            _playerHandler.Dispose();
            _authorizationHandler.Dispose();
            _onGoToGame.Dispose();
        }
    }
}
