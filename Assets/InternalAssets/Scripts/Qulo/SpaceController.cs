using Qulo.Data;
using Qulo.GUI;
using Qulo.Handlers;
using Qulo.Net;
using UniRx;
using UnityEngine;
using Qulo.Views;

namespace Qulo
{
    public class SpaceController : MonoBehaviour
    {
        [SerializeField] private PlanetarySystemView planetarySystemPrefab;
        [SerializeField] private InterstellarPathsView pathsViewPrefab;
        [SerializeField] private SpaceConfigSO spaceConfig;
        [SerializeField] private Transform background;
        [SerializeField] private CameraController cameraController;
        [SerializeField] private GUIManager guiManager;

        private readonly INetworkClient<IResponseData, IRequestData> _networkClient = new NetworkClientDummy();
        private SpaceHandler _spaceHandler;

        private ReactiveCommand<OrbitObjectView> _onObjectClick = new();

        private async void Awake()
        {
            var requestData = await _networkClient.MakeRequestAsync(new SpaceRequestData());
            if (requestData == null || requestData.GetResponseType() != ResponseType.Space)
            {
                Debug.LogError("Can not obtain data for quests");
                return;
            }

            var space = requestData as Data.Space;
            _spaceHandler = new SpaceHandler(new SpaceHandler.Context
            {
                Space = space,
                GuiManager = guiManager,
                SpaceConfig = spaceConfig,
                SpaceBackground = background,
                PlanetarySystemPrefab = planetarySystemPrefab,
                PathsViewPrefab = pathsViewPrefab,
                OnObjectClick = _onObjectClick
            });

            cameraController.SetBounds(_spaceHandler.Size);

            _onObjectClick.Subscribe(x => Debug.Log("Click to object" 
                                                    + x.OrbitObject.objectType.ToString())).AddTo(this);
        }
        
        private void OnDestroy()
        {
            _spaceHandler?.Dispose();
        }
    }
}
