using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Cysharp.Threading.Tasks;
using Newtonsoft.Json;
using UnityEngine;

namespace Qulo.Net
{
    public class NetworkClientDummy : INetworkClient<IResponseData, IRequestData>
    {
        private const string PrefsCredsKey = "PlayerCredentials";
        
        private List<LoginRequestData> _playerCredentialsList = new();

        public void Initialize()
        {
            LoadPlayerCredentials(); 
        }

        public async UniTask<IResponseData> MakeRequestAsync(IRequestData data)
        {
            await UniTask.Delay(1); //HACK
            return GetResponseData(data);
        }

        private IResponseData GetResponseData(IRequestData data)
        {
            if (data.GetRequestType() == RequestType.Registration)
            {
                _playerCredentialsList.Add(data as LoginRequestData);
                SavePlayerCredentials();
                return new RegistrationResponseData
                {
                    TokenType = "dummyRegistration",
                    AccessToken = "accessTokenDummyRegistration",
                    RefreshToken = "RefreshTokenDummyRegistration"
                };
            }
            
            if (data.GetRequestType() == RequestType.Login)
            {
                var reqData = data as LoginRequestData;
                if (reqData == null ||
                    !_playerCredentialsList.Exists(x => x.Email == reqData.Email && x.Password == reqData.Password))
                {
                    return new WrongCredentialsResponseData
                    {
                        StatusCode = "552",
                        Message = "Error",
                        Error = "WrongCredentials"
                    };
                }
                
                return new RegistrationResponseData
                {
                    TokenType = "dummyLogin",
                    AccessToken = "accessTokenDummyLogin",
                    RefreshToken = "RefreshTokenDummyLogin"
                };
            }

            if (data.GetRequestType() == RequestType.Space)
            {
                var space = LoadSpace();
                if (space == null)
                {
                    return new BadResponseData
                    {
                        StatusCode = "552",
                        Message = "Error",
                        Error = "WrongCredentials"
                    };
                }

                return space;
            }

            return null;
        }

        private Data.Space LoadSpace()
        {
            var resource = Resources.Load<TextAsset>("Space");
            if (resource == null)
                return null;
            
            return JsonConvert.DeserializeObject<Data.Space>(resource.text);
        }
        
        private void SavePlayerCredentials()
        {
            var json = JsonConvert.SerializeObject(_playerCredentialsList);
            PlayerPrefs.SetString(PrefsCredsKey, json);
            PlayerPrefs.Save();
        }

        private void LoadPlayerCredentials()
        {
            var json = PlayerPrefs.GetString(PrefsCredsKey, String.Empty);
            if (string.IsNullOrEmpty(json))
                return;

            _playerCredentialsList.Clear();
            _playerCredentialsList = JsonConvert.DeserializeObject<List<LoginRequestData>>(json);
        }
    }
}
