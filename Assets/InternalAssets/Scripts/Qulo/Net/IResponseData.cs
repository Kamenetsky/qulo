using System;
using Newtonsoft.Json;

namespace Qulo.Net
{
    public enum ResponseType
    {
        Registration = 1,
        Login = 2,
        Email = 3,
        BadResponse = 4,
        WrongCredentials = 5,
        
        Space = 6
    }
    
    public interface IResponseData
    {
        ResponseType GetResponseType();
    }
    
    [Serializable]
    public class EmailResponseData : IResponseData
    {
        [JsonProperty("token")] public string Token { get; set; }

        public ResponseType GetResponseType()
        {
            return ResponseType.Email;
        }
    }
    
    [Serializable]
    public class RegistrationResponseData : IResponseData
    {
        [JsonProperty("token_type")] public string TokenType { get; set; }
        [JsonProperty("access_token")] public string AccessToken { get; set; }
        [JsonProperty("refresh_token")] public string RefreshToken { get; set; }
        
        public ResponseType GetResponseType()
        {
            return ResponseType.Registration;
        }
    }
    
    [Serializable]
    public class BadResponseData : IResponseData
    {
        [JsonProperty("status_code")] public string StatusCode { get; set; }
        [JsonProperty("message")] public object Message { get; set; }
        [JsonProperty("error")] public string Error { get; set; }

        public virtual ResponseType GetResponseType()
        {
            return ResponseType.BadResponse;
        }
    }
    
    [Serializable]
    public class WrongCredentialsResponseData : BadResponseData
    {
        public override ResponseType GetResponseType()
        {
            return ResponseType.WrongCredentials;
        }
    }
}
