using System;
using Newtonsoft.Json;
using UnityEngine;

namespace Qulo.Net
{
    public enum RequestType
    {
        Registration = 0,
        Login = 1,
        Email = 2,
        Space = 3,
    }
    
    public interface IRequestData
    {
        RequestType GetRequestType();
        string GetString();
        WWWForm GetData();
    }

    public abstract class RequestData : IRequestData
    {
        public abstract RequestType GetRequestType();
        
        public virtual string GetString()
        {
            return JsonConvert.SerializeObject(this);
        }

        public virtual WWWForm GetData()
        {
            return new WWWForm();
        }
    }

    [Serializable]
    public class EmailRequestData : RequestData
    {
        [JsonProperty("email")] public string Email { get; set; }
        
        public override RequestType GetRequestType()
        {
            return RequestType.Email;
        }
        
        public override WWWForm GetData()
        {
            var form = base.GetData();;
            form.AddField("email", Email);
            return form;
        }
    }

    [Serializable]
    public class LoginRequestData : EmailRequestData
    {
        [JsonProperty("password")] public string Password { get; set; }

        public override RequestType GetRequestType()
        {
            return RequestType.Login;
        }
        
        public override WWWForm GetData()
        {
            var form = base.GetData();;
            form.AddField("password", Password);
            return form;
        }
    }
    
    [Serializable]
    public class RegistrationRequestData : LoginRequestData
    {
        [JsonProperty("cookiePolicy")] public bool CookiePolicy { get; set; }
        [JsonProperty("acceptTerms")] public bool AcceptTerms { get; set; }

        public RegistrationRequestData() //test
        {
            Email = "testtet@test.tst";
            Password = "Awertrqrw&_12";
            CookiePolicy = true;
            AcceptTerms = true;
        }

        public override RequestType GetRequestType()
        {
            return RequestType.Registration;
        }
        
        public override WWWForm GetData()
        {
            var form = base.GetData();
            form.AddField("cookiePolicy", CookiePolicy.ToString());
            form.AddField("acceptTerms", AcceptTerms.ToString());
            return form;
        }
    }
    
    [Serializable]
    public class SpaceRequestData : RequestData
    {
        public override RequestType GetRequestType()
        {
            return RequestType.Space;
        }
    }
}
