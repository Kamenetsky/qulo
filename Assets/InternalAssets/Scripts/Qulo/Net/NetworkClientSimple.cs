
using System;
using System.Text;
using Cysharp.Threading.Tasks;
using Newtonsoft.Json;
using Qulo.Data;
using UnityEngine;
using UnityEngine.Networking;

namespace Qulo.Net
{
    public class NetworkClientSimple : INetworkClient<IResponseData, IRequestData>
    {
        private NetRestConfigSO _config;

        public NetworkClientSimple(NetRestConfigSO config)
        {
            _config = config;
        }

        public async UniTask<IResponseData> MakeRequestAsync(IRequestData data)
        {
            IResponseData result = null;
            try
            {
                var url = _config.BaseUrl + _config.requestUrlSuffixes[data.GetRequestType()];
                using var request = new UnityWebRequest(url, "POST");
                byte[] bodyRaw = Encoding.UTF8.GetBytes(data.GetString());
                request.uploadHandler = new UploadHandlerRaw(bodyRaw);
                request.downloadHandler = new DownloadHandlerBuffer();
                request.SetRequestHeader("Content-Type", "application/json");
                
                await request.SendWebRequest();

                if (request.result != UnityWebRequest.Result.Success)
                {
                    Debug.LogError(request.error);
                }
                else
                { 
                    result = GetResponseData(data.GetRequestType(), request.downloadHandler.text);
                }
            }
            catch (UnityWebRequestException e)
            {
                result = TryParseWebRequestException(e.Text);
            }
            catch (Exception e)
            {
                Debug.LogError(e);
            }

            return result;
        }

        private IResponseData TryParseWebRequestException(string text)
        {
            BadResponseData data = null;
            try
            {
                data = JsonConvert.DeserializeObject<BadResponseData>(text);
                if (data != null)
                {
                    Debug.LogWarning($"Bad server response: {data.StatusCode} : {data.Message}");
                    return data;
                }
            }
            catch (Exception e)
            {
                Debug.LogWarning($"Can not parse web request error {e}");
            }

            return data;
        }
 
        private IResponseData GetResponseData(RequestType requestType, string text)
        {
            IResponseData result = null;
            try
            {
                if (requestType is RequestType.Registration or RequestType.Login)
                {
                    result = JsonConvert.DeserializeObject<RegistrationResponseData>(text);
                }
                else if (requestType == RequestType.Email)
                {
                    result = JsonConvert.DeserializeObject<EmailResponseData>(text);
                }
                else if (requestType == RequestType.Space)
                {
                    var space = LoadSpace();
                    if (space == null)
                    {
                        return new BadResponseData
                        {
                            StatusCode = "552",
                            Message = "Error",
                            Error = "WrongCredentials"
                        };
                    }

                    return space;
                }
            }
            catch (Exception e)
            {
                Debug.LogError($"Response has unknown exception: {e}");
            }

            return result;
        }

        //TODO: remove
        private Data.Space LoadSpace()
        {
            var resource = Resources.Load<TextAsset>("Space");
            if (resource == null)
                return null;
            
            return JsonConvert.DeserializeObject<Data.Space>(resource.text);
        }
    }
}
