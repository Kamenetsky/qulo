using Cysharp.Threading.Tasks;

namespace Qulo.Net
{
    public interface INetworkClient<TOutput, in TInput>
    {
        UniTask<TOutput> MakeRequestAsync(TInput data);
    }
}
