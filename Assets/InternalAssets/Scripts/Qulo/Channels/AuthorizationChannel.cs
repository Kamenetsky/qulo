using System;
using System.Threading.Tasks;
using Qulo.Handlers;
using UniRx;
using Utilities.Reactive;

namespace Qulo.Channels
{
    public class AuthorizationChannel
    {
        public ReactiveTrigger<PlayerCredentialsRejectReason> OnRejectAddCredentials = new ();
        public ReactiveProperty<bool> IsAcceptTerms = new ReactiveProperty<bool>(false);
        public ReactiveProperty<bool> IsCookiePolicy = new ReactiveProperty<bool>(true);
        public Func<string, Task<bool>> OnCheckPlayerEmail;
        public Func<string, string, Task<bool>> OnAddPlayerCredentials;
        public Func<string, string, Task<bool>> OnCheckPlayerCredentials;
    }
}
