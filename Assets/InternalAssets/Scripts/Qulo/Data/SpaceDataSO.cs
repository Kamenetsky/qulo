using System.Collections.Generic;
using System.IO;
using System.Linq;
using Newtonsoft.Json;
using Sirenix.OdinInspector;

#if UNITY_EDITOR
using UnityEditor;
#endif

using UnityEngine;

namespace Qulo.Data
{
    [CreateAssetMenu(fileName = "NewSpace", menuName = "Game/Data/Space")]
    public class SpaceDataSO : ScriptableObject
    {
        public string JsonFileName = "Space";
        
        [SerializeField] private Space _space;

        public Space Space => _space;
        
        private static Space GetSpaceFromScene()
        {
            var pSystems = GameObject.FindGameObjectsWithTag("PlanetSystem").ToList();
            if (pSystems.Count == 0)
                return null;

            var space = new Space();
            space.planetarySystems = new List<PlanetarySystem>();
            int psNum = 0;
            foreach (var psObj in pSystems)
            {
                var ps = new PlanetarySystem();
                space.planetarySystems.Add(ps);
                ps.id = psNum;
                ps.name = psObj.name;
                var prTransform = psObj.transform;
                ps.position = new Position(prTransform.position);
                ps.orbits = new List<Orbit>();
                int objNum = 0;
                for (var j = 0; j < prTransform.childCount; ++j)
                {
                    var orbitTransform = prTransform.GetChild(j);
                    if (!orbitTransform.CompareTag("Orbit"))
                        continue;
                    
                    var dist = Vector2.Distance(orbitTransform.position, psObj.transform.position);
                    var orbit = new Orbit();
                    orbit.radius = dist;
                    ps.orbits.Add(orbit);

                    orbit.orbitObjects = new List<OrbitObject>();
                    for (var k = 0; k < orbitTransform.childCount; ++k)
                    {
                        var orbitObjTransform = orbitTransform.GetChild(k);
                        if (!(orbitObjTransform.CompareTag("Star") || orbitObjTransform.CompareTag("Planet")
                                                                   || orbitObjTransform.CompareTag("WarpPoint")))
                            continue;

                        var orbitObject = new OrbitObject();
                        orbitObject.id = objNum;
                        orbitObject.name = orbitObjTransform.name;
                        orbitObject.angle = GetAngle(psObj.transform.position, orbitObjTransform.position);
                        orbitObject.size = orbitObjTransform.transform.localScale.x;
                        orbit.orbitObjects.Add(orbitObject);
                        objNum++;
                        orbitObject.objectType = OrbitObjectType.Unknown;
                        if (orbitObjTransform.CompareTag("Star"))
                        {
                            orbitObject.objectType = OrbitObjectType.Star;
                        }
                        else if (orbitObjTransform.CompareTag("Planet"))
                        {
                            orbitObject.objectType = OrbitObjectType.Planet;
                            orbitObject.additionalParams.Add("param1", Random.value > 0.5f);
                            orbitObject.additionalParams.Add("param2", Random.value > 0.5f);
                        }
                        else if (orbitObjTransform.CompareTag("WarpPoint"))
                        {
                            orbitObject.objectType = OrbitObjectType.WarpPoint;
                        }
                    }
                }
                
                ++psNum;
            }
            
            //Generate paths
            space.paths = new List<InterstellarPath>();
            foreach (var ps in space.planetarySystems)
            {
                var warpPoints = ps.GetWarpPoints();
                if (warpPoints.Count == 0)
                    continue;
                
                var otherSystems = space.planetarySystems.FindAll(x => x.id != ps.id)
                    .OrderBy(y => Vector2.Distance(y.position.ToVector2(), ps.position.ToVector2())).ToList();

                for (int i = 0; i < warpPoints.Count; ++i)
                {
                    var currWarpPoint = warpPoints[i];
                    if (!ps.GetObjectPosition(currWarpPoint, out Vector2 currWarpPointPos))
                    {
                        Debug.LogWarning("Can not find position for WarpPoint!"); //!!! Very strange
                        continue;
                    }

                    for (int j = 0; j < Mathf.Min(otherSystems.Count, 2); ++j)
                    {
                        var otherSystem = otherSystems[j];
                        //already exists tonnel between planetary systems
                        if (space.paths.Exists(x =>
                                (x.point1.planetSystemId == otherSystem.id && x.point2.planetSystemId == ps.id)
                                || (x.point2.planetSystemId == otherSystem.id && x.point1.planetSystemId == ps.id)))
                        {
                            continue;
                        }
                        
        
                        var otherWarpPoints = otherSystems[j].GetWarpPoints();
                        if (otherWarpPoints.Count == 0)
                            continue;

                        var connectedWarpPoint = otherWarpPoints[0];
                        float minDistance = float.MaxValue;
                        foreach (var owp in otherWarpPoints)
                        {
                            if (!otherSystem.GetObjectPosition(owp, out Vector2 owpPos))
                                continue;

                            var distance = Vector2.Distance(currWarpPointPos, owpPos);
                            if (distance < minDistance)
                            {
                                minDistance = distance;
                                connectedWarpPoint = owp;
                            }
                        }

                        var path = new InterstellarPath();
                        path.point1 = new WarpPointId { planetSystemId = ps.id, warpPointId = currWarpPoint.id };
                        path.point2 = new WarpPointId { planetSystemId = otherSystem.id, warpPointId = connectedWarpPoint.id };
                        path.distortion1 = Random.value;
                        path.distortion2 = Random.value;
                        space.paths.Add(path);
                    }
                }
            }

            return space;
        }

        [Button("Save to Json")]
        private void SaveToJson()
        {
            var space = GetSpaceFromScene();
            if (space == null)
            {
                Debug.LogError("Space data is NULL!");
                return;
            }
            
            if (string.IsNullOrEmpty(JsonFileName))
            {
                Debug.LogError("Empty file json name!");
                return;
            }

            var path = Application.dataPath + "/Resources/" + JsonFileName + ".json";

            using var writer = File.CreateText(path);
            string text = JsonConvert.SerializeObject(space, Formatting.Indented, new JsonSerializerSettings
            {
                ReferenceLoopHandling = ReferenceLoopHandling.Ignore,
                //TypeNameHandling = TypeNameHandling.All,
            });
            writer.Write(text);

            
            //Check
            /*var testSpace = JsonConvert.DeserializeObject<Space>(text, new JsonSerializerSettings
            {
                ReferenceLoopHandling = ReferenceLoopHandling.Ignore,
                TypeNameHandling = TypeNameHandling.All,
            });
            foreach (var ps in testSpace.planetarySystems)
            {
                foreach (var o in ps.orbits)
                {
                    foreach (var oObj in o.orbitObjects)
                    {
                        Debug.LogError("!!! : " + oObj.GetObjectType());
                    }
                }
            }*/
        }
        
        [Button("Get from Scene")]
        private void GetFromScene()
        {
            _space = GetSpaceFromScene();
#if UNITY_EDITOR
            EditorUtility.SetDirty(this);  
#endif
        }

        private static float GetAngle(Vector2 pos1, Vector2 pos2)
        {
            return Mathf.Atan2(pos2.y - pos1.y, pos2.x - pos1.x) * 180 / Mathf.PI;
        }
    }
}
