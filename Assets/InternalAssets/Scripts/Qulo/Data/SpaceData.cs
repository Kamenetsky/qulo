using System;
using System.Collections.Generic;
using Qulo.Net;
using UnityEngine;

namespace Qulo.Data
{
    [Serializable]
    public class WarpPointId
    {
        public int planetSystemId;
        public int warpPointId;
    }
    
    [Serializable]
    public class InterstellarPath
    {
        public float distortion1;
        public float distortion2;
        public WarpPointId point1;
        public WarpPointId point2;
    }
    
    [Serializable]
    public class Space : IResponseData
    {
        public List<PlanetarySystem> planetarySystems;
        public List<CustomObject> customObject;
        public List<InterstellarPath> paths;
        
        public ResponseType GetResponseType()
        {
            return ResponseType.Space;
        }
    }

    [Serializable]
    public class Position
    {
        public float x;
        public float y;
        
        public Position (Vector2 pos)
        {
            x = pos.x;
            y = pos.y;
        }

        public Vector2 ToVector2()
        {
            return new Vector2(x, y);
        }
    }

    [Serializable]
    public class PlanetarySystem
    {
        public int id;
        public string name;
        public Position position;
        public List<Orbit> orbits;

        public List<OrbitObject> GetWarpPoints()
        {
            var result = new List<OrbitObject>();
            foreach (var orbit in orbits)
            {
                result.AddRange(orbit.orbitObjects.FindAll(x => x.objectType == OrbitObjectType.WarpPoint));
            }
            return result;
        }

        public bool GetObjectOrbit(OrbitObject orbitObject, out Orbit orbit)
        {
            orbit = null;
            foreach (var o in orbits)
            {
                var oo = o.orbitObjects.Find(x => x == orbitObject);
                if (oo == null)
                    continue;
                
                orbit = o;
                return true;
            }
            return false;
        }
        
        public bool GetObjectOrbit(int objectId, out Orbit orbit, out OrbitObject orbitObject)
        {
            orbit = null;
            orbitObject = null;
            foreach (var o in orbits)
            {
                var oo = o.orbitObjects.Find(x => x.id == objectId);
                if (oo == null)
                    continue;
                
                orbit = o;
                orbitObject = oo;
                return true;

            }
            return false;
        }
        
        public bool GetObjectPosition(OrbitObject orbitObject, out Vector2 pos)
        { 
            pos = default;
            if (!GetObjectOrbit(orbitObject, out Orbit orbit))
            {
                return false;
            }
            
            pos = GetPositionByRadiusAndAngle(position, orbit.radius, orbitObject.angle);
            return true;
        }
        
        public OrbitObject GetObjectPosition(int objectId, out Vector2 pos)
        {
            pos = default;
            
            if (!GetObjectOrbit(objectId, out Orbit orbit, out OrbitObject orbitObject))
            {
                return null;
            }
            pos = GetPositionByRadiusAndAngle(position, orbit.radius, orbitObject.angle);
            return orbitObject;
        }

        private static Vector2 GetPositionByRadiusAndAngle(Position pos, float radius, float angle)
        {
            var initPos = pos.ToVector2() + Vector2.right * radius;
            return Utilities.Helpers.MathHelpers.RotatePointAroundPivot(initPos,
                pos.ToVector2(), new Vector3(0f, 0f, angle));
        }
    }

    [Serializable]
    public class Orbit
    {
        public float radius;
        public List<OrbitObject> orbitObjects;
    }

    public enum OrbitObjectType
    {
        Planet = 0,
        Star = 1,
        WarpPoint = 2,
        
        Unknown = 1000
    }
    
    [Serializable]
    public class OrbitObject
    {
        public OrbitObjectType objectType;
        public int id;
        public float angle;
        public string name;
        public string description;
        public float size = 1f;

        public Dictionary<string, bool> additionalParams = new Dictionary<string, bool>();
    }

    [Serializable]
    public class CustomObject
    {
        public int id;
        public string name;
        public string description;
        public Position position;
    }
}
