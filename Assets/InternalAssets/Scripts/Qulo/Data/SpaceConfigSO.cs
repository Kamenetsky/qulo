using System.Collections.Generic;
using Qulo.Views;
using Sirenix.OdinInspector;
using UnityEngine;

namespace Qulo.Data
{
    [CreateAssetMenu(fileName = "SpaceConfig", menuName = "Game/Data/SpaceConfig")]
    public class SpaceConfigSO : SerializedScriptableObject
    {
        public Dictionary<OrbitObjectType, Sprite> spaceObjectSprites;

        public ShipView shipPrefab;

        public Sprite planetAdditionalSprite;
        
        [Range(0, 100)]
        public float spaceBorder = 5f;
    }
}
