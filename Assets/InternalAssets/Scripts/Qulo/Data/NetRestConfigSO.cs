using System.Collections;
using System.Collections.Generic;
using Qulo.Net;
using Sirenix.OdinInspector;
using UnityEngine;

namespace Qulo.Data
{
    [CreateAssetMenu(fileName = "NetRestConfig", menuName = "Game/Data/NetRestConfig")]
    public class NetRestConfigSO : SerializedScriptableObject
    {
        public string BaseUrl = "https://dev.qulo.games/api/v1/backend/auth/";
        
        public Dictionary<RequestType, string> requestUrlSuffixes;
    }
}
