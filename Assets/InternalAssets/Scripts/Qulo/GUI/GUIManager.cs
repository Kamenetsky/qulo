﻿using System;
using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;
using System.Linq;
using DG.Tweening;
using Sirenix.OdinInspector;

namespace Qulo.GUI
{
    public class GUIManager : MonoBehaviour 
	{
		[Serializable]
		public class WindowInfo
		{
			[ShowInInspector] public string Type => prefab?.GetType().Name;
			public Window prefab;
			public string resoursePath;
		}
		
		public event Action<Window> OnWindowShown;
		public event Action<Window> OnWindowClosed;
		
		[SerializeField] private GameObject blackScreen;
		[SerializeField] private GameObject blocker;
        [SerializeField] private Canvas baseLayer;
        [SerializeField] private Canvas popupLayer;
        
        [Space(10)]
        [SerializeField] private List<WindowInfo> _types = new List<WindowInfo>();
        
        private List<Window> _windows = new List<Window>();
        
        private void Awake()
		{
			blackScreen.SetActive(false);
			SetBlocked(false);
		}

	    private Window FindWindow(string windowName)
        {
            for (int i = 0; i < _windows.Count; ++i)
            {
                if (windowName == _windows[i].WindowName)
                {
                    return _windows[i];
                }
            }
            return null;
        }

		private void SetWindowLayer(Window window)
		{
			if (window.WindowType == WindowType.Standart)
			{
				window.transform.SetParent(baseLayer.transform, false);
			}
			else
			{
				window.transform.SetParent(popupLayer.transform, false);
			}
		}

		public void SetBlocked(bool isBlocked)
		{
			blocker.SetActive(isBlocked);
		}

        public void SetBlackout(bool visible = true, float duration = .25f)
        {
            if (blackScreen != null)
            {
				if(!visible) 
				{
					blackScreen.GetComponent<Image>().DOFade(0.0f, duration).OnComplete(() => {
						blackScreen.SetActive(false);
					});
				} 
				else 
				{
					if (!blackScreen.activeSelf) 
					{
						blackScreen.SetActive(true);
						Image image = blackScreen.GetComponent<Image>();
						DOTween.Kill(image, false);
						Color color = image.color;
						color.a = 0.0f;
						image.color = color;
						image.DOFade(1f, duration);
					} 
					else 
					{
						Image image = blackScreen.GetComponent<Image>();
						DOTween.Kill(image, false);
						Color color = image.color;
						color.a = 1f;
						image.color = color;
					}
				}
            }
        }

		public T Show<T>() where T : Window
		{
			Type windowType = typeof(T);
			if(_types.Exists(x => x.Type == windowType.Name))
			{
                return ShowWindow<T>();
			}
			return null;
		}

        private T ShowWindow<T>() where T : Window
        {
            Type windowType = typeof(T);
            Debug.Log($"Show window: {windowType}");

            var wndInfo = _types.Find(x => x.Type == windowType.Name);

            if (!wndInfo.prefab && string.IsNullOrEmpty(wndInfo.resoursePath))
	            return null;

            Window windowPrefab = null;
            if (wndInfo.prefab)
            {
	            windowPrefab = wndInfo.prefab;
            }
            else
            {
	            windowPrefab = Resources.Load<Window>(wndInfo.resoursePath);
            }
            
            if (windowPrefab == null)
	            return null;
            
            Window window = Instantiate(windowPrefab);
            
            if (window == null)
	            return null;
            
            if (window.WindowType == WindowType.Dialog || window.WindowType == WindowType.Modal) 
            {
                HideModalWindows();
            }

            _windows.Add(window);
            ShowAndRegister(window);

	        return window as T;
        }

        private void HideModalWindows()
        {
            List<Window> windows = _windows.FindAll(
                x => x.WindowType == WindowType.Dialog || x.WindowType == WindowType.Modal
            );

            foreach (var wnd in windows) 
            {
	            wnd.Hide();
            }
        }

        private int ModalCount => _windows.Count(x => x.WindowType == WindowType.Dialog
                                                     || x.WindowType == WindowType.Modal);

		private void ShowAndRegister(Window window)
		{
			if (!window.gameObject.activeSelf) 
			{
				window.gameObject.SetActive(true);
			}
			window.RegisterWindow(this);
			SetWindowLayer(window);
			window.Show();
			
			OnWindowShown?.Invoke(window);
		}

		//TODO:
		/*private Window PendWindow(Type windowType)
		{
            var windowPrefab = Resources.Load<GameObject>(string.Format("GUI/{0}", _types[windowType]));
            GameObject windowInstance = Instantiate(windowPrefab);
			var window = windowInstance.GetComponent<Window>();
			_windows.Add(window);

			if (window.WindowType == WindowType.Dialog || window.WindowType == WindowType.Modal) 
			{
				if (ModalCount > 1) 
				{
					window.gameObject.SetActive(false);
				} 
				else 
				{
					if (!window.gameObject.activeSelf)
						window.gameObject.SetActive(true);
					ShowAndRegister(window);
				}
			} 
			else 
			{
				ShowAndRegister(window);
			}

			return window;
		}*/

		//TODO: check type by path to resource
		/*public void RegisterWindow<T>(string path) where T : IWindow
		{
			var type = typeof(T);
			if (_types.Exists(x => x.Type == type.Name))
			{
				return;
			}
			_types.Add(new WindowInfo{resoursePath = path});
		}*/
		
		public void RegisterWindow<T>(Window prefab) where T : IWindow
		{
			var type = typeof(T);
			if (_types.Exists(x => x.Type == type.Name))
			{
				return;
			}
			_types.Add(new WindowInfo{ prefab = prefab });
		}

		public void RemoveAll()
		{
			while(_windows.Count > 0) 
			{
				RemoveWindow(_windows[_windows.Count - 1]);
			}
		}
       
        private void PopTopWindow()
        {
            List<Window> windows = _windows.FindAll(x => x.WindowType == WindowType.Dialog || x.WindowType == WindowType.Modal);
            
            if (windows.Count > 0) 
            {
	            windows[0].Close();
            }
        }

		private void CheckPending()
		{
			var wnds = _windows.FindAll(x => x.WindowType == WindowType.Dialog || x.WindowType == WindowType.Modal);
            if (wnds.Count > 0)
            {
                ShowAndRegister(wnds[wnds.Count - 1]);
			}
		}

		public void RemoveWindow(Window window)
		{
			Window wnd = FindWindow(window.WindowName);
			if(wnd != null) 
			{
				_windows.Remove(wnd);
				OnWindowClosed?.Invoke(wnd);
				Destroy(window.gameObject);
			}
			CheckPending();
		}
	}
}