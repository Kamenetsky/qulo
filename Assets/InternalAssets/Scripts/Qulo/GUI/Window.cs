﻿using UnityEngine;

namespace Qulo.GUI
{
    public enum WindowState
    {
        Hidden = 0,
        Showing,
        Present,
        Hiding
    }

	public abstract class Window : MonoBehaviour, IWindow
	{
		[SerializeField] private WindowType windowType = WindowType.Modal;

		private GUIManager _windowManager = null;
        private WindowState _windowState = WindowState.Hidden;

        public WindowState WindowState
        {
	        get { return _windowState; }
	        set { _windowState = value; }
        }

        public string WindowName => GetType().Name;

        public WindowType WindowType => windowType;

        public virtual void RegisterWindow(GUIManager manager)
		{
			_windowManager = manager;
		}

		public virtual void Close()
		{
			_windowManager.RemoveWindow(this);
		}
		

		public virtual void Show()
		{
			if(!gameObject.activeSelf) 
			{
				gameObject.SetActive(true);
			}
			
			_windowState = WindowState.Present;
		}
		
		public virtual void Hide()
		{
			_windowState = WindowState.Hidden;
			gameObject.SetActive(false);
		}
	}
}