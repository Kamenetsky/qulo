﻿using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Qulo.GUI
{
    public class CommonInfoWindow : Window
    {
        [SerializeField] private TMP_Text textInfo = null;
        [SerializeField] private TMP_Text textCaption = null;
        [SerializeField] private Button closeButton;

        private void Awake()
        {
            closeButton.onClick.AddListener(Hide);
        }

        private void OnDestroy()
        {
            closeButton.onClick.RemoveAllListeners();
        }

        public void ShowInfo(string text, string caption = "")
        {
            Show();
            textCaption.text = caption;
            textInfo.text = text;
        }
    }
}
