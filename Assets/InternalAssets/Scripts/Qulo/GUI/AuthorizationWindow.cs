using System;
using System.Collections.Generic;
using Cysharp.Threading.Tasks;
using DG.Tweening;
using Qulo.Channels;
using Qulo.Handlers;
using TMPro;
using UniRx;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using Utilities.Helpers;
using Utilities.Reactive;

namespace Qulo.GUI
{
    public class AuthorizationWindow : Window
    {
        [Serializable]
        public class PanelContent
        {
            public AuthorizationState State;
            public GameObject Panel;
            public CanvasGroup CanvasGroup;
        }
        
        public struct Context
        {
            public string InitEmail;
            public AuthorizationChannel AuthorizationChannel;
            public Action<string, AuthorizationState> OnEnterEmail;
            public Action<string, AuthorizationState> OnEnterPassword;
            public ReactiveTrigger<AuthorizationState, Action> OnDoChangeState;
            public ReactiveTrigger OnShowWrongCredentials;
            public ReactiveTrigger OnGoToGame;
        }

        [Header("Panels content")]
        [SerializeField] private List<PanelContent> panelsContent = new();

        [Space(10)][Header("Registration Email")]
        [SerializeField] private TMP_InputField emailRegInput;
        [SerializeField] private Toggle newsEmailRegToggle;
        [SerializeField] private Toggle contractEmailRegToggle;
        [SerializeField] private Button emailRegBtn;
        [SerializeField] private GameObject warningEmailRegTxt;
        [SerializeField] private EventTrigger emailRegTextTrigger;
        
        [Space(10)][Header("Registration Password")]
        [SerializeField] private TMP_InputField passRegInput;
        [SerializeField] private TMP_InputField passRegRepeatInput;
        [SerializeField] private Button passRegLogInBtn;
        [SerializeField] private GameObject warningPassRegTxt;
        [SerializeField] private EventTrigger passRegTextTrigger;
        
        [Space(10)][Header("Authorization Email")]
        [SerializeField] private TMP_InputField emailAuthInput;
        [SerializeField] private Toggle emailAuthSaveDataToggle;
        [SerializeField] private Button emailAuthBtn;
        [SerializeField] private EventTrigger emailAuthTextTrigger;
        
        [Space(10)][Header("Authorization Password")]
        [SerializeField] private TMP_InputField passAuthInput;
        [SerializeField] private Button passAuthLogInBtn;
        [SerializeField] private EventTrigger passAuthForgotPassTextTrigger;
        [SerializeField] private GameObject passAuthWrongInfoTxt;
        [SerializeField] private EventTrigger passAuthTextTrigger;
        
        [Space(10)][Header("Restore Email")]
        [SerializeField] private TMP_InputField emailRestoreInput;
        [SerializeField] private Button emailRestoreBtn;
        [SerializeField] private GameObject passRestoreWrongInfoTxt;
        [SerializeField] private EventTrigger emailRestoreTextTrigger;
        
        [Space(10)][Header("Restore Password")]
        [SerializeField] private Button passwordRestoreBtn;
        [SerializeField] private EventTrigger passwordRestoreTextTrigger;

        [Space(10)][SerializeField] private GameObject inputBlocker;
        
        [Space(10)][SerializeField] private EventTrigger toGameTrigger;
        
        private AuthorizationState _currentState;

        private Context _ctx;

        public void SetContext(Context ctx)
        {
            _ctx = ctx;
            Init();
        }

        private void OnDestroy()
        {
            emailRegBtn.onClick.RemoveAllListeners();
            emailAuthBtn.onClick.RemoveAllListeners();
            emailRestoreBtn.onClick.RemoveAllListeners();
            
            passRegLogInBtn.onClick.RemoveAllListeners();
            passAuthLogInBtn.onClick.RemoveAllListeners();
            
            emailRegTextTrigger.triggers.Clear();
            passRegTextTrigger.triggers.Clear();
            emailAuthTextTrigger.triggers.Clear();
            passAuthTextTrigger.triggers.Clear();
            passwordRestoreTextTrigger.triggers.Clear();
            emailRestoreTextTrigger.triggers.Clear();
            passAuthForgotPassTextTrigger.triggers.Clear();
            toGameTrigger.triggers.Clear();
            
            newsEmailRegToggle.onValueChanged.RemoveAllListeners();
            contractEmailRegToggle.onValueChanged.RemoveAllListeners();
        }

        private void Init()
        {
            Clear();
            foreach (var it in panelsContent)
            {
                it.Panel.SetActive(false);
            }

            emailRegBtn.onClick.AddListener(OnEmailButtonClick);
            emailAuthBtn.onClick.AddListener(OnEmailButtonClick);
            emailRestoreBtn.onClick.AddListener(OnEmailButtonClick);
            
            passRegLogInBtn.onClick.AddListener(OnPasswordButtonClick);
            passAuthLogInBtn.onClick.AddListener(OnPasswordButtonClick);
            
            emailRegTextTrigger.AddEventListener(EventTriggerType.PointerClick, OnTextTriggerAlreadyRegisterClick);
            passRegTextTrigger.AddEventListener(EventTriggerType.PointerClick, OnTextTriggerAlreadyRegisterClick);
            emailAuthTextTrigger.AddEventListener(EventTriggerType.PointerClick, OnTextTriggerNotRegisterClick);
            passAuthTextTrigger.AddEventListener(EventTriggerType.PointerClick, OnTextTriggerNotRegisterClick);
            passwordRestoreTextTrigger.AddEventListener(EventTriggerType.PointerClick, OnTextTriggerNotRegisterClick);
            emailRestoreTextTrigger.AddEventListener(EventTriggerType.PointerClick, OnTextTriggerNotRegisterClick);
            passAuthForgotPassTextTrigger.AddEventListener(EventTriggerType.PointerClick, OnTextTriggerForgotPasswordClick);
            toGameTrigger.AddEventListener(EventTriggerType.PointerClick, x => _ctx.OnGoToGame?.Notify());

            _ctx.OnDoChangeState.Subscribe((x, y) => ChangeState(x, y).Forget()).AddTo(this);
            _ctx.OnShowWrongCredentials.Subscribe(ShowWrongCredentials).AddTo(this);
            
            if (string.IsNullOrEmpty(_ctx.InitEmail))
            {
                ChangeState(AuthorizationState.RegistrationEmail).Forget();
            }
            else
            {
                ChangeState(AuthorizationState.AuthorizationEmail, () => emailAuthInput.text = _ctx.InitEmail).Forget();
            }
            
            newsEmailRegToggle.onValueChanged.AddListener(x => _ctx.AuthorizationChannel.IsCookiePolicy.Value = x);
            contractEmailRegToggle.onValueChanged.AddListener(x => _ctx.AuthorizationChannel.IsAcceptTerms.Value = x);
        }

        private void OnEmailButtonClick()
        {
            TMP_InputField input = null;
            switch (_currentState)
            {
                case AuthorizationState.RegistrationEmail:
                    input = emailRegInput; break;
                case AuthorizationState.AuthorizationEmail:
                    input = emailAuthInput; break;
                case AuthorizationState.RestoreEmail:
                    input = emailRestoreInput; break;
            }

            if (input == null)
            {
                Debug.LogWarning("Can not find email input!");
                return;
            }

            if (input == emailRegInput && !contractEmailRegToggle.isOn)
            {
               warningEmailRegTxt.SetActive(true);
               return;
            }
            
            _ctx.OnEnterEmail?.Invoke(input.text, _currentState);
        }
        
        private void OnPasswordButtonClick()
        {
            TMP_InputField input = null;
            switch (_currentState)
            {
                case AuthorizationState.RegistrationPassword:
                    input = passRegInput; break;
                case AuthorizationState.AuthorizationPassword:
                    input = passAuthInput; break;
                case AuthorizationState.RestorePassword:
                    _ctx.OnEnterPassword?.Invoke(String.Empty, _currentState);
                    return;
            }

            if (input == null)
            {
                Debug.LogWarning("Can not find email input!");
                return;
            }

            if (input == passRegInput && input.text != passRegRepeatInput.text)
            {
                Debug.LogWarning("Registration passwords does hot maths!");
                warningPassRegTxt.SetActive(true);
                return;
            }
            
            _ctx.OnEnterPassword?.Invoke(input.text, _currentState);
        }

        private void OnTextTriggerAlreadyRegisterClick(BaseEventData eventData)
        {
            ChangeState(AuthorizationState.AuthorizationEmail).Forget();
        }
        
        private void OnTextTriggerNotRegisterClick(BaseEventData eventData)
        {
            ChangeState(AuthorizationState.RegistrationEmail).Forget();
        }
        
        private void OnTextTriggerForgotPasswordClick(BaseEventData eventData)
        {
            ChangeState(AuthorizationState.RestoreEmail).Forget();
        }

        private void Clear()
        {
            emailRegInput.text = String.Empty;
            passRegInput.text = String.Empty;
            passRegRepeatInput.text = String.Empty;
            emailAuthInput.text = String.Empty;
            passAuthInput.text = String.Empty;
            emailRestoreInput.text = String.Empty;
            passAuthWrongInfoTxt.SetActive(false);
            passRestoreWrongInfoTxt.SetActive(false);
            warningEmailRegTxt.SetActive(false);
            warningPassRegTxt.SetActive(false);
        }
        
        private async UniTaskVoid ChangeState(AuthorizationState state, Action onComplete = null)
        {
            var currPanelContent = panelsContent.Find(x => x.State == _currentState);
            if (state == _currentState)
            {
                currPanelContent.Panel.SetActive(true);
                currPanelContent.CanvasGroup.alpha = 1f;
                return;
            }
            
            int delay = state == AuthorizationState.SplashScreen ? 1000 : 300;
            inputBlocker.SetActive(true);
            var nextPanelContent = panelsContent.Find(x => x.State == state);
            _currentState = state;
            currPanelContent.CanvasGroup.DOFade(0f, delay / 1000f);
            Clear();
            onComplete?.Invoke();
            nextPanelContent.CanvasGroup.alpha = 0f;
            nextPanelContent.Panel.SetActive(true);
            nextPanelContent.CanvasGroup.DOFade(1f, delay / 1000f);
            await UniTask.Delay(delay * 2);
            inputBlocker.SetActive(false);
            currPanelContent.Panel.SetActive(false);
        }

        private void ShowWrongCredentials()
        {
            passAuthWrongInfoTxt.SetActive(true);
            passRestoreWrongInfoTxt.SetActive(true); 
        }

        #region Unity
        
        public void OnChangePasswordMode(TMP_InputField input)
        {
            if (input.inputType == TMP_InputField.InputType.Password)
                input.inputType = TMP_InputField.InputType.Standard;
            else if (input.inputType == TMP_InputField.InputType.Standard)
                input.inputType = TMP_InputField.InputType.Password;
            
            input.ForceLabelUpdate();
        }

        public void OnClearPlayerPrefs()
        {
            PlayerPrefs.DeleteAll();
            PlayerPrefs.Save();
        }
        
        #endregion
    }
}
