﻿using Qulo.Data;
using Qulo.Views;
using TMPro;
using UniRx;
using UnityEngine;
using UnityEngine.UI;

namespace Qulo.GUI
{
    public class SpaceObjectInfoWindow : Window
    {
        public struct Context
        {
            public ReactiveProperty<OrbitObject> SelectedOrbitObject;
            public ReactiveProperty<OrbitObject> ShipOrbitObject;
        }
        
        [SerializeField] private TMP_Text textInfo = null;
        [SerializeField] private TMP_Text textCaption = null;
        [SerializeField] private Button closeButton;
        
        private Context _ctx;

        public void SetContext(Context ctx)
        {
            _ctx = ctx;

            _ctx.SelectedOrbitObject.Subscribe(ShowInfo).AddTo(this);
            closeButton.onClick.AddListener(() => _ctx.SelectedOrbitObject.Value = null);
        }

        private void ShowInfo(OrbitObject orbitObject)
        {
            if (orbitObject == null || orbitObject == _ctx.ShipOrbitObject.Value)
            {
                Hide();
                return;
            }

            Show();
            textCaption.text = orbitObject.name;
            textInfo.text = orbitObject.description;
        }
    }
}
