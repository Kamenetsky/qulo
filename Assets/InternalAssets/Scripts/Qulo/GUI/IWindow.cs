﻿namespace Qulo.GUI
{
    public enum WindowType
    {
        Modal = 0,  // Above HUD, below Dialog
        Dialog = 1,     // Above Modal
        Standart = 2,        // Below all
    }

    public interface IWindow
    {
        void RegisterWindow(GUIManager manager);
        void Close();
        void Show();
        WindowType WindowType { get; }
    }
}