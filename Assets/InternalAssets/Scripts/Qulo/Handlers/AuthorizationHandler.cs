using System;
using Qulo.Channels;
using Qulo.GUI;
using UnityEngine;
using Utilities.Helpers;
using Utilities.Reactive;

namespace Qulo.Handlers
{
    public enum AuthorizationState
    {
        RegistrationEmail = 0,
        RegistrationPassword = 1,
        
        SplashScreen = 2,
        Greetings = 3,
        
        AuthorizationEmail = 4,
        AuthorizationPassword = 5,
        
        Entrance = 6,
        
        RestoreEmail = 7,
        RestorePassword = 8
    }
    
    public class AuthorizationHandler : IDisposable
    {
        public struct Context
        {
            public GUIManager GuiManager;
            public AuthorizationChannel AuthorizationChannel;
            public ReactiveTrigger OnGoToGame;
            public string LastSavedEmail;
        }
        
        private Action<string, AuthorizationState> _onEnterEmail;
        private Action<string, AuthorizationState> _onEnterPassword;

        private readonly ReactiveTrigger<AuthorizationState, Action> _onStateChange = new ();
        private readonly ReactiveTrigger _onShowWrongCredentials = new ();

        private AuthorizationState _currentState;
        private readonly Context _ctx;
        private string _currentEmail;
        private bool _isHold;

        public AuthorizationHandler(Context ctx)
        {
            _ctx = ctx;
            
            _onEnterEmail += OnDoEnterEmail;
            _onEnterPassword += OnDoEnterPassword;

            var authorizationWindow = _ctx.GuiManager.Show<AuthorizationWindow>();
            authorizationWindow.SetContext(new AuthorizationWindow.Context{ OnEnterEmail = _onEnterEmail, 
                OnEnterPassword = _onEnterPassword, InitEmail = _ctx.LastSavedEmail,
                OnGoToGame = _ctx.OnGoToGame, OnDoChangeState = _onStateChange,
                OnShowWrongCredentials = _onShowWrongCredentials,
                AuthorizationChannel = _ctx.AuthorizationChannel
            });

            _ctx.OnGoToGame.Subscribe(() => Debug.Log("Go To Quests"));
        }
        
        public void Dispose()
        {
            _onEnterEmail -= OnDoEnterEmail;
            _onEnterPassword -= OnDoEnterPassword;
            
            _ctx.OnGoToGame?.Dispose();
        }

        private async void OnDoEnterEmail(string email, AuthorizationState state)
        {
            if (!EmailValidator.IsEmailValid(email))
            {
                Debug.LogWarning("Email has wrong format!");
                _ctx.AuthorizationChannel.OnRejectAddCredentials?.Notify(PlayerCredentialsRejectReason.WrongEmail);
                return;
            }
            
            if (_isHold)
                return;

            _isHold = true;
            _ctx.GuiManager.SetBlocked(true);

            bool result = false;

            AuthorizationState nextState = _currentState;
            if (state == AuthorizationState.AuthorizationEmail)
            {
                nextState = AuthorizationState.AuthorizationPassword;
                result = true;
            }
            else if (state == AuthorizationState.RegistrationEmail)
            {
                nextState = AuthorizationState.RegistrationPassword;
                result = true;
            }
            else if (state == AuthorizationState.RestoreEmail)
            {
                result = await _ctx.AuthorizationChannel.OnCheckPlayerEmail.Invoke(email);
                if (!result)
                {
                    _ctx.AuthorizationChannel.OnRejectAddCredentials?.Notify(PlayerCredentialsRejectReason.WrongEmail);
                    nextState = AuthorizationState.RestorePassword;
                }
            }
            else
            {
                Debug.LogWarning("Email wrong state!");
            }
            
            _isHold = false;
            _ctx.GuiManager.SetBlocked(false);

            if (result)
            {
                _currentEmail = email;
                _onStateChange?.Notify(nextState, null);
            }
            else
            {
                _onShowWrongCredentials.Notify();
            }
        }
        
        private async void OnDoEnterPassword(string password, AuthorizationState state)
        {
            if (String.IsNullOrEmpty(password))
            {
                Debug.LogWarning("Password is empty!");
                _ctx.AuthorizationChannel.OnRejectAddCredentials?.Notify(PlayerCredentialsRejectReason.EmptyPassword);
                return;
            }
            
            if (_isHold)
                return;

            _isHold = true;
            _ctx.GuiManager.SetBlocked(true);

            bool result = false;

            AuthorizationState nextState = _currentState;
            if (state == AuthorizationState.AuthorizationPassword)
            {
                nextState = AuthorizationState.Entrance;
                result = await _ctx.AuthorizationChannel.OnCheckPlayerCredentials(_currentEmail, password);
            }
            else if (state == AuthorizationState.RegistrationPassword)
            {
                nextState = AuthorizationState.Entrance;
                result = await _ctx.AuthorizationChannel.OnAddPlayerCredentials(_currentEmail, password);
            }
            else
            {
                Debug.LogWarning("Email wrong state!");
            }
            
            
            _isHold = false;
            _ctx.GuiManager.SetBlocked(false);

            if (result)
            {
                _onStateChange?.Notify(nextState, null);
            }
            else
            {
                _onShowWrongCredentials.Notify();
            }
        }
    }
}
