using System;
using System.Collections.Generic;
using Qulo.Data;
using UnityEngine;
using Qulo.Views;
using UniRx;

namespace Qulo.Handlers
{
    public class InterstellarPathsHandler : IDisposable
    {
        public class Path
        {
            public OrbitObject orbitObject1;
            public OrbitObject orbitObject2;
            public Vector2 position1;
            public Vector2 position2;
            public List<Vector3> path = new List<Vector3>();
        }
        
        public struct Context
        {
            public Data.Space Space;
            public InterstellarPathsView PathViewPrefab;
            public Transform SpaceBackground;
            public ReactiveCommand<OrbitObjectView> OnObjectClick;
            public ReactiveProperty<OrbitObject> ShipOrbitObject;
            public ReactiveCommand<Path> OnTryWarp;
        }
        
        private Context _ctx;
        private List<Path> _paths = new List<Path>();
        
        public InterstellarPathsHandler(Context ctx)
        {
            _ctx = ctx;

            _ctx.OnObjectClick.Subscribe(CheckOrbitObjectForWarp);
            Init();
        }

        private void Init()
        {
            foreach (var path in _ctx.Space.paths)
            {
                var psId1 = path.point1.planetSystemId;
                var wpId1 = path.point1.warpPointId;
                var psId2 = path.point2.planetSystemId;
                var wpId2 = path.point2.warpPointId;
                
                var ps1 = _ctx.Space.planetarySystems.Find(x => x.id == psId1);
                var ps2 = _ctx.Space.planetarySystems.Find(x => x.id == psId2);

                var wp1 = ps1.GetObjectPosition(wpId1, out var pos1);
                if (wp1 == null)
                {
                    Debug.LogError($"Can not find WarpPoint with id:{wpId1} at planet system {psId1} : {ps1.name}");
                    continue;
                }
                
                var wp2 = ps2.GetObjectPosition(wpId2, out var pos2);
                if (wp2 == null)
                {
                    Debug.LogError($"Can not find WarpPoint with id:{wpId2} at planet system {psId2} : {ps2.name}");
                    continue;
                }

                Path internalPath = new Path
                {
                    orbitObject1 = wp1,
                    orbitObject2 = wp2,
                    position1 = pos1,
                    position2 = pos2
                };
                _paths.Add(internalPath);
                var pathView = UnityEngine.Object.Instantiate(_ctx.PathViewPrefab, _ctx.SpaceBackground);
                pathView.gameObject.SetActive(true);
                pathView.SetContext(new InterstellarPathsView.Context
                {
                    point1 = pos1,
                    point2 = pos2,
                    distortion1 = path.distortion1,
                    distortion2 = path.distortion2,
                    OnPathCreated = x => internalPath.path.AddRange(x)
                });
            }
        }

        private void CheckOrbitObjectForWarp(OrbitObjectView ooView)
        {
            if (ooView.OrbitObject == _ctx.ShipOrbitObject.Value)
                return;
            
            var path = _paths.Find(x => (x.orbitObject1 == ooView.OrbitObject
                                         && _ctx.ShipOrbitObject.Value == x.orbitObject2)
                                        || (x.orbitObject2 == ooView.OrbitObject
                                            && _ctx.ShipOrbitObject.Value == x.orbitObject1));

            if (path != null)
            {
                _ctx.OnTryWarp?.Execute(path);
                return;
            }
            
            var psShip = _ctx.Space.planetarySystems.Find(x => 
                x.GetObjectOrbit(_ctx.ShipOrbitObject.Value, out var orbit));
            var psDesire = _ctx.Space.planetarySystems.Find(x => 
                x.GetObjectOrbit(ooView.OrbitObject, out var orbit));
                
            if (psShip != psDesire) //out of planetary system and not warp-point
                return;

            psShip.GetObjectPosition(_ctx.ShipOrbitObject.Value, out var pos1);
            psShip.GetObjectPosition(ooView.OrbitObject, out var pos2);
            var innerPath = new Path
            {
                orbitObject1 = _ctx.ShipOrbitObject.Value,
                orbitObject2 = ooView.OrbitObject,
                position1 = pos1,
                position2 = pos2,
                path = new List<Vector3>{pos1, pos2}
            };
            _ctx.OnTryWarp?.Execute(innerPath);
        }
        
        public void Dispose()
        {
            _ctx.OnObjectClick.Dispose();
        }
    }
}
