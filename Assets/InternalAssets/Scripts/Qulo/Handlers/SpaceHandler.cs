using System;
using System.Collections.Generic;
using System.Linq;
using Qulo.Data;
using Qulo.GUI;
using UniRx;
using UnityEngine;
using Qulo.Views;
using Space = Qulo.Data.Space;

namespace Qulo.Handlers
{
    public class SpaceHandler : IDisposable
    {
        public struct Context
        {
            public Space Space;
            public GUIManager GuiManager;
            public PlanetarySystemView PlanetarySystemPrefab;
            public InterstellarPathsView PathsViewPrefab;
            public SpaceConfigSO SpaceConfig;
            public Transform SpaceBackground;
            public ReactiveCommand<OrbitObjectView> OnObjectClick;
        }
        
        private readonly ReactiveProperty<OrbitObject> _selectedOrbitObject = new (null);
        private readonly ReactiveProperty<OrbitObject> _shipOrbitObject = new (null);
        private readonly ReactiveCommand<InterstellarPathsHandler.Path> _onTryWarp = new ReactiveCommand<InterstellarPathsHandler.Path>();

        private readonly Context _ctx;
        private Rect _size;
        private readonly InterstellarPathsHandler _interstellarPathsHandler;
        private readonly ShipHandler _shipHandler;
        private SpaceObjectInfoWindow _infoWindow;

        private List<PlanetarySystemView> _planetarySystemViews = new List<PlanetarySystemView>();

        public Rect Size => _size;

        public SpaceHandler(Context ctx)
        {
            _ctx = ctx;

            _interstellarPathsHandler = new InterstellarPathsHandler(new InterstellarPathsHandler.Context
            {
                Space = _ctx.Space,
                SpaceBackground = _ctx.SpaceBackground,
                PathViewPrefab = _ctx.PathsViewPrefab,
                OnObjectClick = _ctx.OnObjectClick,
                ShipOrbitObject = _shipOrbitObject,
                OnTryWarp = _onTryWarp
            });

            _ctx.OnObjectClick.Subscribe(x => _selectedOrbitObject.Value = x.OrbitObject);

            InitSpaceView();

            var originView = _planetarySystemViews[0].OrbitObjectViews
                .Find(x => x.OrbitObject.objectType == OrbitObjectType.WarpPoint);
            _shipOrbitObject.Value = originView.OrbitObject;

            _shipHandler = new ShipHandler(new ShipHandler.Context
            {
                ShipOrbitObject = _shipOrbitObject,
                SpaceBackground = _ctx.SpaceBackground,
                SpaceConfig = _ctx.SpaceConfig,
                OnTryWarp = _onTryWarp,
                StartPosition = originView.transform.position
            });
            
            _infoWindow = _ctx.GuiManager.Show<SpaceObjectInfoWindow>();
            _infoWindow.SetContext(new SpaceObjectInfoWindow.Context
            {
                SelectedOrbitObject = _selectedOrbitObject,
                ShipOrbitObject = _shipOrbitObject
            });
            _infoWindow.Hide();
        }

        private void InitSpaceView()
        {
            var xMin = float.MaxValue;
            var xMax = float.MinValue;
            var yMin = float.MaxValue;
            var yMax = float.MinValue;
            foreach (var ps in _ctx.Space.planetarySystems)
            {
                if (ps.orbits.Count == 0)
                    continue;
                
                ps.orbits = ps.orbits.OrderBy(x => x.radius).ToList();

                var farOrbitRadius = ps.orbits[ps.orbits.Count - 1].radius;
                xMin = ps.position.x - farOrbitRadius;
                if (xMin < _size.xMin)
                    _size.xMin = xMin;
                xMax = ps.position.x + farOrbitRadius;
                if (xMax > _size.xMax)
                    _size.xMax = xMax;
                yMin = ps.position.y - farOrbitRadius;
                if (yMin < _size.yMin)
                    _size.yMin = yMin;
                yMax = ps.position.y + farOrbitRadius;
                if (yMax > _size.yMax)
                    _size.yMax = yMax;
                
                var psView = UnityEngine.Object.Instantiate(_ctx.PlanetarySystemPrefab, _ctx.SpaceBackground);
                psView.transform.position = new Vector3(ps.position.x, ps.position.y, 0f);
                psView.SetContext(new PlanetarySystemView.Context
                {
                    PlanetarySystem = ps, 
                    SpaceConfig = _ctx.SpaceConfig,
                    OnObjectClick = _ctx.OnObjectClick
                });
                _planetarySystemViews.Add(psView);
            }
            
            _size.xMin -= _ctx.SpaceConfig.spaceBorder;
            _size.yMin -= _ctx.SpaceConfig.spaceBorder;
            _size.xMax += _ctx.SpaceConfig.spaceBorder;
            _size.yMax += _ctx.SpaceConfig.spaceBorder;
        }

        public void Dispose()
        {
            _interstellarPathsHandler.Dispose();
            _selectedOrbitObject.Dispose();
            _shipOrbitObject.Dispose();
            _shipHandler.Dispose();
            _onTryWarp.Dispose();
        }
    }
}
