using System;
using System.Threading.Tasks;
using Qulo.Channels;
using Qulo.GUI;
using Qulo.Net;
using UnityEngine;

namespace Qulo.Handlers
{
    public enum PlayerCredentialsRejectReason
    {
        EmailAlreadyExists = 0,
        EmptyPassword = 1,
        WrongEmail = 3,
        WrongCredentials = 4
    }
    
    public class PlayerHandler : IDisposable
    {
        public struct Context
        {
            public GUIManager GuiManager;
            public INetworkClient<IResponseData, IRequestData> NetworkClient;
            public AuthorizationChannel AuthorizationChannel;
        }

        private Context _ctx;

        public string LastSavedEmail => PlayerPrefs.GetString("LastSavedEmail");

        private string _accessToken;
        private string _refreshToken;

        private CommonInfoWindow _infoWindow;

        public PlayerHandler(Context ctx)
        {
            _ctx = ctx;
            _ctx.AuthorizationChannel.OnAddPlayerCredentials += AddNewRegistration;
            _ctx.AuthorizationChannel.OnCheckPlayerCredentials += CheckCredentials;
            _ctx.AuthorizationChannel.OnCheckPlayerEmail += CheckEmail;
        }
        
        public void Dispose()
        {
            _ctx.AuthorizationChannel.OnAddPlayerCredentials -= AddNewRegistration;
            _ctx.AuthorizationChannel.OnCheckPlayerCredentials -= CheckCredentials;
            _ctx.AuthorizationChannel.OnCheckPlayerEmail -= CheckEmail;
        }

        private async Task<bool> CheckCredentials(string email, string password)
        {
            if (string.IsNullOrEmpty(password))
            {
                _ctx.AuthorizationChannel.OnRejectAddCredentials?.Notify(PlayerCredentialsRejectReason.EmptyPassword);
                return false;
            }
            
            var data = new LoginRequestData { Email = email, Password = password };
            var result = await _ctx.NetworkClient.MakeRequestAsync(data);

            return ProcessAuthData(result, email);;
        }

        private async Task<bool> AddNewRegistration(string email, string password)
        {
            if (string.IsNullOrEmpty(password))
            {
                _ctx.AuthorizationChannel.OnRejectAddCredentials?.Notify(PlayerCredentialsRejectReason.EmptyPassword);
                return false;
            }
            
            var data = new RegistrationRequestData { Email = email, Password = password, 
                CookiePolicy = _ctx.AuthorizationChannel.IsCookiePolicy.Value, 
                AcceptTerms = _ctx.AuthorizationChannel.IsAcceptTerms.Value };
            //var data = new RegistrationRequestData();
            var result = await _ctx.NetworkClient.MakeRequestAsync(data);
            
            return ProcessAuthData(result, email);
        }
        
        private async Task<bool> CheckEmail(string email)
        {
            var data = new EmailRequestData { Email = email };
            var result = await _ctx.NetworkClient.MakeRequestAsync(data);
            
            return ProcessAuthData(result, email);
        }

        private bool ProcessAuthData(IResponseData data, string email)
        {
            bool result = false;
            if (data == null)
            {
                Debug.LogError("Response for authentication request is null");
            }
            else if (data.GetResponseType() == ResponseType.Registration || 
                     data.GetResponseType() == ResponseType.Login)
            {
                PlayerPrefs.SetString("LastSavedEmail", email);
                result = true;
            }
            else if (data.GetResponseType() == ResponseType.Email)
            {
                ShowInfo($"Token: {(data as EmailResponseData)?.Token}", "Information");
            }
            else if (data.GetResponseType() == ResponseType.BadResponse)
            {
                var info = (BadResponseData) data;
                Debug.LogError($"Bad response for authentication request {info.Error} : {info.Message}");
                _ctx.AuthorizationChannel.OnRejectAddCredentials?.Notify(PlayerCredentialsRejectReason.WrongCredentials);
                ShowInfo(info.Message.ToString(), "Bad request");
            }

            return result;
        }

        private void ShowInfo(string message, string caption = "")
        {
            if (!_infoWindow)
            {
                _infoWindow = _ctx.GuiManager.Show<CommonInfoWindow>();
            }
            _infoWindow.ShowInfo(message, caption);
        }
    }
}
