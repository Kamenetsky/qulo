using System;
using System.Collections.Generic;
using Cysharp.Threading.Tasks;
using Qulo.Data;
using Qulo.Views;
using UniRx;
using UnityEngine;

namespace Qulo.Handlers
{
    public class ShipHandler : IDisposable
    {
        public struct Context
        {
            public SpaceConfigSO SpaceConfig;
            public Transform SpaceBackground;
            public Vector3 StartPosition;
            public ReactiveProperty<OrbitObject> ShipOrbitObject;
            public ReactiveCommand<InterstellarPathsHandler.Path> OnTryWarp;
        }

        private readonly Context _ctx;
        private readonly ShipView _shipView;
        private bool _isOnPath;

        public bool IsOnPath => _isOnPath;

        public ShipHandler(Context ctx)
        {
            _ctx = ctx;

            _shipView = UnityEngine.Object.Instantiate(_ctx.SpaceConfig.shipPrefab, _ctx.SpaceBackground);
            _shipView.transform.position = _ctx.StartPosition;
            _ctx.OnTryWarp?.Subscribe(x => TryWarp(x).Forget());
        }

        private async UniTaskVoid TryWarp(InterstellarPathsHandler.Path path)
        {
            if (IsOnPath || path == null)
                return;
            
            _isOnPath = true;
            bool invertPath = false;
            Vector3 finalPos;
            if (_ctx.ShipOrbitObject.Value == path.orbitObject1)
            {
                _ctx.ShipOrbitObject.Value = path.orbitObject2;
                finalPos = path.position2;
            }
            else
            {
                invertPath = true;
                _ctx.ShipOrbitObject.Value = path.orbitObject1;
                finalPos = path.position1;
            }
            
            var pathArray = new List<Vector3>(path.path);
            if (invertPath)
                pathArray.Reverse();

            await _shipView.Warp(pathArray, finalPos);

            _isOnPath = false;
        }
        
        public void Dispose()
        {
            _ctx.ShipOrbitObject.Dispose();
            _ctx.OnTryWarp.Dispose();
        }
    }
}
