using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

namespace Qulo
{
    [RequireComponent(typeof(Camera))][DisallowMultipleComponent]
    public class CameraController : MonoBehaviour
    {
        [SerializeField] private float panSpeed = 50;

        private Vector3 _dragOrigin;
        private Vector3 _cameraDragOrigin;
        private Vector3 _currentPosition;

        private Camera _camera;
        private Rect _bounds;
        private bool _checkBounds;
        private bool _isScrolling;

        private void Awake()
        {
            _camera = GetComponent<Camera>();
        }

        public void SetBounds(Rect bounds)
        {
            _bounds = bounds;
            _checkBounds = true;
        }

        private void LateUpdate()
        {
            //if (EventSystem.current.IsPointerOverGameObject())
            if (IsPointerOverUIObject())
                return;

            if (Input.GetMouseButtonDown(0))
            {
                _cameraDragOrigin = transform.position;
                _dragOrigin = _camera.ScreenToViewportPoint(Input.mousePosition);
                _isScrolling = true;
            }

            if (Input.GetMouseButtonUp(0))
            {
                _isScrolling = false;
            }

            if (Input.GetMouseButton(0) && _isScrolling)
            {
                var currPos = transform.position;
                Vector3 pos = _camera.ScreenToViewportPoint(Input.mousePosition) - _dragOrigin;
                Vector3 desirePos = _cameraDragOrigin - new Vector3(pos.x, pos.y, 0f) * panSpeed;
                Vector3 move = desirePos - currPos;
                var nextPos = currPos + new Vector3(move.x, move.y, 0f);
                transform.position = nextPos;
            }

            if (Math.Abs(Input.mouseScrollDelta.y) > .001f && !_isScrolling)
            {
                var cameraSize = _camera.orthographicSize;
                _camera.orthographicSize = Math.Clamp(cameraSize + Input.mouseScrollDelta.y, 5f, 15f); //TODO: make range parametrized
            }

            ClampPosition();
        }

        private void ClampPosition()
        {
            if (!_checkBounds)
                return;

            var pos = transform.position;
            transform.position = new Vector3(ClampX(pos), ClampY(pos), pos.z);
        }

        private bool IsInBounds(Vector2 pos)
        {
            var cameraHeight = _camera.orthographicSize;
            var cameraWidth = cameraHeight * _camera.aspect;
            return pos.x > _bounds.xMin + cameraWidth && pos.x < _bounds.xMax - cameraWidth
                                                      && pos.y > _bounds.yMin + cameraHeight &&
                                                      pos.y < _bounds.yMax - cameraHeight;
        }

        private float ClampX(Vector2 pos)
        {
            var cameraHeight = _camera.orthographicSize;
            var cameraWidth = cameraHeight * _camera.aspect;

            if (pos.x < _bounds.xMin + cameraWidth)
            {
                return _bounds.xMin + cameraWidth;
            }

            if (pos.x > _bounds.xMax - cameraWidth)
            {
                return _bounds.xMax - cameraWidth;
            }

            return pos.x;
        }

        private float ClampY(Vector2 pos)
        {
            var cameraHeight = _camera.orthographicSize;

            if (pos.y < _bounds.yMin + cameraHeight)
            {
                return _bounds.yMin + cameraHeight;
            }

            if (pos.y > _bounds.yMax - cameraHeight)
            {
                return _bounds.yMax - cameraHeight;
            }

            return pos.y;
        }
        
        public static bool IsPointerOverUIObject()
        {
            PointerEventData eventDataCurrentPosition = new PointerEventData(EventSystem.current);
            eventDataCurrentPosition.position = new Vector2(Input.mousePosition.x, Input.mousePosition.y);
            List<RaycastResult> results = new List<RaycastResult>();
            EventSystem.current.RaycastAll(eventDataCurrentPosition, results);
            foreach (RaycastResult r in results)
                if (r.gameObject.GetComponent<RectTransform>())
                    return true;

            return false;
        }
    }
}
