using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;

namespace Utilities.Helpers
{
    [RequireComponent(typeof(TMP_Text))]
    public class TextMeshProHyperLinkHolder : MonoBehaviour, IPointerClickHandler
    {
        private TMP_Text _textTMP;
        
        public void OnPointerClick(PointerEventData eventData) 
        {
            if (_textTMP == null)
                _textTMP = GetComponent<TMP_Text>();
            // If you are not in a Canvas using Screen Overlay, put your camera instead of null
            int linkIndex = TMP_TextUtilities.FindIntersectingLink(_textTMP, eventData.position, null); 
            if (linkIndex != -1) 
            {
                TMP_LinkInfo linkInfo = _textTMP.textInfo.linkInfo[linkIndex];
                Application.OpenURL(linkInfo.GetLinkID());
            }
        }
    }
}
