using System;
using UnityEngine.EventSystems;

namespace Utilities.Helpers
{
    public static class EventTriggerExtention
    {
        public static void AddEventListener(this EventTrigger trigger, EventTriggerType eventType, Action<BaseEventData> callback)
        {
            EventTrigger.Entry entry = new EventTrigger.Entry();
            entry.eventID = eventType;
            entry.callback = new EventTrigger.TriggerEvent();
            entry.callback.AddListener(new UnityEngine.Events.UnityAction<BaseEventData>(callback));
            trigger.triggers.Add(entry);
        }
    }
}
