﻿using System.Collections.Generic;
using UnityEngine;

namespace Utilities.Helpers
{
	public static class Bezier
	{

		public static Vector3 GetPoint(Vector3 p0, Vector3 p1, Vector3 p2, float t)
		{
			t = Mathf.Clamp01(t);
			float oneMinusT = 1f - t;
			return
				oneMinusT * oneMinusT * p0 +
				2f * oneMinusT * t * p1 +
				t * t * p2;
		}

		public static Vector3 GetFirstDerivative(Vector3 p0, Vector3 p1, Vector3 p2, float t)
		{
			return
				2f * (1f - t) * (p1 - p0) +
				2f * t * (p2 - p1);
		}

		public static Vector3 GetPoint(Vector3 p0, Vector3 p1, Vector3 p2, Vector3 p3, float t)
		{
			t = Mathf.Clamp01(t);
			float OneMinusT = 1f - t;
			return
				OneMinusT * OneMinusT * OneMinusT * p0 +
				3f * OneMinusT * OneMinusT * t * p1 +
				3f * OneMinusT * t * t * p2 +
				t * t * t * p3;
		}

		public static Vector3 GetFirstDerivative(Vector3 p0, Vector3 p1, Vector3 p2, Vector3 p3, float t)
		{
			t = Mathf.Clamp01(t);
			float oneMinusT = 1f - t;
			return
				3f * oneMinusT * oneMinusT * (p1 - p0) +
				6f * oneMinusT * t * (p2 - p1) +
				3f * t * t * (p3 - p2);
		}

		public static Vector3 GetPoint(List<Vector3> points, float t)
		{
			Vector3 result = Vector3.zero;
			if (points.Count > 10)
			{
				Debug.LogWarning("Too many points for Bezier curve");
				return result;
			}
			
			if (points.Count < 3)
			{
				Debug.LogWarning("Too few points for Bezier curve");
				return result;
			}

			t = Mathf.Clamp01(t);
			float oneMinusT = 1f - t;
			for (int i = 0; i < points.Count; ++i)
			{
				float mult1 = 1f;
				for (int j = 0; j < i; ++j)
				{
					mult1 *= t;
				}

				float mult2 = 1f;
				for (int j = points.Count - i - 1; j > 0; --j)
				{
					mult2 *= oneMinusT;
				}

				float koef = GetBinominal(i, points.Count - 1);
				result += points[i] * koef * mult1 * mult2;
			}

			return result;
		}

		public static float GetBinominal(int k, int n)
		{
			float result = 1;
			for (int i = 1; i <= k; i++)
			{
				result *= n - (k - i);
				result /= i;
			}

			return result;
		}
		
		public static List<Vector3> GetPath(List<Vector3> points, int count)
		{
			List<Vector3> result = new List<Vector3>();
			float step = 1f / count;
			for (float t = 0f; t <= 1f; t += step)
			{
				result.Add(GetPoint(points, t));
			}

			return result;
		}

		public static List<Vector3> GetTransformedByBasisPoints(List<Vector3> points, Vector3 p1, Vector3 p2)
		{
			var dir = p2 - p1;
			var angle = Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg;
			var scale = Vector3.one * Vector3.Distance(p1, p2);
			if (angle > 90 || angle < -90) //simple mirror by Y
				scale.y = -scale.y;
			var rotation = Quaternion.AngleAxis(angle, Vector3.forward);
			Matrix4x4 m = Matrix4x4.TRS(p1, rotation, scale);

			List<Vector3> result = new List<Vector3>();
			foreach (var p in points)
			{
				Vector3 pos = m.MultiplyPoint3x4(p);
				result.Add(pos);
			}
			return result;
		}
	}
}