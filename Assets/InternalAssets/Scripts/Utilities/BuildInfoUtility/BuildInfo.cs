﻿using UnityEditor;

namespace Utilities.BuildVersions
{
    internal static class BuildInfo
    {
#if UNITY_EDITOR 
#if UNITY_ANDROID
        public static string bundleVersionCode => PlayerSettings.Android.bundleVersionCode.ToString();
#elif UNITY_IOS
        public static string bundleVersionCode => PlayerSettings.iOS.buildNumber;
#elif UNITY_WEBGL
        public static string bundleVersionCode => "";
#else
            public static string bundleVersionCode => "";
#endif
        
        private static string bundleVersion => PlayerSettings.bundleVersion;
        

        public static string fullVersion => GetFullVersion(bundleVersion, bundleVersionCode);
#endif
        
#if ENABLE_DEV_BUILD && !ENABLE_CREATIVES_BUILD
        public static string GetFullVersion(string version, string build) => $"v{version}_b{build}d";
#elif ENABLE_CREATIVES_BUILD
        public static string GetFullVersion(string version, string build) => $"v{version}_b{build}c";
#else
        public static string GetFullVersion(string version, string build) => $"v{version}_b{build}r";
#endif
    }
}