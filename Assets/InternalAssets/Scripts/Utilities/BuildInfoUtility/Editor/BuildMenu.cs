﻿
#if UNITY_EDITOR
using UnityEditor;

namespace Utilities.BuildVersions
{
    internal sealed class BuildMenu : UnityEditor.Editor
    {
        /*[MenuItem("Build/Build Android")]
        public static void BuildAndroid(string outputPath, string bundleFileName)
        {
            // Set build options
            var buildPlayerOptions = new BuildPlayerOptions
            {
                locationPathName = outputPath,
                target = BuildTarget.Android,
                options = BuildOptions.None,
                targetGroup = BuildTargetGroup.Android,
                customBuildTarget = "Android",
                extraScriptingDefines = new[] { "MY_DEFINE" }
            };

            // Set APK file name
            PlayerSettings.Android.bundleVersionCode = "1";
            PlayerSettings.Android.bundleVersionName = "1.0.0";
            PlayerSettings.productName = "My Game";
            PlayerSettings.SetApplicationIdentifier(BuildTargetGroup.Android, "com.mycompany.mygame");
            PlayerSettings.SetScriptingDefineSymbolsForGroup(BuildTargetGroup.Android, "MY_DEFINE");
            PlayerSettings.SetVirtualRealitySupported(BuildTargetGroup.Android, false);
            PlayerSettings.SetApiCompatibilityLevel(BuildPipeline.GetBuildTargetGroup(BuildTarget.Android), ApiCompatibilityLevel.NET_2_0);

            PlayerSettings.SetPropertyString("AndroidAppBundleVersionCode", "1");
            PlayerSettings.SetPropertyString("AndroidAppBundleVersionName", "1.0.0");
            PlayerSettings.SetPropertyString("AndroidPackageName", "com.mycompany.mygame");
            PlayerSettings.SetPropertyString("AndroidTargetDevice", "armv7");
            PlayerSettings.SetPropertyString("AndroidArchitecture", "ARMv7");
            PlayerSettings.SetPropertyString("AndroidKeystoreName", "my.keystore");
            PlayerSettings.SetPropertyString("AndroidKeystorePass", "mykeystorepassword");
            PlayerSettings.SetPropertyString("AndroidKeyaliasName", "mykeyalias");
            PlayerSettings.SetPropertyString("AndroidKeyaliasPass", "mykeyaliaspassword");
            PlayerSettings.SetPropertyInt("AndroidMinSdkVersion", 21);
            PlayerSettings.SetPropertyInt("AndroidTargetSdkVersion", 30);
            PlayerSettings.SetPropertyInt("AndroidVersionCode", 1);
            PlayerSettings.SetPropertyString("AndroidVersionName", "1.0.0");
            PlayerSettings.SetPropertyString("AndroidGradleBuildType", "release");
            PlayerSettings.SetPropertyString("AndroidGradleBuildOutputPath", $"{outputPath}/{bundleFileName}.apk");

            // Build the project
            BuildPipeline.BuildPlayer(buildPlayerOptions);
        }*/
    }
}
#endif