
#if UNITY_EDITOR && (UNITY_ANDROID || UNITY_IOS)
using System;
using System.IO;
using System.Text.RegularExpressions;
using UnityEngine;
using Internal;
using UnityEditor;
using UnityEditor.Build;
using UnityEditor.Build.Reporting;
using UnityEditor.Callbacks;
using System.Diagnostics;
using Debug = UnityEngine.Debug;

namespace Utilities.BuildVersions
{
    [InitializeOnLoad]
    internal sealed class IncrementBuildVersionPreprocess : IPostprocessBuildWithReport, IPreprocessBuildWithReport
    {
        public int callbackOrder => default;

        [MenuItem("Tools/Build/Increase Both Versions")]
        private static void IncreaseBothVersions()
        {
            IncreaseRevision();
            IncreasePlatformVersion();
        }
        
        [MenuItem("Tools/Build/Increase Major Version")]
        private static void IncreaseMajor() => IncrementVersion(new System.Version(1, 0, 0, 0));

        [MenuItem("Tools/Build/Increase Minor Version")]
        private static void IncreaseMinor() => IncrementVersion(new System.Version(0, 1, 0, 0));
 
        [MenuItem("Tools/Build/Increase Build Version")]
        private static void IncreaseBuild() => IncrementVersion(new System.Version(0, 0, 1, 0));
        
        [MenuItem("Tools/Build/Increase Current Revision Version")]
        private static void IncreaseRevision() => IncrementVersion(new System.Version(0, 0, 0, 1));
        
        [MenuItem("Tools/Build/Increase Platform Version")]
        private static void IncreasePlatformVersion()
        {
#if UNITY_ANDROID
            PlayerSettings.Android.bundleVersionCode += 1;
#elif UNITY_IOS
            PlayerSettings.iOS.buildNumber = (int.Parse(PlayerSettings.iOS.buildNumber) + 1).ToString();
#endif
        }

        public void OnPostprocessBuild(BuildReport report)
        {
            //IncreaseRevision(buildPath);
            
            switch (report.summary.platform)
            {
                case BuildTarget.Android:
                {
                    const string projectName = "bank-empire";
#if ENABLE_DEV_BUILD && !ENABLE_CREATIVES_BUILD
                    const string versionType = "development";
#elif ENABLE_CREATIVES_BUILD
                    const string versionType = "creatives";
#else
                    const string versionType = "release";
#endif

                    var filePath = report.summary.outputPath;
                    var fileExtension = EditorUserBuildSettings.buildAppBundle ? "aab" : "apk";
                    var newFileName = $"{projectName}_v{BuildRuntimeInfo.Instance.version}_b{BuildRuntimeInfo.Instance.build}_{versionType}.{fileExtension}";

                    RenameAndOverwriteApk(filePath, newFileName);
                    break;
                }
            }
        }
        
        public void OnPreprocessBuild(BuildReport report)
        {
#if ENABLE_DEV_BUILD && !ENABLE_CREATIVES_BUILD
            const string versionType = "Development";
#elif ENABLE_CREATIVES_BUILD
            const string versionType = "Creatives";
#else
            const string versionType = "Release";
#endif
            
            var shouldIncrement = EditorUtility.DisplayDialog(
                "Increment Version?",
                $"Current: {PlayerSettings.bundleVersion} [{versionType}]",
                "Yes",
                "No"
            );
 
            if (shouldIncrement)
            {
                IncreaseBothVersions();
                
                // Sending the tag to git
                RunGit($"git tag {BuildInfo.fullVersion} {GetLastCommitId()}");
            }

            WriteVersion();
        }

        private static void IncrementVersion(System.Version versionIncrement)
        {
            var version = new System.Version(PlayerSettings.bundleVersion);
            var newVersion = new System.Version(
                version.Major + versionIncrement.Major,
                version.Minor + versionIncrement.Minor,
                version.Build + versionIncrement.Build,
                version.Revision + versionIncrement.Revision
            );
            
            PlayerSettings.bundleVersion = newVersion.ToString();
        }
        
        [DidReloadScripts]
        private static void WriteVersion()
        {
            var settings = BuildRuntimeInfo.Instance;
            if (settings != null)
            {
                settings.version = BuildInfo.bundleVersion;
                settings.build = BuildInfo.bundleVersionCode;
                EditorUtility.SetDirty(settings);
                AssetDatabase.SaveAssetIfDirty(settings);
            }
        }

        private static void RenameAndOverwriteApk(string sourceFilePath, string newFileName)
        {
            var fileName = Path.GetFileName(sourceFilePath);
            var newFilePath = Path.Combine(Path.GetDirectoryName(sourceFilePath), newFileName);

            if (File.Exists(newFilePath))
            {
                File.Delete(newFilePath);
            }

            File.Move(sourceFilePath, newFilePath);

            Debug.Log($"APK renamed from {fileName} to {newFileName}");

            // Handle OBB files
            var sourceDirectory = Path.GetDirectoryName(sourceFilePath);
            var obbFiles = Directory.GetFiles(sourceDirectory, "*.obb");

            foreach (var obbFile in obbFiles)
            {
                var obbFileName = Path.GetFileName(obbFile);
                var obbFileExtension = Path.GetExtension(obbFile);
                var newObbFilePath = Path.Combine(sourceDirectory, newFileName + obbFileExtension);

                if (File.Exists(newObbFilePath))
                {
                    File.Delete(newObbFilePath);
                }

                File.Move(obbFile, newObbFilePath);

                Debug.Log($"OBB file renamed from {obbFileName} to {newFileName + obbFileExtension}");
            }
        }

        private static string GetLastCommitId() => RunGit("git log -1 --format=\"%H\"");

        private static string RunGit(string command)
        {
            var repoPath = Application.dataPath.Replace("/Assets", "");

            var process = new Process()
            {
                StartInfo = new ProcessStartInfo()
                {
                    FileName = "cmd.exe",
                    Arguments = $"/C cd {repoPath} && {command}",
                    UseShellExecute = false,
                    RedirectStandardOutput = true,
                    CreateNoWindow = true
                }
            };
            
            process.Start();
            var output = process.StandardOutput.ReadToEnd();
            process.WaitForExit();

            // Returning the ID of the last commit without newline characters
            return output.Trim();
        }

        /*private static void IncrementBuild(string buildPath)
        {
            var settingsPath = Path.GetDirectoryName(Application.dataPath);
            settingsPath = Path.Combine(settingsPath, "ProjectSettings");
            settingsPath = Path.Combine(settingsPath, "ProjectSettings.asset");

            if (!File.Exists(settingsPath))
            {
                Debug.LogError("Couldn't find project settings file.");
                return;
            }

            var lines = File.ReadAllLines(settingsPath);

            if (!lines[0].StartsWith("%YAML"))
            {
                Debug.LogError("Project settings file needs to be serialized as a text asset. (Check 'Project Settings->Editor')");
                return;
            }

            const string pattern = @"^(\s*iPhoneBundleVersion:\s*)([\d\.]+)$";
            var success = false;

            System.Version version = null;

            for (var i = 0; i < lines.Length; i++)
            {
                var line = lines[i];

                if (!Regex.IsMatch(line, pattern))
                {
                    continue;
                }

                var match = Regex.Match(line, pattern);

                version = new System.Version(match.Groups[2].Value);

                var major = version.Major < 0 ? 0 : version.Major;
                var minor = version.Minor < 0 ? 0 : version.Minor;
                var build = version.Build < 0 ? 0 : version.Build;
                var revision = version.Revision < 0 ? 0 : version.Revision;

                version = new System.Version(major, minor, build, revision + 1);

                line = match.Groups[1].Value + version;

                lines[i] = line;
                success = true;

                break;
            }

            if (!success)
            {
                Debug.LogError("Couldn't find bundle version in ProjectSettings.asset");
                return;
            }

            File.WriteAllLines(settingsPath, lines);

            Debug.Log("Build version: " + version);

#if PLIST_CS
            var plistPath = Path.Combine(buildPath, "Info.plist");

            if (!File.Exists(plistPath))
            {
                Debug.LogWarning("Couldn't find Info.plist in build output.");
                return;
            }

            // modify plist
            var plist = (Dictionary<string, object>)Plist.readPlist(plistPath);

            plist["CFBundleShortVersionString"] = version.Major.ToString() + "." + version.Minor.ToString() + "." +
                version.Build.ToString();
            plist["CFBundleVersion"] = version.Revision.ToString();

            Plist.writeXml(plist, plistPath);
#endif
        }*/
    }
}
#endif