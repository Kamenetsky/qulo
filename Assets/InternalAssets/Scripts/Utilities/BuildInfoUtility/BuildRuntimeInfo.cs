
using Core.Scriptable;
using UnityEngine;

namespace Utilities.BuildVersions
{
    [CreateAssetMenu(fileName = "BuildRuntimeInfoSettings", menuName = "Utils/Build Runtime Info Settings")]
    internal sealed class BuildRuntimeInfo : DescriptionBaseSO
    {
        public static BuildRuntimeInfo Instance => _instance ??= LoadInfo();
        private static BuildRuntimeInfo _instance;
        
        public string fullVersion => BuildInfo.GetFullVersion(version, build);
 
        public string version;
        public string build;

        private static BuildRuntimeInfo LoadInfo()
        {
            var result = Resources.Load<BuildRuntimeInfo>("BuildRuntimeInfoSettings");
            if (result == null)
            {
                Debug.LogError("You need create asset file before build from context menu \"Assets/Create/Build runtime info settings\"");
            }
 
            return result;
        }
    }
}