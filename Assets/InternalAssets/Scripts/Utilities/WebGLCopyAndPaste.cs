﻿using UnityEngine;
using UnityEngine.EventSystems;
using System.Runtime.InteropServices;

namespace Utilities
{
    public class WebGLCopyAndPasteAPI
    {
#if UNITY_WEBGL

        [DllImport("__Internal")]
        private static extern void initWebGLCopyAndPaste(StringCallback cutCopyCallback, StringCallback pasteCallback);

        [DllImport("__Internal")]
        private static extern void passCopyToBrowser(string str);

        delegate void StringCallback(string content);


        [RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.AfterAssembliesLoaded)]
        private static void Init()
        {
            if (!Application.isEditor)
            {
                initWebGLCopyAndPaste(GetClipboard, ReceivePaste);
            }
        }

        private static void SendKey(string baseKey)
        {
            string appleKey = "%" + baseKey;
            string naturalKey = "^" + baseKey;

            var currentObj = EventSystem.current.currentSelectedGameObject;
            if (currentObj == null)
            {
                return;
            }

            var input = currentObj.GetComponent<UnityEngine.UI.InputField>();
            if (input != null)
            {
                input.ProcessEvent(Event.KeyboardEvent(naturalKey));
                input.ProcessEvent(Event.KeyboardEvent(appleKey));
                return;
            }

            var inputTmp = currentObj.GetComponent<TMPro.TMP_InputField>();
            if (input != null)
            {
                inputTmp.ProcessEvent(Event.KeyboardEvent(naturalKey));
                inputTmp.ProcessEvent(Event.KeyboardEvent(appleKey));
            }
        }

        [AOT.MonoPInvokeCallback(typeof(StringCallback))]
        private static void GetClipboard(string key)
        {
            SendKey(key);
            passCopyToBrowser(GUIUtility.systemCopyBuffer);
        }

        [AOT.MonoPInvokeCallback(typeof(StringCallback))]
        private static void ReceivePaste(string str)
        {
            GUIUtility.systemCopyBuffer = str;
        }

#endif
    }
}
