
using UnityEngine;

namespace UI
{
    internal sealed class UIFactory : MonoBehaviour
    {
        public T CreateWindow<T>(string windowPrefabPath) where T : UIWindow
        {
            var windowPrefab = Resources.Load<T>(windowPrefabPath);
            var windowInstance = Instantiate(windowPrefab);
            
            return windowInstance;
        }

        public T CreatePopup<T>(string popupPrefabPath) where T : UIPopup
        {
            var popupPrefab = Resources.Load<T>(popupPrefabPath);
            var popupInstance = Instantiate(popupPrefab);
            
            return popupInstance;
        }
    }
}