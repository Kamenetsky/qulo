
using System.Text.RegularExpressions;

internal static class InputValidator
{
    public static bool ValidateEmail(string email)
    {
        // A simple email validation check
        var emailRegex = new Regex(@"^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$");
        return emailRegex.IsMatch(email);
    }

    public static bool ValidatePassword(string password)
    {
        // A simple password validation check (at least 8 characters, one uppercase, one lowercase, and one number)
        var passwordRegex = new Regex(@"^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)[a-zA-Z\d]{8,}$");
        return passwordRegex.IsMatch(password);
    }
}