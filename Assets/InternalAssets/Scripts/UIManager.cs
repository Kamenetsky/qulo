
using System.Collections.Generic;
using UnityEngine;

namespace UI
{
    internal sealed class UIManager : MonoBehaviour
    {
        [SerializeField] private UIFactory uiFactory;

        private Dictionary<string, UIWindow> _openWindows = new();
        private readonly Stack<UIPopup> _openPopups = new();

        public T OpenWindow<T>(string windowPrefabPath) where T : UIWindow
        {
            if (_openWindows.ContainsKey(windowPrefabPath))
            {
                _openWindows[windowPrefabPath].Show();
                return _openWindows[windowPrefabPath] as T;
            }

            var window = uiFactory.CreateWindow<T>(windowPrefabPath);
            _openWindows.Add(windowPrefabPath, window);
            
            return window;
        }

        public void CloseWindow(string windowPrefabPath)
        {
            if (_openWindows.ContainsKey(windowPrefabPath))
            {
                _openWindows[windowPrefabPath].Hide();
                _openWindows.Remove(windowPrefabPath);
            }
        }

        public T OpenPopup<T>(string popupPrefabPath) where T : UIPopup
        {
            var popup = uiFactory.CreatePopup<T>(popupPrefabPath);
            _openPopups.Push(popup);
            
            return popup;
        }

        public void ClosePopup()
        {
            if (_openPopups.Count > 0)
            {
                var popup = _openPopups.Pop();
                popup.Hide();
            }
        }
    }
}