﻿
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;

namespace Utilities.TweenAnimations
{
    internal sealed class TweenAnimationTimelineEditorWindow : EditorWindow
    {
        private TweenAnimationController _selectedController;
        private Vector2 _scrollPosition;
        private float _previewTime;
        private bool _isPreviewing;

        [MenuItem("Window/Tween Animation Timeline Editor")]
        public static void ShowWindow()
        {
            GetWindow<TweenAnimationTimelineEditorWindow>("Tween Animation Timeline");
        }

        private void OnGUI()
        {
            _selectedController = EditorGUILayout.ObjectField("Tween Animation Controller", _selectedController, typeof(TweenAnimationController), true) as TweenAnimationController;

            if (_selectedController == null)
            {
                EditorGUILayout.HelpBox("Select a Tween Animation Controller to edit its animations.", MessageType.Info);
                return;
            }

            EditorGUILayout.Space();

            _scrollPosition = EditorGUILayout.BeginScrollView(_scrollPosition);

            EditorGUI.BeginChangeCheck();

            List<BaseTweenAnimation> animations = _selectedController.GetAnimations();

            for (int i = 0; i < animations.Count; i++)
            {
                EditorGUILayout.BeginHorizontal();

                EditorGUILayout.LabelField($"Animation {i + 1}:", GUILayout.Width(80));

                animations[i] = EditorGUILayout.ObjectField(animations[i], typeof(BaseTweenAnimation), true) as BaseTweenAnimation;

                if (GUILayout.Button("Remove", GUILayout.Width(80)))
                {
                    animations.RemoveAt(i);
                    i--;
                }

                EditorGUILayout.EndHorizontal();
            }

            if (EditorGUI.EndChangeCheck())
            {
                Undo.RecordObject(_selectedController, "Modify Tween Animation Controller");
                EditorUtility.SetDirty(_selectedController);
            }

            EditorGUILayout.Space();

            if (GUILayout.Button("Add New Animation"))
            {
                Undo.RecordObject(_selectedController, "Add New Tween Animation");
                _selectedController.GetAnimations().Add(null);
                EditorUtility.SetDirty(_selectedController);
            }

            EditorGUILayout.Space();

            EditorGUILayout.BeginHorizontal();

            if (GUILayout.Button("Start Preview"))
            {
                StartPreview();
            }

            if (GUILayout.Button("Stop Preview"))
            {
                StopPreview();
            }

            EditorGUILayout.EndHorizontal();

            EditorGUILayout.EndScrollView();
        }

        private void StartPreview()
        {
            /*if (_selectedController == null || _isPreviewing) return;

            _isPreviewing = true;
            _previewTime = 0f;

            foreach (var animation in _selectedController.GetAnimations().Where(animation => animation != null))
            {
                animation.SaveState();
            }*/
        }

        private void StopPreview()
        {
            /*if (_selectedController == null || !_isPreviewing) return;

            _isPreviewing = false;

            foreach (var animation in _selectedController.GetAnimations().Where(animation => animation != null))
            {
                animation.RestoreState();
            }*/
        }

        private void Update()
        {
            /*if (_selectedController != null && _isPreviewing)
            {
                _previewTime += Time.deltaTime;

                foreach (var animation in _selectedController.GetAnimations().Where(animation => animation != null))
                {
                    animation.UpdatePreview(_previewTime);
                }

                Repaint();
            }*/
        }
    }
}