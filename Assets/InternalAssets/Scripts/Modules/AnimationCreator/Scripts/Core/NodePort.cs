﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace AnimationCreator.Core
{
    public enum NodePortCapacity
    {
        Multi,
        Single
    }
    
    [System.Serializable]
    public abstract class NodePort
    {
        public string portName;
        public string id;
        public NodePortCapacity capacity = NodePortCapacity.Multi;
        [SerializeField] private List<string> connectedData;

        public List<string> ConnectionData
        {
            get
            {
                if (connectedData == null) connectedData = new List<string>();
                return connectedData;
            }
        }

        #region Connections Control

        public bool TryAddConnection(string newNodeData)
        {
            switch (capacity)
            {
                case NodePortCapacity.Multi:
                    if (!ConnectionData.Contains(newNodeData))
                    {
                        ConnectionData.Add(newNodeData);
                        return true;
                    }
                    return false;
                case NodePortCapacity.Single:
                    if (!ConnectionData.Contains(newNodeData))
                    {
                        ConnectionData.Clear();
                        ConnectionData.Add(newNodeData);
                        return true;
                    }
                    return false;
            }

            return false;
        }

        
        public bool TryRemoveConnection(string nodeToRemove)
        {
            if (ConnectionData.Contains(nodeToRemove))
            {
                ConnectionData.Remove(nodeToRemove);
                return true;
            }

            return false;
        }

        #endregion

        #region Setup

        protected NodePort()
        {
            GenerateIdIfNotExisted();
        }

        public void GenerateIdIfNotExisted()
        {
            if (string.IsNullOrEmpty(id))
            {
                id = Guid.NewGuid().ToString();
            }
        }

        #endregion

        #region Control


        #endregion
        
    }

    [System.Serializable]
    public class NodePortInput : NodePort
    {
        
    }
    
    [System.Serializable]
    public class NodePortOutput : NodePort
    {
        
    }
    
    [AttributeUsage(AttributeTargets.Field)]
    public class NodePortAttribute : Attribute
    {
        public enum NameType
        {
            NormalName,
            GetMethod
        }

        public string MainName { get; }
        public NameType Type { get; }
        public NodePortCapacity Capacity { get; }
        
        public NodePortAttribute(string mainName,NameType nameType = NameType.NormalName,NodePortCapacity capacity = NodePortCapacity.Multi)
        {
            Type = nameType;
            MainName = mainName;
            Capacity = capacity;
        }
    }
    
}