using System;
using System.Collections.Generic;
using System.Linq;
using AnimationCreator.Nodes;
using UnityEngine;

namespace AnimationCreator.Core
{
    public class AnimationPlayer : MonoBehaviour
    {
        public event System.Action<string> OnPlayEvent; 
        public event System.Action OnNodesChange; 
        
        [SerializeField] private string currentSelectedID = "";
        [SerializeField] private bool activateGameObjectOnPlay = true;
        [SerializeReference] private List<BaseNode> nodes;

        public bool Initialized { get; private set; }
        private readonly Dictionary<string, BaseNode> _dictionary = new Dictionary<string, BaseNode>();

        private Action _onCompleteCallback;
        
        //-----------------------GETTER----------------------------
        public string CurrentSelectedID
        {
            get => currentSelectedID;
            set
            {
                currentSelectedID = value;
                OnNodesChange?.Invoke();
            }
        }
        public List<BaseNode> GetNodes()
        {
            if (nodes == null) nodes = new List<BaseNode>();
            return nodes;
        }

        //-----------------------UNITY----------------------------
        private void Awake()
        {
        }
        
        private void Start()
        {
            Initialize();
        }
                
        private void OnDisable()
        {
            ClearAnimation();
        }

        //-----------------------SETUP----------------------------
        private void Initialize()
        {
            if(Initialized) return;
            Initialized = true;
            var orderedNodes = GetNodes().OrderByDescending(x => x.GetPriority()).ToList();
            
            foreach (var node in orderedNodes)
            {
                node.Initialize(this);
                _dictionary.Add(node.ID,node);
            }
            
            foreach (var node in orderedNodes)
            {
                node.OnInitializeCompleted();
            }
        }




        //-----------------------PLAY----------------------------
        public void ClearAnimation()
        {
            foreach (var node in nodes)
            {
                node.ClearNode();
            }
        }
        
        public void Play(string eventName)
        {   
            if(activateGameObjectOnPlay && !gameObject.activeSelf) gameObject.SetActive(true);
            Initialize();
            ClearAnimation();
            OnPlayEvent?.Invoke(eventName);
        }
        
        public void Play(string eventName,Action onComplete)
        {   
            if(activateGameObjectOnPlay && !gameObject.activeSelf) gameObject.SetActive(true);
            Initialize();
            ClearAnimation();
            OnPlayEvent?.Invoke(eventName);
            _onCompleteCallback = onComplete;
        }
        
        //-----------------------CONTROL GRAPH----------------------------
        public bool TryAddNode(BaseNode node)
        {
            GetNodes().Add(node);
            OnNodesChange?.Invoke();
            return true;
        }
        
        public bool TryRemoveNode(BaseNode node)
        {
            if (GetNodes().Contains(node))
            {
                GetNodes().Remove(node);
                OnNodesChange?.Invoke();
                return true;
            }
            return false;
        }
        
        public bool GetNodeByID(string id,out BaseNode node)
        {
            return _dictionary.TryGetValue(id, out node);
        }

        public void OnCompleteAnimationGroup(AnimationGroupNode animationGroupNode)
        {
            _onCompleteCallback?.Invoke();
        }
    }
}
