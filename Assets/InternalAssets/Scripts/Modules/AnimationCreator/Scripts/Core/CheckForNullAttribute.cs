﻿using System;
using UnityEngine;

namespace AnimationCreator.Core
{
#if UNITY_EDITOR
    
    using UnityEditor;

    [CustomPropertyDrawer(typeof(CheckForNullAttribute))]
    public class CheckForNullAttributePropertyDrawer : PropertyDrawer
    {
        private GUIStyle _titleStyle;

        private const float BoxHeight = 40; 
        
        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            if (_titleStyle == null)
            {
                _titleStyle = new GUIStyle(GUI.skin.box)
                {
                    fontSize = 15,
                    fontStyle = FontStyle.Bold,
                    alignment = TextAnchor.MiddleCenter
                };
                _titleStyle.alignment = TextAnchor.MiddleCenter;
            }
            position.height = EditorGUIUtility.singleLineHeight;
            if (property.objectReferenceValue == null)
            {
                var helpBoxRect = new Rect(position);
                helpBoxRect.y += position.height + 5;
                helpBoxRect.height = BoxHeight;
                EditorGUI.HelpBox(helpBoxRect,"You need to Assign This Field",MessageType.Warning);
            }
            EditorGUI.PropertyField(position, property,new GUIContent(property.displayName));
        }


        public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
        {
            float normalHeight = EditorGUIUtility.singleLineHeight;
            if (property.objectReferenceValue == null)
            {
                normalHeight += EditorGUIUtility.singleLineHeight + 5 + BoxHeight;
            }

            return normalHeight;
        }
        
    }
    
#endif
    
    [AttributeUsage(AttributeTargets.Field)]
    public class CheckForNullAttribute : PropertyAttribute
    {
    }
}