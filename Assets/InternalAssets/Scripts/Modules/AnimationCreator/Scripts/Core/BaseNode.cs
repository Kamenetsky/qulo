﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace AnimationCreator.Core
{
    [System.Serializable]
    public abstract class BaseNode
    {
        public event System.Action OnNodeDelete; 
        public event System.Action<bool> OnIsPlayingChanged; 
        protected const string IconsPath = "Assets/InternalAssets/Scripts/Modules/AnimationCreator/Editor/Gizmos/";

        [SerializeField] protected string nodeName = "NodeName";
        [SerializeField,HideInInspector] protected string id;
        [SerializeField,HideInInspector] protected Vector2 nodePosition = Vector2.zero;

        public bool IsInitialized { get; protected set; } = false;
        private bool _isPlaying;

        public bool IsPlaying
        {
            get => _isPlaying;
            protected set
            {
                if (_isPlaying == value) return;
                _isPlaying = value;
                OnIsPlayingChanged?.Invoke(_isPlaying);
            }
        }
        protected AnimationPlayer Player { get; set; }

        //-----------------------GETTER--------------------------
        public string ID => id;
        public string NodeName
        {
            get => nodeName;
            set => nodeName = value;
        }

        public Vector2 NodePosition
        {
            get => nodePosition;
            set => nodePosition = value;
        }
        
        //-----------------------SETUP--------------------------
        #region Setup

        protected BaseNode()
        {
            GenerateIdIfNotExisted();
        }
        
        public virtual void Initialize(AnimationPlayer player)
        {
            if(IsInitialized) return;
            Player = player;
            IsInitialized = true;
        }

        public virtual void OnInitializeCompleted()
        {
            
        }
        
        public void GenerateIdIfNotExisted()
        {
            if (string.IsNullOrEmpty(id))
            {
                id = Guid.NewGuid().ToString();
            }
        }
        

        #endregion
        
        //-----------------------VIRTUAL--------------------------
        public void GetAllTweens(ref List<TweenData> allTweens)
        {
            if (this is TweenData t)
            {
                if (!allTweens.Contains(t))
                {
                    allTweens.Add(t);
                }
            }

            var nodePortOutput = GetMainOutput();
            if(nodePortOutput == null) return;
            foreach (var s in nodePortOutput.ConnectionData)
            {
                if (Player.GetNodeByID(s,out BaseNode node))
                {
                    node.GetAllTweens(ref allTweens);
                }

            }
        }

        public virtual float GetLength()
        {
            return 0;
        }
        
        public virtual int GetPriority()
        {
            return 0;
        }
        
        public abstract NodePortOutput GetMainOutput();
        
        public void OnBeforeDelete()
        {
            OnNodeDelete?.Invoke();
        }
        
        protected void PerformOutputPort(NodePortOutput portOutput)
        {
            foreach (var s in portOutput.ConnectionData)
            {
                if (Player.GetNodeByID(s, out BaseNode node))
                {
                    node.EnterNode(portOutput.portName);
                }
            }
        }

        public virtual void EnterNode(string name)
        {
        }
        
        public virtual void ClearNode()
        {
        }
        
        public virtual string GetNodeTitle()
        {
            return "TITLE";
        }
        
        public virtual string GetTitleExtra()
        {
            return "";
        }
        
        public virtual string GetIconPath()
        {
            return "TITLE";
        }
        
        public virtual string GetNodeClass()
        {
            return "node-class";
        }
        
        public virtual string GetSearchName()
        {
            return GetNodeTitle();
        }
        
    }
}