﻿using AnimationCreator.Nodes;
using DG.Tweening;
using UnityEngine;

namespace AnimationCreator.Core
{
    [System.Serializable]
    public abstract class TweenData : BaseNode
    {
        [SerializeField,HideInInspector,NodePort("")] protected NodePortInput enterPort;
        [SerializeField,HideInInspector,NodePort("Complete")] protected NodePortOutput completePort;
        [Header("Settings")]
        [SerializeField] protected float duration = .5f;
        [SerializeField] protected float delay = 0f;
        [SerializeField] protected Ease ease = Ease.InOutSine;
        [SerializeField] protected bool timeIndependent = false;
        [SerializeField] protected bool setFromImmediately = false;
        [SerializeField] protected bool setFromOnPlay = true;
        [Header("Loop")] 
        [SerializeField] protected int loopCount = 0;
        [SerializeField] protected LoopType loopType = LoopType.Yoyo;

        protected Tween Tween;
        protected AnimationGroupNode ParentGroupNode;

        
        //----------------------------------NODE CONTROL-------------------------

        public void SetParentGroupNode(AnimationGroupNode parent)
        {
            //ParentGroupNodeID = parent.ID;
            ParentGroupNode = parent;
        }

        protected bool GetTimeIndependent()
        {
            return ParentGroupNode != null && ParentGroupNode.IsInitialized
                ? ParentGroupNode.TimeIndependent
                : timeIndependent;
        }
        
        public override void EnterNode(string name)
        {
            base.EnterNode(name);
            Tween = Animate().SetLoops(loopCount,loopType).OnComplete(OnAnimationCompleted);
            IsPlaying = true;
        }

        public override void ClearNode()
        {
            base.ClearNode();
            ClearAnimation();
            IsPlaying = false;
        }

        public virtual void ClearAnimation()
        {
            Tween?.Kill();
            IsPlaying = false;
        }

        public override int GetPriority()
        {
            return 10;
        }

        public virtual void OnAnimationCompleted()
        {
            PerformOutputPort(completePort);
            ParentGroupNode?.CompletePlayTweenNode(this);
            IsPlaying = false;
        }

        public override NodePortOutput GetMainOutput()
        {
            return completePort;
        }

        public override float GetLength()
        {
            return delay + duration;
        }

        //----------------------------------ABSTRACTS-------------------------
        public abstract Tween Animate();

        public virtual void OnStartPlay()
        {
            IsPlaying = true;
        }

    }
}