﻿using AnimationCreator.Core;
using DG.Tweening;
using UnityEngine;

namespace AnimationCreator.Animations
{
    [System.Serializable]
    public class RotateTween : TweenData
    {
        //-----------------------References-------------------------
        [Header("Rotate Tween")]
        [SerializeField,CheckForNull] private Transform target;
        [SerializeField] private Vector3 from = Vector3.zero;
        [SerializeField] private Vector3 to = Vector3.one;
        [SerializeField] private RotateMode rotateMode;
        [SerializeField] private bool local = true;
        
        public override Tween Animate()
        {
            if (local)
            {
                return target.DOLocalRotate(to, duration,rotateMode).SetEase(ease).From(from, setFromImmediately)
                    .SetDelay(delay).SetUpdate(GetTimeIndependent());
            }
            return target.DORotate(to, duration,rotateMode).SetEase(ease).From(from, setFromImmediately)
                .SetDelay(delay).SetUpdate(GetTimeIndependent());
        }

        
        public override void OnStartPlay()
        {
            if (setFromOnPlay)
            {
                target.rotation = Quaternion.Euler(from);
            }
        }

        
        //-----------------------Update Graph Node-------------------------
        public override string GetNodeTitle()
        {
            return "Rotate";
        }

        public override string GetTitleExtra()
        {
            if (local)
            {
                return " Local";
            }
            return "";
        }

        public override string GetIconPath()
        {
            return IconsPath + "Rotate.png";
        }
    }
}