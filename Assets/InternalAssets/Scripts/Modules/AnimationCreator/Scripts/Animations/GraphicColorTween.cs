﻿using AnimationCreator.Core;
using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

namespace AnimationCreator.Animations
{
    [System.Serializable]
    public class GraphicColorTween : TweenData
    {
        //-----------------------References-------------------------
        [Header("Graphic Color Tween")]
        [SerializeField,CheckForNull] private Graphic target;
        [SerializeField] private Color from = Color.white;
        [SerializeField] private Color to = Color.red;
        
        public override Tween Animate()
        {
            return target.DOColor(to,duration).From(from,setFromImmediately).SetEase(ease).SetEase(ease)
                .SetDelay(delay).SetUpdate(GetTimeIndependent());
        }

        public override void OnStartPlay()
        {
            if (setFromOnPlay)
            {
                target.color = from;
            }
        }
        
        //-----------------------Update Graph Node-------------------------
        public override string GetNodeTitle()
        {
            return "Graphic Color";
        }

        public override string GetIconPath()
        {
            return IconsPath + "Color.png";
        }
    }
}