﻿using AnimationCreator.Core;
using DG.Tweening;
using UnityEngine;

namespace AnimationCreator.Animations
{
    [System.Serializable]
    public class SpriteRendererColorTween : TweenData
    {
        //-----------------------References-------------------------
        [Header("SpriteRenderer Color Tween")]
        [SerializeField,CheckForNull] private SpriteRenderer target;
        [SerializeField] private Color from = Color.white;
        [SerializeField] private Color to = Color.red;
        
        public override Tween Animate()
        {
            return target.DOColor(to,duration).From(from,setFromImmediately).SetEase(ease).SetEase(ease)
                .SetDelay(delay).SetUpdate(GetTimeIndependent());
        }

        public override void OnStartPlay()
        {
            if (setFromOnPlay)
            {
                target.color = from;
            }
        }

        
        //-----------------------Update Graph Node-------------------------
        public override string GetNodeTitle()
        {
            return "Sprite Color";
        }

        public override string GetIconPath()
        {
            return IconsPath + "Color.png";
        }
    }
}