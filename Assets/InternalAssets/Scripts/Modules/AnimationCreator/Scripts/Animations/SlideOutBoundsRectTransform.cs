﻿using AnimationCreator.Core;
using DG.Tweening;
using UnityEngine;

namespace AnimationCreator.Animations
{
    [System.Serializable]
    public class SlideOutBoundsRectTransform : TweenData
    {
        //-----------------------References-------------------------
        [Header("Slide out Tween")]
        [SerializeField,CheckForNull] private RectTransform target;
        [SerializeField] private Vector3 from = Vector3.zero;
        [SerializeField] private Vector2 direction = Vector2.up;
        [SerializeField] private bool resetScale = true;
        [SerializeField] private bool resetRotation = true;

        private Canvas _canvas;
        private Vector2 _targetSize;
        private Vector2 _canvasSize;
        
        public override void Initialize(AnimationPlayer player)
        {
            base.Initialize(player);
            _canvas = target.GetComponentInParent<Canvas>();
            _targetSize = target.sizeDelta;
            _canvasSize = _canvas.renderingDisplaySize;
        }
        
        public override void OnStartPlay()
        {
            if (setFromOnPlay)
            {
                target.anchoredPosition3D = from;
            }
        }


        public override Tween Animate()
        {
            if(resetScale) target.localScale = Vector3.one;
            if(resetRotation) target.localRotation = Quaternion.identity;
            
            var newPos = Vector2.Scale(_canvasSize/2 + (_targetSize/2f),direction);
            
            return target.DOAnchorPos3D(newPos, duration).SetEase(ease).From(from, setFromImmediately).SetEase(ease)
                .SetDelay(delay).SetUpdate(GetTimeIndependent());
        }

        //-----------------------Update Graph Node-------------------------
        public override string GetNodeTitle()
        {
            return "Slide";
        }

        public override string GetIconPath()
        {
            return IconsPath + "Move.png";
        }
    }
}