﻿using AnimationCreator.Core;
using DG.Tweening;
using UnityEngine;

namespace AnimationCreator.Animations
{
    [System.Serializable]
    public class CanvasGroupFadeTween : TweenData
    {
        //-----------------------References-------------------------
        [Header("Fade Tween")]
        [SerializeField,CheckForNull] private CanvasGroup target;
        [SerializeField,Range(0,1)] private float from = 0;
        [SerializeField,Range(0,1)] private float to = 1;

        //-----------------------Control Animation-------------------------
        public override Tween Animate()
        {
            return target.DOFade(to, duration).SetEase(ease).From(from, setFromImmediately).SetEase(ease)
                .SetDelay(delay).SetUpdate(GetTimeIndependent());
        }

        public override void OnStartPlay()
        {
            if (setFromOnPlay)
            {
                target.alpha = from;
            }
        }

        //-----------------------Update Graph Node-------------------------
        public override string GetNodeTitle()
        {
            return "Canvas Group Fade";
        }

        public override string GetIconPath()
        {
            return IconsPath + "Fade.png";
        }
    }
}