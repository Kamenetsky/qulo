﻿using AnimationCreator.Core;
using DG.Tweening;
using UnityEngine;

namespace AnimationCreator.Animations
{
    [System.Serializable]
    public class MoveTween : TweenData
    {
        public enum TransformType
        {
            Transform,
            RectTransform
        }
        //-----------------------References-------------------------
        [Header("Move Tween")]
        [SerializeField] private TransformType transformType = TransformType.Transform;
        [SerializeField,CheckForNull] private Transform target;
        [SerializeField] private Vector3 from = Vector3.zero;
        [SerializeField] private Vector3 to = Vector3.one;
        [SerializeField] private bool local = true;

        private RectTransform _rectTransform;
        
        public override void Initialize(AnimationPlayer player)
        {
            base.Initialize(player);
            if (transformType == TransformType.RectTransform)
            {
                if(target is RectTransform) _rectTransform = target as RectTransform;
            }
        }

        public override Tween Animate()
        {
            if (_rectTransform != null)
            {
                return _rectTransform.DOAnchorPos3D(to, duration).SetEase(ease).From(from, setFromImmediately).SetDelay(delay)
                    .SetUpdate(GetTimeIndependent());
            }

            if (local)
            {
                return target.DOLocalMove(to, duration).SetEase(ease).From(from, setFromImmediately)
                    .SetDelay(delay).SetUpdate(GetTimeIndependent());
            }
            else
            {
                return target.DOMove(to, duration).SetEase(ease).From(from, setFromImmediately)
                    .SetDelay(delay).SetUpdate(GetTimeIndependent());
            }
        }
        
        public override void OnStartPlay()
        {
            if (setFromOnPlay)
            {
                target.position = from;
            }
        }
        
        //-----------------------Update Graph Node-------------------------
        public override string GetNodeTitle()
        {
            return "Move Tween";
        }

        public override string GetTitleExtra()
        {
            if (local)
            {
                return " Local";
            }
            return "";
        }

        public override string GetIconPath()
        {
            return IconsPath + "Move.png";
        }
    }
}