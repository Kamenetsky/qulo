﻿using AnimationCreator.Core;
using DG.Tweening;
using UnityEngine;

namespace AnimationCreator.Animations
{
    [System.Serializable]
    public class PunchScaleTween : TweenData
    {
        //-----------------------References-------------------------
        [Header("Punch Scale Tween")]
        [SerializeField,CheckForNull] private Transform target;
        [SerializeField] private Vector3 from = Vector3.one;
        [SerializeField] private Vector3 punch = Vector3.one * .1f;
        [SerializeField] private int vibrato = 10;
        [SerializeField] private float elasticity = 1f;
        
        public override Tween Animate()
        {
            if (setFromOnPlay)
            {
                target.localScale = from;
            }
            return target.DOPunchScale(punch,duration,vibrato,elasticity).SetEase(ease).SetEase(ease)
                .SetDelay(delay).SetUpdate(GetTimeIndependent());
        }

        //-----------------------Update Graph Node-------------------------
        public override string GetNodeTitle()
        {
            return "Punch Scale";
        }

        public override string GetIconPath()
        {
            return IconsPath + "Scale.png";
        }
    }
}