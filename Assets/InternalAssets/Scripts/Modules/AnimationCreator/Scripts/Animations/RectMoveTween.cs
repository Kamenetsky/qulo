﻿using AnimationCreator.Core;
using DG.Tweening;
using UnityEngine;

namespace AnimationCreator.Animations
{
    [System.Serializable]
    public class RectMoveTween : TweenData
    {
        //-----------------------References-------------------------
        [Header("Rect Move Tween")]
        [SerializeField,CheckForNull] private RectTransform target;
        [SerializeField] private Vector3 from = Vector3.zero;
        [SerializeField] private Vector3 to = Vector3.one;
        
        public override Tween Animate()
        {
            return target.DOAnchorPos3D(to, duration).SetEase(ease).From(from, setFromImmediately).SetEase(ease)
                .SetDelay(delay).SetUpdate(GetTimeIndependent());
        }

        
        public override void OnStartPlay()
        {
            if (setFromOnPlay)
            {
                target.anchoredPosition3D = from;
            }
        }

        
        //-----------------------Update Graph Node-------------------------
        public override string GetNodeTitle()
        {
            return "Rect Move";
        }

        public override string GetIconPath()
        {
            return IconsPath + "Move.png";
        }
    }
}