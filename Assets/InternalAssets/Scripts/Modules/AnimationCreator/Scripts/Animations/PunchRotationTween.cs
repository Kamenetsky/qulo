﻿using AnimationCreator.Core;
using DG.Tweening;
using UnityEngine;

namespace AnimationCreator.Animations
{
    [System.Serializable]
    public class PunchRotationTween : TweenData
    {
        //-----------------------References-------------------------
        [Header("Punch Rotation Tween")]
        [SerializeField,CheckForNull] private Transform target;
        [SerializeField] private Vector3 from = Vector3.zero;
        [SerializeField] private Vector3 punch = Vector3.one;
        [SerializeField] private int vibrato = 10;
        [SerializeField] private float elasticity = 1f;
        
        public override Tween Animate()
        {
            if (setFromOnPlay)
            {
                target.rotation = Quaternion.Euler(from);
            }
            return target.DOPunchRotation(punch,duration,vibrato,elasticity).SetEase(ease).SetEase(ease)
                .SetDelay(delay).SetUpdate(GetTimeIndependent());
        }

        //-----------------------Update Graph Node-------------------------
        public override string GetNodeTitle()
        {
            return "Punch Rotation";
        }

        public override string GetIconPath()
        {
            return IconsPath + "Rotate.png";
        }
    }
}