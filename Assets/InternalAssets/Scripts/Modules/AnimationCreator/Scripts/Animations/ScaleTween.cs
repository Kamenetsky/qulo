﻿using AnimationCreator.Core;
using DG.Tweening;
using UnityEngine;

namespace AnimationCreator.Animations
{
    [System.Serializable]
    public class ScaleTween : TweenData
    {
        //-----------------------References-------------------------
        [Header("Scale Tween")]
        [SerializeField,CheckForNull] private Transform target;
        [SerializeField] private Vector3 from = Vector3.zero;
        [SerializeField] private Vector3 to = Vector3.one;
        
        public override Tween Animate()
        {
            return target.DOScale(to, duration).SetEase(ease).From(from, setFromImmediately).SetEase(ease)
                .SetDelay(delay).SetUpdate(GetTimeIndependent());
        }

        public override void OnStartPlay()
        {
            if (setFromOnPlay)
            {
                target.localScale = from;
            }
        }

        
        //-----------------------Update Graph Node-------------------------
        public override string GetNodeTitle()
        {
            return "Scale";
        }

        public override string GetIconPath()
        {
            return IconsPath + "Scale.png";
        }
    }
}