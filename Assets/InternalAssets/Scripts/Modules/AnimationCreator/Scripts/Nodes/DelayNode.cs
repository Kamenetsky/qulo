﻿using System.Collections;
using AnimationCreator.Core;
using UnityEngine;

namespace AnimationCreator.Nodes
{
    [System.Serializable]
    public class DelayNode : BaseNode
    {
        [SerializeField,HideInInspector,NodePort("Enter")] protected NodePortOutput enterPort;
        [SerializeField,HideInInspector,NodePort("")] protected NodePortInput input;
        [SerializeField] private bool timeIndependent = false;
        [SerializeField] private float duration = .5f;

        private Coroutine _coroutine;
        
        public override NodePortOutput GetMainOutput()
        {
            return enterPort;
        }

        public override float GetLength()
        {
            return duration;
        }

        public override void EnterNode(string name)
        {
            base.EnterNode(name);
            _coroutine = Player.StartCoroutine(IEDelay());
        }

        public override void ClearNode()
        {
            base.ClearNode();
            if(_coroutine != null) Player.StopCoroutine(_coroutine);
        }

        private IEnumerator IEDelay()
        {
            if (timeIndependent)
            {
                yield return new WaitForSecondsRealtime(duration);
            }
            else
            {
                yield return new WaitForSeconds(duration);
            }

            PerformOutputPort(enterPort);
        }
        
        public override string GetIconPath()
        {
            return IconsPath + "Auto.png";
        }

        public override string GetNodeClass()
        {
            return "event-node";
        }

        public override string GetNodeTitle()
        {
            return "Delay";
        }

        public override string GetTitleExtra()
        {
            return " " + duration + "s";
        }
    }
}