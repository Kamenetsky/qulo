﻿using AnimationCreator.Core;
using UnityEngine;
using UnityEngine.Events;

namespace AnimationCreator.Nodes
{
    [System.Serializable]
    public class UnityEventNode : BaseNode
    {
        [SerializeField,HideInInspector,NodePort("Enter")] protected NodePortInput enterPort;
        [SerializeField] private UnityEvent onEnterEvent;

        public UnityEventNode()
        {
            onEnterEvent = new UnityEvent();
        }
        
        public override void EnterNode(string name)
        {
            base.EnterNode(name);
            onEnterEvent?.Invoke();
        }

        public override NodePortOutput GetMainOutput()
        {
            return null;
        }
        
        public override string GetIconPath()
        {
            return IconsPath + "Event.png";
        }

        public override string GetNodeClass()
        {
            return "node-event";
        }

        public override string GetNodeTitle()
        {
            return "Unity Event";
        }

    }
}