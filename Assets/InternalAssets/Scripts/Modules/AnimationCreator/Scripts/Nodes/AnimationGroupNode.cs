﻿using System.Collections.Generic;
using AnimationCreator.Core;
using UnityEngine;

namespace AnimationCreator.Nodes
{
    [System.Serializable]
    public class AnimationGroupNode : BaseNode
    {
        [SerializeField,HideInInspector,NodePort("Enter")] protected NodePortOutput enterPort;
        [SerializeField,HideInInspector,NodePort("Complete")] protected NodePortOutput completePort;
        [SerializeField,HideInInspector,NodePort("")] protected NodePortInput input;
        [SerializeField] private string eventName = "Play";
        [SerializeField] private bool timeIndependent = false;

        private List<TweenData> _allTweens;
        private List<TweenData> _currentlyPlayingTweens = new List<TweenData>();

        //Getter
        public bool TimeIndependent => timeIndependent;

        public override void Initialize(AnimationPlayer player)
        {
            _allTweens = new List<TweenData>();
            base.Initialize(player);
            Player.OnPlayEvent += OnEvent;
        }
        
        public void CompletePlayTweenNode(TweenData tweenData)
        {
            if (_currentlyPlayingTweens.Contains(tweenData))
            {
                _currentlyPlayingTweens.Remove(tweenData);
                if (_currentlyPlayingTweens.Count <= 0)
                {
                    PerformOutputPort(completePort);
                    Player.OnCompleteAnimationGroup(this);
                }
            }
        }

        public override NodePortOutput GetMainOutput()
        {
            return enterPort;
        }
        
        public override void OnInitializeCompleted()
        {
            foreach (var s in enterPort.ConnectionData)
            {
                if (Player.GetNodeByID(s,out BaseNode node))
                {
                    node.GetAllTweens(ref _allTweens);
                }
            }

            foreach (var tweenData in _allTweens)
            {
                tweenData.SetParentGroupNode(this);
            }
        }

        private void OnEvent(string obj)
        {
            if (obj == eventName)
            {
                EnterNode("Forward");
            }
        }

        public override void EnterNode(string name)
        {
            _currentlyPlayingTweens = new List<TweenData>();
            foreach (var tweenData in _allTweens)
            {
                tweenData.OnStartPlay();
                _currentlyPlayingTweens.Add(tweenData);
            }

            PerformOutputPort(enterPort);
        }

        public override string GetIconPath()
        {
            return IconsPath + "Enter.png";
        }

        public override string GetNodeClass()
        {
            return "node-enter";
        }

        public override string GetSearchName()
        {
            return "Animation Clip";
        }

        public override string GetNodeTitle()
        {
            return "";
        }

        public override string GetTitleExtra()
        {
            return " " + eventName;
        }

    }
}