﻿using AnimationCreator.Core;
using UnityEngine;

namespace AnimationCreator.Nodes
{
    [System.Serializable]
    public class DebugNode : BaseNode
    {
        [SerializeField,HideInInspector,NodePort("")] protected NodePortInput enterPort;
        [SerializeField] private string value = "Debug";

        public override void EnterNode(string name)
        {
            base.EnterNode(name);
            Debug.Log(value);
        }
        
        public override NodePortOutput GetMainOutput()
        {
            return null;
        }

        public override string GetIconPath()
        {
            return IconsPath + "Restart.png";
        }

        public override string GetNodeClass()
        {
            return "node-enter";
        }

        public override string GetNodeTitle()
        {
            return "Debug";
        }

    }
}