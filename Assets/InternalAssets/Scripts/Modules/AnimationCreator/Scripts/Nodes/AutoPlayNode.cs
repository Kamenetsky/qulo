﻿using AnimationCreator.Core;
using UnityEngine;

namespace AnimationCreator.Nodes
{
    [System.Serializable]
    public class AutoPlayNode : BaseNode
    {
        [SerializeField,HideInInspector,NodePort("Enter")] protected NodePortOutput enterPort;

        public override void OnInitializeCompleted()
        {
            PerformOutputPort(enterPort);
        }

        public override int GetPriority()
        {
            return -10;
        }

        public override NodePortOutput GetMainOutput()
        {
            return enterPort;
        }

        public override string GetIconPath()
        {
            return IconsPath + "Auto.png";
        }

        public override string GetNodeClass()
        {
            return "auto-node";
        }

        public override string GetNodeTitle()
        {
            return "Auto Start";
        }

        public override string GetSearchName()
        {
            return "Auto Play";
        }

        public override string GetTitleExtra()
        {
            return " ";
        }
    }
}