﻿using AnimationCreator.Core;
using UnityEngine;

namespace AnimationCreator.Nodes
{
    [System.Serializable]
    public class TriggerPlayEvent : BaseNode
    {
        [SerializeField,HideInInspector,NodePort("Enter")] protected NodePortInput enterPort;
        [SerializeField] private string eventName = "Play";

        public override void EnterNode(string name)
        {
            base.EnterNode(name);
            Player.Play(eventName);
        }
        
        public override NodePortOutput GetMainOutput()
        {
            return null;
        }

        public override string GetIconPath()
        {
            return IconsPath + "Restart.png";
        }

        public override string GetNodeClass()
        {
            return "node-enter";
        }

        public override string GetSearchName()
        {
            return "Trigger Event";
        }

        public override string GetNodeTitle()
        {
            return "Trigger => ";
        }

        public override string GetTitleExtra()
        {
            return " " + eventName;
        }
    }
}