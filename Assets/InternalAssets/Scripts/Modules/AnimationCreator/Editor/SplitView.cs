﻿using UnityEngine;
using UnityEngine.UIElements;

namespace AnimationCreator.Editor
{
    public class SplitView : TwoPaneSplitView
    {
        public new class UxmlFactory : UxmlFactory<SplitView, TwoPaneSplitView.UxmlTraits> {}
        
        public SplitView()
        {
            
        }
    }
}