﻿using System.Linq;
using AnimationCreator.Core;
using UnityEditor;
using UnityEngine;
using UnityEngine.UIElements;

namespace AnimationCreator.Editor.Graph
{
    public class GraphInspector : VisualElement
    {
        public void Cancel()
        {
            Clear();
        }
        public void DrawNode(AnimationGraphWindow animationGraphWindow,AnimationPlayer animationPlayer,NodeView nodeView)
        {
            Clear();
            if(animationPlayer == null) return;
            if(animationGraphWindow == null) return;
            if(nodeView == null) return;
            var serializedObject = new SerializedObject(animationPlayer);
            var nodeProperty = serializedObject.FindProperty("nodes");
            
            var imguiContainer = new IMGUIContainer(() =>
            {
                serializedObject.Update();
                var currentNode = animationPlayer.GetNodes().FirstOrDefault(x => x.ID == nodeView.Node.ID);
                var indexOf = animationPlayer.GetNodes().IndexOf(currentNode);

                var property = nodeProperty.GetArrayElementAtIndex(indexOf);
                property.isExpanded = true;
                EditorGUILayout.Separator();
                EditorGUILayout.PropertyField(property,true);
                EditorGUILayout.Space(10);
                if (serializedObject.ApplyModifiedProperties())
                {
                    animationGraphWindow.RequestUpdate();
                }
            });
            Add(imguiContainer);
        }
        
    }
}