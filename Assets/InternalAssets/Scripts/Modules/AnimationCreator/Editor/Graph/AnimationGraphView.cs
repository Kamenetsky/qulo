﻿using System;
using System.Collections.Generic;
using AnimationCreator.Core;
using UnityEditor;
using UnityEditor.Experimental.GraphView;
using UnityEngine;
using UnityEngine.UIElements;

namespace AnimationCreator.Editor.Graph
{
    public class AnimationGraphView : GraphView
    {
        private readonly AnimationGraphWindow _window;
        private readonly AnimationPlayer _player;
        private readonly Dictionary<string, NodeView> _allNodesViewDictionary = new Dictionary<string, NodeView>();
        private readonly List<NodeView> _allNodesView = new List<NodeView>();
        
        private AnimationGraphSearchWindow _searchWindow;

        public AnimationGraphView(AnimationGraphWindow window, AnimationPlayer player)
        {
            //Ref
            _window = window;
            _player = player;

            //Setup
            AddGridBackground();
            AddManipulators();
            AddSearchWindow();
            RefreshGraph();
        }
        
        #region Setup

        public void RefreshGraph()
        {
            foreach (var graphElement in graphElements)
            {
                RemoveElement(graphElement);

            }
            
            _allNodesView.Clear();
            _allNodesViewDictionary.Clear();
            
            graphViewChanged -= OnGraphViewChanged;
            
            var list = _player.GetNodes();
            foreach (var node in list)
            {
                CreateNodeView(node);
            }
            
            foreach (var node in _allNodesView)
            {
                node.SetupConnections();
            }
            
            graphViewChanged += OnGraphViewChanged;
        }
        
        private void AddGridBackground()
        {
            var gridBackground = new GridBackground();
            gridBackground.StretchToParentSize();
            Insert(0,gridBackground);
        }

        private void AddManipulators()
        {
            SetupZoom(ContentZoomer.DefaultMinScale,ContentZoomer.DefaultMaxScale);
            this.AddManipulator(new SelectionDragger());
            this.AddManipulator(new RectangleSelector());
            this.AddManipulator(new ContentDragger());
        }
        
        private void AddSearchWindow()
        {
            if (_searchWindow == null)
            {
                _searchWindow = ScriptableObject.CreateInstance<AnimationGraphSearchWindow>();
                
                _searchWindow.Initialize(this);
            }

            nodeCreationRequest = context =>
                SearchWindow.Open(new SearchWindowContext(context.screenMousePosition), _searchWindow);
        }

        #endregion

        #region Nodes Control
        
        public bool TryGetNodeByGuidName(string nodeName, out NodeView nodeView)
        {
            return _allNodesViewDictionary.TryGetValue(nodeName, out nodeView);
        }
        
        private NodeView CreateNodeView(BaseNode nodeData)
        {
            NodeView nodeView = null;

            nodeView = new NodeView(_window,this,_player,nodeData);
            nodeView?.Setup();

            _allNodesViewDictionary.TryAdd(nodeData.ID,nodeView);
            _allNodesView.Add(nodeView);
            AddElement(nodeView);
            return nodeView;
        }

        public void CreateNewBaseNode(Type nodeDataType,Vector2 pos)
        {
            var newNodeData = (BaseNode)Activator.CreateInstance(nodeDataType);
            newNodeData.GenerateIdIfNotExisted();
            newNodeData.NodeName = newNodeData.GetNodeTitle();
            newNodeData.NodePosition = pos;
            
            Undo.RecordObject(_player,"Create New Node");
            _player.TryAddNode(newNodeData);
            EditorUtility.SetDirty(_player);

            //Create Node View
            _window.RequestUpdate();
        }
        
        #endregion
        
        #region Graph Control

        public void RefreshGraphData()
        {
            foreach (var nodeView in _allNodesView)
            {
                nodeView.RefreshData();
            }

            if (_player != null)
            {
                Selection.activeGameObject = _player.gameObject;
            }
        }
        
        private GraphViewChange OnGraphViewChanged(GraphViewChange graphViewChange)
        {
            var nodesToRemoveFromRemoveList = new List<NodeView>();
            
            
            foreach (var nodeView in nodesToRemoveFromRemoveList)
            {
                if (graphViewChange.elementsToRemove != null) graphViewChange.elementsToRemove.Remove(nodeView);
            }
            
            if (graphViewChange.edgesToCreate != null)
            {
                graphViewChange.edgesToCreate.ForEach(edge =>
                {
                    if(edge.output.node is not NodeView parentNodeView) return;
                    parentNodeView.OnConnectEdge(edge);
                });
            }

            
            if (graphViewChange.elementsToRemove != null)
            {
                graphViewChange.elementsToRemove.ForEach(elem =>
                {
                    if (elem is Edge edge)
                    {
                        var outputNode = (NodeView)edge.output.node;
                        outputNode.ClearEdge(edge);
                    }
                    if (elem is NodeView node)
                    {
                        if (!node.DeleteNode())
                        {
                            nodesToRemoveFromRemoveList.Add(node);
                        }
                    }

                });
                _window.RequestUpdate();
            }

            return graphViewChange;
        }
        
        public override List<Port> GetCompatiblePorts(Port startPort, NodeAdapter nodeAdapter)
        {
            var compatiblePorts = new List<Port>();

            ports.ForEach(port =>
            {
                if(startPort == port) return;
                if(startPort.node == port.node) return;
                if(startPort.direction == port.direction) return;
                compatiblePorts.Add(port);
            });
            
            return compatiblePorts;
        }

        #endregion
        
        #region Utility

        public Vector2 GetLocalMousePosition(Vector2 mousePos,bool isSearchWindow = false)
        {
            var worldMousePos = mousePos;

            if (isSearchWindow)
            {
                worldMousePos -= _window.position.position;
            }
            var localMousePos = contentViewContainer.WorldToLocal(worldMousePos);
            return localMousePos;
        }

        #endregion



    }
}