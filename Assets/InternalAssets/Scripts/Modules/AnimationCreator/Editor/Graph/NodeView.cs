﻿using System.Collections.Generic;
using System.Reflection;
using AnimationCreator.Core;
using UnityEditor;
using UnityEditor.Experimental.GraphView;
using UnityEngine;
using UnityEngine.UIElements;

namespace AnimationCreator.Editor.Graph
{
    public class NodeView : Node
    {
        private Dictionary<string, Port> Ports { get; }
        private Dictionary<string, NodePort> AllNodePorts { get; }
        private Dictionary<string, NodePortInput> AllNodePortsInput { get; }
        private Dictionary<string,NodePortOutput> AllNodePortsOutput { get; }
        
        private readonly AnimationGraphWindow _graphWindow;
        private readonly AnimationGraphView _graphView;
        private readonly AnimationPlayer _player;
        private readonly BaseNode _node;
        
        //-----------------GETTER--------------------
        public Port EnterPort { get; private set; }
        public BaseNode Node => _node;


        public NodeView(AnimationGraphWindow window, AnimationGraphView graphView,AnimationPlayer player,BaseNode node)
            : base("Assets/InternalAssets/Scripts/Modules/AnimationCreator/Editor/NodeView.uxml")
        {
            Ports = new Dictionary<string, Port>();
            AllNodePorts = new Dictionary<string, NodePort>();
            AllNodePortsInput = new Dictionary<string, NodePortInput>();
            AllNodePortsOutput = new Dictionary<string, NodePortOutput>();
            
            
            _graphWindow = window;
            _graphView = graphView;
            _player = player;
            _node = node;

            name = node.ID;
            title = node.NodeName + node.GetTitleExtra();
            
            style.left = node.NodePosition.x;
            style.top = node.NodePosition.y;
            
            if (!string.IsNullOrEmpty(node.GetNodeClass()))
            {
                AddToClassList(node.GetNodeClass());
            }
            
            string iconPath = node.GetIconPath();
            if (!string.IsNullOrEmpty(iconPath))
            {
                titleContainer.Q<VisualElement>("Icon").style.backgroundImage =
                    new StyleBackground(AssetDatabase.LoadAssetAtPath<Texture2D>(iconPath));
            }

            var hasNullValues = false;
                
            var type = node.GetType();
            var fields = type.GetFields(BindingFlags.Public | BindingFlags.Instance | BindingFlags.NonPublic);
            foreach (var field in fields)
            {
                var attribute = (CheckForNullAttribute)field.GetCustomAttribute(typeof(CheckForNullAttribute));
                if (attribute != null)
                {
                    var value = field.GetValue(node);
                    if ((Object)value == null)
                    {
                        hasNullValues = true;
                        break;
                    }
                }
                    
            }
            
            titleContainer.Q<VisualElement>("NullValue").style.display = hasNullValues? DisplayStyle.Flex : DisplayStyle.None;

            
            FindNodePorts();
            CreatePorts();
            _node.OnIsPlayingChanged += OnIsPlayingChanged;
        }

        private void OnIsPlayingChanged(bool obj)
        {
            if (obj)
            {
                AddToClassList("IsPlaying");
            }
            else
            {
                RemoveFromClassList("IsPlaying");
            }
        }

        #region Setup

        
        public void RefreshData()
        {
            title = _node.NodeName + _node.GetTitleExtra();

            var hasNullValues = false;
            var type = _node.GetType();
            var fields = type.GetFields(BindingFlags.Public | BindingFlags.Instance | BindingFlags.NonPublic);
            foreach (var field in fields)
            {
                var attribute = (CheckForNullAttribute)field.GetCustomAttribute(typeof(CheckForNullAttribute));
                if (attribute != null)
                {
                    var value = field.GetValue(_node);
                    if ((Object)value == null)
                    {
                        hasNullValues = true;
                        break;
                    }
                }
                    
            }
            
            titleContainer.Q<VisualElement>("NullValue").style.display = hasNullValues? DisplayStyle.Flex : DisplayStyle.None;

        }
        
        public void Setup()
        {

        }
        
        private void FindNodePorts()
        {
            var parentType = _node.GetType();
            var fields = parentType.GetFields(BindingFlags.Public | BindingFlags.Instance | BindingFlags.NonPublic);
            foreach (var field in fields)
            {
                var nodePortAttribute = (NodePortAttribute)field.GetCustomAttribute(typeof(NodePortAttribute));
                if (nodePortAttribute != null)
                {
                    var nodePort = (NodePort)field.GetValue(_node);
                    if(nodePort == null) continue;
                    nodePort.capacity = nodePortAttribute.Capacity;
                    nodePort.portName = nodePortAttribute.MainName;

                    AllNodePorts.TryAdd(nodePort.id,nodePort);

                    if (nodePort is NodePortInput nodePortInput)
                    {
                        AllNodePortsInput.TryAdd(nodePortInput.id,nodePortInput);
                    }
                    if (nodePort is NodePortOutput nodePortOutput)
                    {
                        AllNodePortsOutput.TryAdd(nodePortOutput.id,nodePortOutput);
                    }
                }
            }
        }

        private void CreatePorts()
        {
            foreach (var portInput in AllNodePortsInput.Values)
            {
                EnterPort = CreatePort(portInput.portName,portInput.id, Direction.Input,
                    portInput.capacity == NodePortCapacity.Multi? Port.Capacity.Multi : Port.Capacity.Single);
            }
            foreach (var portOutput in AllNodePortsOutput.Values)
            {
                CreatePort(portOutput.portName,portOutput.id, Direction.Output, 
                    portOutput.capacity == NodePortCapacity.Multi? Port.Capacity.Multi : Port.Capacity.Single);
            }
        }

        #endregion

        #region Control Node View


        public override void OnSelected()
        {
            base.OnSelected();
            if (_player == null)
            {
                return;
            }
            //Selection.activeGameObject = _player.gameObject;
            Undo.RecordObject(_player,"Select New Node");
            _player.CurrentSelectedID = _node.ID;
            _graphWindow.OnCurrentNodeChanged(this);
            EditorUtility.SetDirty(_player);
            contentContainer.Q<VisualElement>("border2").AddToClassList("selected-node");
        }

        public override void OnUnselected()
        {
            base.OnUnselected();

            contentContainer.Q<VisualElement>("border2").RemoveFromClassList("selected-node");
        }

        public override void SetPosition(Rect newPos)
        {
            base.SetPosition(newPos);
            Undo.RecordObject(_player,"Set new Position");
            _node.NodePosition = newPos.position;
            EditorUtility.SetDirty(_player);
        }

        #endregion
        
        #region Control Ports

        public bool TryGetNodePort(string portID,out NodePort nodePort)
        {
            return AllNodePorts.TryGetValue(portID, out nodePort);
        }
        
        public bool TryGetPort(string portID,out Port port)
        {
            return Ports.TryGetValue(portID, out port);
        }

        private Port CreatePort(string portName,string portId, Direction direction,Port.Capacity capacity)
        {
            var port = InstantiatePort(Orientation.Horizontal,direction,capacity,typeof(bool));
            port.name = portId;
            port.portName = portName;
            Ports.TryAdd(portId, port);

            if (direction == Direction.Input)
            {
                inputContainer.Add(port);
            }
            else
            {
                outputContainer.Add(port);                
            }

            return port;
        }

        #endregion

        #region Control Node Main

        public void SetupConnections()
        {
            foreach (var portOutput in AllNodePortsOutput.Values)
            {
                foreach (var id in portOutput.ConnectionData)
                {
                    if (TryGetPort(portOutput.id, out var port))
                    {
                        if (_graphView.TryGetNodeByGuidName(id,out var nodeView))
                        {
                            _graphView.AddElement(port.ConnectTo(nodeView.EnterPort));
                        }
                    }
                }
            }
        }

        public bool DeleteNode()
        {
            _node.OnBeforeDelete();
            
            Undo.RecordObject(_player,"Delete Node");
            _player.TryRemoveNode(_node);
            _graphWindow.DeleteNode(this);
            return true;
        }

        public void ClearEdge(Edge edge)
        {
            var toNodeView = (NodeView)edge.input.node;
            var fromNodeView = (NodeView)edge.output.node;

            var toNodePortId = edge.input.name;
            var fromNodePortId = edge.output.name;

            TryGetNodePort(toNodePortId,out var toNodePort);
            TryGetNodePort(fromNodePortId,out var fromNodePort);

            var toNodeData = toNodeView.Node;
            var fromNodeData = fromNodeView.Node;
            
            Undo.RecordObject(_player,"Clear Node Connections");
            if (fromNodePort.TryRemoveConnection(toNodeData.ID))
            {
            }
            else
            {
                Debug.Log("Remove Connection failed");
            }
        }

        public void OnConnectEdge(Edge edge)
        {
            var toNodeView = (NodeView)edge.input.node;
            var fromNodeView = (NodeView)edge.output.node;

            var toNodePortId = edge.input.name;
            var fromNodePortId = edge.output.name;

            TryGetNodePort(toNodePortId,out var toNodePort);
            TryGetNodePort(fromNodePortId,out var fromNodePort);

            var toNodeData = toNodeView.Node;
            var fromNodeData = fromNodeView.Node;
            
            Undo.RecordObject(_player,"Connect Nodes");
            if (fromNodePort.TryAddConnection(toNodeData.ID))
            {
            }
            else
            {
                Debug.Log("Connection failed");
            }
        }

        #endregion


    }
}