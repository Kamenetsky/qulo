﻿using System;
using System.Collections.Generic;
using System.Linq;
using AnimationCreator.Core;
using UnityEditor.Experimental.GraphView;
using UnityEngine;

namespace AnimationCreator.Editor.Graph
{
    public class AnimationGraphSearchWindow : ScriptableObject,ISearchWindowProvider
    {
        private AnimationGraphView _graphView;
        private Texture2D _indentationIcon;
        private List<Type> _allTypes;

        public void Initialize(AnimationGraphView graphView)
        {
            _graphView = graphView;

            _allTypes = GetAllStates();
            
            _indentationIcon = new Texture2D(1, 1);
            _indentationIcon.SetPixel(0,0,Color.clear);
            _indentationIcon.Apply();
        }
        
        private static List<Type> GetAllStates()
        {
            var types = (from domainAssembly in System.AppDomain.CurrentDomain.GetAssemblies()
                from assemblyType in domainAssembly.GetTypes()
                where (assemblyType.IsSubclassOf(typeof(BaseNode)) && !assemblyType.IsAbstract)
                select assemblyType).ToList();
            return types;
        }
        
        public List<SearchTreeEntry> CreateSearchTree(SearchWindowContext context)
        {
            var searchTreeEntries = new List<SearchTreeEntry>()
            {
                new SearchTreeGroupEntry(new GUIContent("Create"),0),
            };
            
                        
            searchTreeEntries.Add(new SearchTreeGroupEntry(new GUIContent("Main Nodes"),1));

            foreach (var type in _allTypes)
            {
                var instance = (BaseNode)Activator.CreateInstance(type);
                if(!(instance is TweenData))
                {
                    var s = new SearchTreeEntry(new GUIContent(instance.GetSearchName(), _indentationIcon))
                    {
                        level = 2,
                        userData = type,
                    };
                    searchTreeEntries.Add(s);
                }
            }

            
            searchTreeEntries.Add(new SearchTreeGroupEntry(new GUIContent("Animations"),1));
            foreach (var type in _allTypes)
            {
                var instance = (BaseNode)Activator.CreateInstance(type);
                if(instance is TweenData)
                {
                    var s = new SearchTreeEntry(new GUIContent(instance.GetSearchName(), _indentationIcon))
                    {
                        level = 2,
                        userData = type 
                    };
                    searchTreeEntries.Add(s);
                }
            }


            
            return searchTreeEntries;
        }

        public bool OnSelectEntry(SearchTreeEntry searchTreeEntry, SearchWindowContext context)
        {
            var type = (Type) searchTreeEntry.userData;
            if (type != null)
            {
                _graphView.CreateNewBaseNode(type,_graphView.GetLocalMousePosition(context.screenMousePosition,true));
            }
            return true;
        }
    }
}