﻿using AnimationCreator.Core;
using UnityEditor;
using UnityEditor.SceneManagement;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UIElements;

namespace AnimationCreator.Editor.Graph
{
    public class AnimationGraphWindow : EditorWindow
    {
        private const string TreePath = "Assets/InternalAssets/Scripts/Modules/AnimationCreator/Editor/UIGraphWindow.uxml";
        private const string StyleSheetPath = "Assets/InternalAssets/Scripts/Modules/AnimationCreator/Editor/UIGraphWindow.uss";

        
        private AnimationPlayer _player;
        private VisualElement _root;
        private AnimationGraphView _graphView;
        private GraphInspector _graphInspector;
        private bool _requestUpdate;
        private int _refreshCounter = 0;
        
        [MenuItem("Tools/Animation Graph")]
        public static AnimationGraphWindow ShowWindow()
        {
            var wnd = GetWindow<AnimationGraphWindow>();
            wnd.titleContent = new GUIContent("Animation graph");
            return wnd;
        }
        
        public void RequestUpdate()
        {
            _refreshCounter = 0;
            _requestUpdate = true;
        }

        private void OnFocus()
        {
            if (_graphView != null)
            {
                if (_player == null)
                {
                    CreateGUI();
                }
                else
                {
                    _graphView.RefreshGraphData();
                }
            }
        }

        #region Events

        private void Update()
        {
            if (_requestUpdate && _graphView != null)
            {
                _refreshCounter++;
                if (_refreshCounter < 2) return;
                _graphView.RefreshGraph();
                _requestUpdate = false;
            }
        }

        private void OnEnable()
        {
            Selection.selectionChanged += OnSelectionChanged;
            EditorApplication.playModeStateChanged += OnModeStateChanged;
            EditorSceneManager.sceneOpened += OnSceneLoad;
            Undo.undoRedoPerformed += UndoRedoPerformed;
        }

        private void OnDisable()
        {
            Selection.selectionChanged -= OnSelectionChanged;
            EditorApplication.playModeStateChanged -= OnModeStateChanged;
            EditorSceneManager.sceneOpened -= OnSceneLoad;
            Undo.undoRedoPerformed -= UndoRedoPerformed;
        }

        private void OnSceneLoad(Scene scene, OpenSceneMode mode)
        {
            CreateGUI();
        }
        
        private void UndoRedoPerformed()
        {
            RequestUpdate();
        }
        
        private void OnSelectionChanged()
        {
            if (TryGetPlayerFromSelection())
            {
                CreateGUI();
            }
        }

        #endregion

        private void OnModeStateChanged(PlayModeStateChange state)
        {
            switch (state)
            {
                case PlayModeStateChange.EnteredEditMode:
                    CreateGUI();
                    break;
                case PlayModeStateChange.ExitingEditMode:
                    CreateGUI();
                    break;
                case PlayModeStateChange.EnteredPlayMode:
                    CreateGUI();
                    break;
                case PlayModeStateChange.ExitingPlayMode:
                    CreateGUI();
                    break;
            }
        }
        
        private void ClearWindow()
        {
            rootVisualElement.Clear();
            rootVisualElement.Add(new Label("No Menu Graph selected"));
            _graphInspector?.Cancel();
        }
        
        private bool TryGetPlayerFromSelection()
        {
            if (Selection.activeGameObject != null && Selection.activeGameObject.GetComponentInParent<AnimationPlayer>(true) != null)
            {
                var newPlayer = Selection.activeGameObject.GetComponentInParent<AnimationPlayer>(true);
                if (_player != null && _player == newPlayer) return false;
                _player = newPlayer;
                return true;
            }
            return false;
        }

        public void ForceOpen(AnimationPlayer player)
        {
            _player = null;
            CreateGUI();
        }
        
        public void CreateGUI()
        {
            rootVisualElement.Clear();
            if (_player == null)
            {
                TryGetPlayerFromSelection();
            }
            
            if (_player == null)
            {
                ClearWindow();
                return;
            }

            _root = rootVisualElement;
            
            var visualTree = AssetDatabase.LoadAssetAtPath<VisualTreeAsset>(TreePath);
            var styleSheet = AssetDatabase.LoadAssetAtPath<StyleSheet>(StyleSheetPath);
            
            _root.styleSheets.Add(styleSheet);
            visualTree.CloneTree(_root);

            
            _graphView = new AnimationGraphView(this,_player);
            _graphView.styleSheets.Add(styleSheet);
            
            _graphView.StretchToParentSize();
            
            
            _root.Q<VisualElement>("Graph-Root").Add(_graphView);
            var button = _root.Q<Button>("Refresh");
            button.clicked += RequestUpdate;


            _graphInspector = new GraphInspector();
            _root.Q<VisualElement>("Inspector").Add(_graphInspector);
            _graphInspector.Cancel();
            _root.Q<VisualElement>("OwnerName").Q<Label>().text = _player.name;
        }

        public void OnCurrentNodeChanged(NodeView nodeView)
        {
            _graphInspector.DrawNode(this,_player,nodeView);
        }

        public void DeleteNode(NodeView nodeView)
        {
            _graphInspector.Cancel();
        }
    }
}