﻿using System.Linq;
using AnimationCreator.Core;
using AnimationCreator.Editor.Graph;
using UnityEditor;
using UnityEditor.UIElements;
using UnityEngine;
using UnityEngine.UIElements;

namespace AnimationCreator.Editor
{
    [CustomEditor(typeof(AnimationPlayer),true)]
    public class AnimationPlayerEditor : UnityEditor.Editor
    {
        private AnimationPlayer _player;

        private VisualElement _root;
        private VisualElement _rootData;
        private SerializedProperty _activateGameObjectOnPlayProperty;

        private void OnEnable()
        {
            _player = target as AnimationPlayer;
            if(_player == null) return;
            _activateGameObjectOnPlayProperty = serializedObject.FindProperty("activateGameObjectOnPlay");
        }

        public override VisualElement CreateInspectorGUI()
        {
            _root = new VisualElement();
            
            var openButton = new Button(() =>
            {
                var animationGraphWindow = AnimationGraphWindow.ShowWindow();
                animationGraphWindow.ForceOpen(_player);
            });

            openButton.text = "Open Graph";
            openButton.style.backgroundColor = Color.gray;
            openButton.style.borderBottomColor = Color.white;
            openButton.style.borderTopColor = Color.white;
            openButton.style.borderRightColor = Color.white;
            openButton.style.borderLeftColor = Color.white;
            openButton.style.borderBottomWidth = 1;
            openButton.style.borderTopWidth = 1;
            openButton.style.borderRightWidth = 1;
            openButton.style.borderLeftWidth = 1;
            openButton.style.unityFontStyleAndWeight = FontStyle.Bold;
            openButton.style.paddingBottom = 2;
            openButton.style.paddingTop = 2;
            _root.Add(openButton);
            
            
            _root.Add(new PropertyField(_activateGameObjectOnPlayProperty));
            _rootData = new VisualElement();
            _root.Add(_rootData);
            
            //Draw();

            return _root;
        }
    }
}