﻿
using System.Threading.Tasks;
using RTLTMPro;
using UnityEngine.Localization;
using UnityEngine.ResourceManagement.AsyncOperations;

namespace Microservices.Localization
{
    internal static class LocalizationManager
    {
        public static string GetText(LocalizedString value, object[] arguments = null)
        {
            return value.IsEmpty ? string.Empty : ProcessRTL(value.GetLocalizedString(arguments));
        }

        public static async Task<string> GetTextAsync(LocalizedString value, object[] arguments = null)
        {
            if (value.IsEmpty)
            {
                return string.Empty;
            }
            
            var loadingOperation = value.GetLocalizedStringAsync(arguments);

            await loadingOperation.Task.ConfigureAwait(false);

            if (loadingOperation.Status == AsyncOperationStatus.Succeeded)
            {
                return ProcessRTL(loadingOperation.Result);
            }
            
#if UNITY_EDITOR || ENABLE_DEV_BUILD
            UnityEngine.Debug.LogError("Could not load String Table\n" + loadingOperation.OperationException.ToString());
#endif
            
            return $"({value.TableReference}\\{value.TableEntryReference})";
        }

        public static async Task<string> GetTextAsync(LocalizedString value, string purpose, object[] arguments = null)
        {
            var loadingOperation = value.GetLocalizedStringAsync(arguments);

            await loadingOperation.Task.ConfigureAwait(false);

            if (loadingOperation.Status == AsyncOperationStatus.Succeeded)
            {
                return ProcessRTL(loadingOperation.Result);
            }
            
#if UNITY_EDITOR || ENABLE_DEV_BUILD
            UnityEngine.Debug.LogError("Could not load String Table\n" + loadingOperation.OperationException.ToString());
#endif
            
            return $"{purpose} ({value.TableReference}\\{value.TableEntryReference})";
        }
        
        private static string ProcessRTL(string localizedString)
        {
            if (TextUtils.IsRTLInput(localizedString))
            {
                var fixedString = new FastStringBuilder(RTLSupport.DefaultBufferSize);
                fixedString.Clear();
                RTLSupport.FixRTL(localizedString, fixedString, false, false, false);
                localizedString = fixedString.ToString();
            }

            return localizedString;
        }
    }
}