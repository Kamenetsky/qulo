
using System.Collections.Generic;
using SceneManagement.Scriptable;
using UnityEngine;

namespace SceneManagement
{
    internal sealed class InitializationLoader : MonoBehaviour
    {
        [Header("Settings")]
        [SerializeField] private List<SceneSO> _managersScenes = default;

        private SceneLoader _sceneLoader;
    
        // Initialization is the only scene in BuildSettings, thus it has index 0
        private const int FisrtSceneIndex = 0;
        private bool _isUnloadProcessing = false;

        private async void Awake()
        {
            // Initialization of the SceneLoader system
            _sceneLoader = SceneLoader.Instance;
            await _sceneLoader.InitializeAsync();

            StartLoad();
        }

        private async void StartLoad()
        {
            _sceneLoader.onSceneReady.OnEventRaised += OnCompleted;
                
            // Load the persistent managers scenes
            await _sceneLoader.LoadMultipleAsync(_managersScenes, false);
        }

        private void OnCompleted(string sceneName)
        {
            if (!_isUnloadProcessing)
            {
                _isUnloadProcessing = true;
                SceneLoader.Unload(FisrtSceneIndex);
            }
        }

        /*[Header("Settings")]
        [SerializeField] private List<SceneSO> _managersScenes = default;

        [Header("Broadcasting on")]
        [SerializeField] private AssetReferencee _menuLoadChannel = default;

        private void Start()
        {
            //Load the persistent managers scene
            _managersScene.sceneReference.LoadSceneAsync(LoadSceneMode.Additive, true).Completed += LoadEventChannel;
        }

        private void LoadEventChannel(AsyncOperationHandle<SceneInstance> obj)
        {
            _menuLoadChannel.LoadAssetAsync<VoidEventChannelSO>().Completed += LoadMainMenu;
        }

        private void LoadMainMenu(AsyncOperationHandle<VoidEventChannelSO> obj)
        {
            obj.Result.RaiseEvent();

            SceneManager.UnloadSceneAsync(0); //Initialization is the only scene in BuildSettings, thus it has index 0
        }*/
    }
}