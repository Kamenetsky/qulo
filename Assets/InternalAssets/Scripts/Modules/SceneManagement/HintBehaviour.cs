
using System.Collections.Generic;
using Microservices.Localization;
using TMPro;
using UnityEngine;
using UnityEngine.Localization;
using Random = UnityEngine.Random;

namespace SceneManagement.UI
{
    [RequireComponent(typeof(HintBehaviour))]
    [DisallowMultipleComponent]
    internal sealed class HintBehaviour : MonoBehaviour
    {
        [Header("Settings")]
        [SerializeField] private List<LocalizedString> _hintLocalizationKeys;

        [Header("References")]
        [SerializeField] private TextMeshProUGUI _hintText;

        private void Start() => ChangeHint();

        private void ChangeHint() => _hintText.text = GetRandomHint();

        private string GetRandomHint() => _hintLocalizationKeys.Count > 0 ? LocalizationManager.GetText(_hintLocalizationKeys[Random.Range(0, _hintLocalizationKeys.Count)]) : string.Empty;
    }
}