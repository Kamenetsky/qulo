﻿
using Core.Scriptable;
using SceneManagement.Scriptable;
using UnityEngine;
using UnityEngine.Events;

namespace SceneManagement.Events.Scriptable
{
	/// <summary>
	/// This class is used for scene-loading events.
	/// Takes a SceneSO of the location or menu that needs to be loaded, and a bool to specify if a loading screen needs to display.
	/// </summary>
	[CreateAssetMenu(fileName = "New Load Event", menuName = "Game/Events/Load Event Channel")]
	internal sealed class LoadEventChannelSO : DescriptionBaseSO
	{
		public UnityAction<SceneSO> OnLoadingRequested;

		public void RaiseEvent(SceneSO locationToLoad)
		{
			OnLoadingRequested?.Invoke(locationToLoad);
			
			if (OnLoadingRequested == null)
			{
				Debug.LogWarning("A Scene loading was requested, but nobody picked it up. " + "Check why there is no SceneLoader already present, " + "and make sure it's listening on this Load Event channel.");
			}
		}
	}
}
