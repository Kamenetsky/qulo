
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Core;
using Core.Events.Scriptable;
using Microservices.Core;
using SceneManagement.Scriptable;
using SceneManagement.UI;
using UnityEngine;
using UnityEngine.ResourceManagement.AsyncOperations;
using UnityEngine.SceneManagement;

namespace SceneManagement
{
    internal sealed class SceneLoader : Singleton<SceneLoader>
	{
		public StringEventChannelSO onSceneReady { get; private set; }
        public LoaderConfigSO config { get; private set; }

        private const string _configPath = "LoaderConfig";
        private Task _loadTask;

        private float _totalProgress = 0f;
		private float _totalMaxProgress = 0f;
        
        private GameObject _bootScreen;

		public async Task InitializeAsync()
        {
            var request =  Resources.LoadAsync(_configPath, typeof(LoaderConfigSO)).GetAwaiter();
			await request;
			
            config = request.GetResult() as LoaderConfigSO;
			
			if (config)
			{
				var handle = config.onSceneReady.LoadAssetAsync<StringEventChannelSO>();
				await handle.Task;

                onSceneReady = handle.Result;
			}
        }

        public async Task LoadSingleAsync(SceneSO scene, bool useUI)
        {
            await LoadMultipleAsync(new[] { scene }, useUI);
        }

        public async Task LoadMultipleAsync(IList<SceneSO> scenes, bool useUI)
		{
			if (_loadTask != null)
			{
				await _loadTask;
			}
			
			_loadTask = LoadAsync(scenes, useUI);

			await _loadTask;

			_loadTask = null;
		}
	
		private async Task LoadAsync(IList<SceneSO> scenes, bool useUI)
		{
			if (useUI)
			{
				//_loaderConfig.onStateChange.RaiseEvent(LocalizationManager.Localize(GameTexts.LoadingTitle));
				InitializeProgressUI(scenes.Count);
				
				await GetLoaderUI().EnableUIAsync();
			}

			foreach (var sceneToLoad in scenes)
			{
				await LoadAsync(sceneToLoad, useUI);
			}
			
			OnAsyncLoadComplete();
		}

		private async Task LoadAsync(SceneSO sceneToLoad, bool useUI)
		{
			if (sceneToLoad.sceneReference.RuntimeKeyIsValid())
			{
				var sceneReference = sceneToLoad.sceneReference;
				var handle = sceneReference.LoadSceneAsync(sceneToLoad.loadSceneMode);

				if (useUI)
				{
					await UpdateProgressUIAsync(() => handle.PercentComplete);
				}

				await handle.Task;

				//One way to handle manual scene activation.
				if (handle.Status == AsyncOperationStatus.Succeeded)
				{
					handle.Result.ActivateAsync();

					// Save loaded scenes (to be unloaded at next load request)
					var scene = handle.Result.Scene;

					SceneManager.SetActiveScene(scene);
					LightProbes.TetrahedralizeAsync();

                    onSceneReady.RaiseEvent(scene.name);

#if UNITY_EDITOR && ENABLE_DEV_BUILD
					// Set our reference to the AsyncOperationHandle (see next section)
					Debug.Log(string.Format("{0} successfully loaded.", scene.name));
#endif
				}
			}
		}

        public static void Unload(int buildId) => SceneManager.UnloadSceneAsync(buildId);
	
		public static void Unload(SceneSO scene)
		{
			if (scene != null) //would be null if the game was started in Initialisation
			{
				if (scene.sceneReference.OperationHandle.IsValid())
				{
					//Unload the scene through its AssetReference, i.e. through the Addressable system
					scene.sceneReference.UnLoadScene();
				}
#if UNITY_EDITOR
				else
				{
					//Only used when, after a "cold start", the player moves to a new scene
					//Since the AsyncOperationHandle has not been used (the scene was already open in the editor),
					//the scene needs to be unloaded using regular SceneManager instead of as an Addressable
					SceneManager.UnloadSceneAsync(scene.sceneReference.editorAsset.name);
				}
#endif
			}
		}

		private void InitializeProgressUI(int scenesCount)
		{
			_totalProgress = 0;
			_totalMaxProgress = scenesCount;
		}

		private async Task UpdateProgressUIAsync(Func<float> getPercentComplete)
		{
			if(_totalProgress < _totalMaxProgress)
			{
				var startProgress = 0f;
				var currentProgress = startProgress;
				const float currentMaxProgress = 1f;
                
                //_loaderConfig.onStateChange.RaiseEvent(string.Format(("{0}: {1}"), "Loading", scene.name));
				
				const int millisecondsDelay = 50;

				while (currentProgress < currentMaxProgress)
				{
					var currentTargetProgress = getPercentComplete() / currentMaxProgress;
					
					startProgress = currentProgress;
					currentProgress = Mathf.MoveTowards(startProgress, currentTargetProgress, config.progressAnimationMultiplier * millisecondsDelay);

					UpdateProgress(_totalProgress + currentProgress, _totalMaxProgress);

					await Task.Delay(TimeSpan.FromMilliseconds(millisecondsDelay));
				}

				_totalProgress += currentMaxProgress;
			}
		}

        private void UpdateProgress(float current, float max) =>
            config.onUpdateProgress.RaiseEvent(CalculateProgress(current, max));

		private void OnAsyncLoadComplete()
		{
			// Initialization is the only scene in BuildSettings, thus it has index 0
			//SceneManager.UnloadSceneAsync(FisrtSceneIndex);
			
			GetLoaderUI().DisableUI();
		}

		private static float CalculateProgress(float current, float max) => current / max;

		private SceneLoaderView GetLoaderUI()
		{
			if (_bootScreen == null)
			{
				_bootScreen = Instantiate(config.bootScreen, this.transform);
			}

			return _bootScreen.GetComponent<SceneLoaderView>();
		}
	}
}