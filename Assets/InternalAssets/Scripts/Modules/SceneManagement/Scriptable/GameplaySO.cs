
using UnityEngine;

namespace SceneManagement.Scriptable
{
    /// <summary>
    /// This class contains Settings specific to Gameplay scenes only
    /// </summary>

    [CreateAssetMenu(fileName = "NewGameplayScene", menuName = "Game/Scene/Gameplay")]
    internal sealed class GameplaySO : SceneSO { }
}