
using Core.Events.Scriptable;
using Core.Scriptable;
using UnityEngine;
using UnityEngine.AddressableAssets;

// CreateAssetMenu commented since we don't want to create more than one Initialisation LoaderConfigSO
namespace SceneManagement.Scriptable
{
    [CreateAssetMenu(fileName = "LoaderConfig", menuName = "Game/Loader Config")]
    internal sealed class LoaderConfigSO : DescriptionBaseSO
    {
        [Header("Settings")] 
        [SerializeField, Range(0.1f, 1f)] private float _progressAnimationMultiplier = 0.5f;

        [Header("References")]
        [SerializeField] private GameObject _bootScreen;
	
        [Header("Broadcasting on channels")]
        //[SerializeField] private BoolEventChannelSO _toggleUI = default;
        [SerializeField] private FloatEventChannelSO _onUpdateProgress = default;
        [SerializeField] private StringEventChannelSO _onStateChange = default;
        [SerializeField] private AssetReference _onSceneReady = default;

        public float progressAnimationMultiplier => _progressAnimationMultiplier;
        
        public GameObject bootScreen => _bootScreen;
        
        public FloatEventChannelSO onUpdateProgress => _onUpdateProgress;
        public StringEventChannelSO onStateChange => _onStateChange;
        public AssetReference onSceneReady => _onSceneReady;
    }
}