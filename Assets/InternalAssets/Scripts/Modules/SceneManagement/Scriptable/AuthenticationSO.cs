
using UnityEngine;

// CreateAssetMenu commented since we don't want to create more than one Auntification GameSceneSO
namespace SceneManagement.Scriptable
{
    [CreateAssetMenu(fileName = "Authentication", menuName = "Game/Scene/Authentication")]
    internal sealed class AuthenticationSO : SceneSO { }
}