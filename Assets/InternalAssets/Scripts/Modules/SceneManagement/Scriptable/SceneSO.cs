
using Core.Scriptable;
using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.SceneManagement;

namespace SceneManagement.Scriptable
{
    /// <summary>
    /// This class is a base class which contains what is common to all game scenes (Locations, Menus, Managers)
    /// </summary>
    internal class SceneSO : DescriptionBaseSO
    {
        [Header("Settings")]
        [SerializeField] protected SceneType _sceneType;
    
        [Tooltip("Used at runtime to load the scene from the right AssetBundle")]
        [SerializeField] protected AssetReference _sceneReference; 
    
        [Space(10)]
        [SerializeField] protected LoadSceneMode _loadSceneMode = LoadSceneMode.Single;

        public SceneType sceneType => _sceneType;
        public AssetReference sceneReference => _sceneReference;
        public LoadSceneMode loadSceneMode => _loadSceneMode;

        /// <summary>
        /// Used by the SceneSelector tool to discern what type of scene it needs to load
        /// </summary>
        /// <param name="Location">SceneSelector tool will also load PersistentManagers and Gameplay. Playable scene</param>
        /// <param name="Menu">SceneSelector tool will also load Gameplay. Playable scene</param>
        /// <param name="Initialisation">Special scene</param>
        /// <param name="PersistentManagers">Special scene</param>
        /// <param name="Gameplay">Special scene</param>
        /// <param name="Art">Special scene</param>
        internal enum SceneType
        {
            Location,
            Menu,
            Initialisation,
            PersistentManagers,
            Gameplay,
            Authentication,
            Art,
        }
    }
}