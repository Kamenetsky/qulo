
using UnityEngine;

// CreateAssetMenu commented since we don't want to create more than one Initialisation GameSceneSO
namespace SceneManagement.Scriptable
{
    [CreateAssetMenu(fileName = "PersistentManagers", menuName = "Game/Scene/Persistent Managers")]
    internal sealed class PersistentManagersSO : SceneSO { }
}