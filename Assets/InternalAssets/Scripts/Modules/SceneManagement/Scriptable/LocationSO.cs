
using UnityEngine;
using UnityEngine.Localization;

namespace SceneManagement.Scriptable
{
    /// <summary>
    /// This class contains Settings specific to Locations only
    /// </summary>

    [CreateAssetMenu(fileName = "NewLocation", menuName = "Game/Scene/Location")]
    internal sealed class LocationSO : SceneSO
    {
        public LocalizedString locationName;
    
        [Space(10)]
        [SerializeField] private int _id;
        [SerializeField] private string _name;
        [SerializeField] private string _version;
    }
}