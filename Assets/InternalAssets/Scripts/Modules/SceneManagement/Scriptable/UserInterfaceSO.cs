
using UnityEngine;

namespace SceneManagement.Scriptable
{
    /// <summary>
    /// This class contains Settings specific to UserInterface only
    /// </summary>

    [CreateAssetMenu(fileName = "NewUserInterface", menuName = "Game/Scene/User Interface")]
    internal sealed class UserInterfaceSO : SceneSO { }
}