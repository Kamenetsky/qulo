
using System;
using System.Threading.Tasks;
using Core.Authentication;
using Core.Backend;
using SceneManagement.Scriptable;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using Utilities.BuildVersions;
using Random = UnityEngine.Random;

namespace SceneManagement.UI
{
    [DisallowMultipleComponent]
    internal sealed class SceneLoaderView : MonoBehaviour
    {
        [Header("Settings")] 
        [SerializeField] private LoadingBackground[] _loadingBackground;
/*#if DOTWEEN_ENABLED
        [SerializeField] private AnimationSequencerController _animationSequencerController;
#endif*/

        [Header("References")] 
        [SerializeField] private Image _logoIcon;
        [SerializeField] private Image _backgroundIcon;

        [Space(10)]
        [SerializeField] private CanvasGroup _canvasGroup;
        [SerializeField] private Canvas _loadingInterface = default;
        [SerializeField] private Slider _loadingSlider;
        [SerializeField] private TextMeshProUGUI _loadingStateText;
        
        [Space(10)]
        [SerializeField] private TextMeshProUGUI _playerIdText;
        [SerializeField] private TextMeshProUGUI _gameVersionText;
        
        private LoaderConfigSO _loaderConfig;

        private void Awake()
        {
            _loaderConfig = SceneLoader.Instance.config;
            Initialize();
        }

        private void Initialize()
        {
            _canvasGroup.alpha = 0f;
            
            var styleVariation = Random.Range(0, _loadingBackground.Length);

            _logoIcon.sprite = _loadingBackground[styleVariation].logoImage;
            _backgroundIcon.sprite = _loadingBackground[styleVariation].backgroundImage;
            
            _gameVersionText.text = BuildRuntimeInfo.Instance.fullVersion;
        }

        private void OnEnable()
        {
            _loaderConfig.onUpdateProgress.OnEventRaised += OnUpdateProgress;
            _loaderConfig.onStateChange.OnEventRaised += OnStateChange;
        }
    
        private void OnDisable()
        {
            _loaderConfig.onUpdateProgress.OnEventRaised -= OnUpdateProgress;
            _loaderConfig.onStateChange.OnEventRaised -= OnStateChange;
        }

        private void OnStateChange(string text) => _loadingStateText.text = text;

        private void OnUpdateProgress(float progress) => _loadingSlider.value = progress;
    
        private void Reset() => _loadingSlider.value = 0f;

        public async Task EnableUIAsync()
        {
            _loadingInterface.enabled = true;

            await Task.Yield(); //HACK to remove warning

#if UNITY_ANDROID
            _playerIdText.gameObject.SetActive(PlayGamesPlatform.Instance.IsAuthenticated());
            if (PlayGamesPlatform.Instance.IsAuthenticated())
            {
                _playerIdText.text = PlayGamesPlatform.Instance.GetUserId();
            }
#elif UNITY_IOS
            _playerIdText.gameObject.SetActive(false);
#endif

/*#if DOTWEEN_ENABLED
            await _animationSequencerController.AsyncPlay(
                () =>
                {
                    _canvasGroup.alpha = 1f;
                },
                null
            );
#endif*/
            
            Reset();
        }

        public void DisableUI() => _loadingInterface.enabled = false;

        [Serializable]
        private struct LoadingBackground
        {
            public Sprite logoImage;
            public Sprite backgroundImage;
        }
    }
}