﻿
using System;
using System.Runtime.CompilerServices;
using UnityEngine;

namespace Core
{
    internal static class ResourceRequestExtensions
    {
        public static ResourceRequestAwaiter GetAwaiter(this ResourceRequest request) => new(request);

        internal readonly struct ResourceRequestAwaiter : INotifyCompletion
        {
            private readonly ResourceRequest _request;

            public ResourceRequestAwaiter(ResourceRequest request) => _request = request;
            public ResourceRequestAwaiter GetAwaiter() => this;
            public UnityEngine.Object GetResult() => _request.asset;
            public bool IsCompleted => _request.isDone;
            public void OnCompleted(Action action) => _request.completed += _ => action();
        }
    }
}