
using UnityEngine;

namespace Microservices.Core
{
    [DisallowMultipleComponent]
    internal abstract class Singleton<T> : MonoBehaviour where T : Component
    {
        private static T _instance;

        public static T Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = FindObjectOfType<T>();

                    if (_instance == null)
                    {
                        _instance = new GameObject { name = typeof(T).Name }.AddComponent<T>();
                    }
                
                    DontDestroyOnLoad(_instance);
                }

                return _instance;
            }
        }
    }
}