﻿
using DG.Tweening;
using UnityEngine;

namespace Utilities.TweenAnimations
{
    internal class CustomMoveAnimation : CustomAnimation
    {
        public Vector3 moveFrom;
        public Vector3 moveTo;

        public override void Play()
        {
            transform.position = moveFrom;
            transform.DOMove(moveTo, duration).SetEase(easeType);
        }
    }
}