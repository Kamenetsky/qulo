﻿
using DG.Tweening;
using UnityEngine;

namespace Utilities.TweenAnimations
{
    internal abstract class CustomAnimation : MonoBehaviour
    {
        public string animationName;
        public float duration;
        public Ease easeType;

        public abstract void Play();
    }
}