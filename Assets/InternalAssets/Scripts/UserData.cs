
[System.Serializable]
internal sealed class UserData
{
    public string userId;
    public string email;
    public string authToken;
}