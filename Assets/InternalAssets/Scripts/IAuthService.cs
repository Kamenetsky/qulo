
using Cysharp.Threading.Tasks;

internal interface IAuthService
{
    public UniTask<ProfileData> GetProfileDataAsync(string userId);
    public UniTask<bool> SavePlayerProgressAsync(string userId, int hardCurrency, int softCurrency);
}