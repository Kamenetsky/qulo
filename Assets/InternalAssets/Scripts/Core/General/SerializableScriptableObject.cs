﻿
using UnityEditor;
using UnityEngine;

namespace Core.Scriptable
{
	internal class SerializableScriptableObject : ScriptableObject
	{
		[SerializeField, HideInInspector] private string _guid;

#if UNITY_EDITOR
		private void OnValidate()
		{
			var path = AssetDatabase.GetAssetPath(this);
			_guid = AssetDatabase.AssetPathToGUID(path);
		}
#endif
	}
}
