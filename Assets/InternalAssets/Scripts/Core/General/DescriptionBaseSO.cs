﻿
using UnityEngine;

namespace Core.Scriptable
{
    /// <summary>
	/// Base class for ScriptableObjects that need a public description field.
	/// </summary>
	internal class DescriptionBaseSO : SerializableScriptableObject
	{
		[TextArea] public string description;
	}
}