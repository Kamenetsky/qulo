﻿
using System.IO;
using UnityEditor;
using UnityEngine;

namespace Core
{
    internal abstract class ResourceSingleton<T> : ScriptableObject where T : ScriptableObject
    {
        /*private static T m_Instance;
        const string AssetPath = "Assets/Resources";
 
        public static T Instance
        {
            get
            {
                if (ReferenceEquals(m_Instance, null))
                {
                    m_Instance = Resources.Load<T>(AssetPath + typeof(T).Name);
#if UNITY_EDITOR
                    if (m_Instance == null)
                    {
                        //Debug.LogError("ResourceSingleton Error: Fail load at " + "Singletons/" + typeof(T).Name);
                        CreateAsset();
                    }
                    else
                    {
                        //Debug.Log("ResourceSingleton Loaded: " + typeof (T).Name);
                    }
#endif
                    var inst = m_Instance as ResourceSingleton<T>;
                    if (inst != null)
                    {
                        inst.OnInstanceLoaded();
                    }
                }
                return m_Instance;
            }
        }
 
        public void Make() { }
        static void CreateAsset()
        {
            m_Instance = ScriptableObject.CreateInstance<T>();
            var path = Path.Combine(AssetPath, typeof(T).ToString() + ".asset");
            path = AssetDatabase.GenerateUniqueAssetPath(path);
            AssetDatabase.CreateAsset(m_Instance, path);
 
            AssetDatabase.SaveAssets();
            AssetDatabase.Refresh();
            EditorUtility.FocusProjectWindow();
            Selection.activeObject = m_Instance;
        }
 
        protected virtual void OnInstanceLoaded()
        {
        }*/
    }
}