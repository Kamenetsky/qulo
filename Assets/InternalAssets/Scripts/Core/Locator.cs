
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace Core
{
    internal sealed class Locator : Dictionary<Type, ArrayList>
    {
        private Locator() { }

        public void Add<T>(T obj)
        {
#if UNITY_EDITOR
            UnityEngine.Debug.Log("Locator.Add<" + typeof(T).FullName + ">()");
#endif
            TryAdd(typeof(T), new ArrayList());
            this[typeof(T)].Add(obj);
        }

        public T Get<T>() => (T)this[typeof(T)][this[typeof(T)].Count - 1];
        
        public IEnumerable<T> GetAll<T>() => this[typeof(T)].Cast<T>();

        public bool Contains<T>() => ContainsKey(typeof(T));

        private static Locator _instance;
        public static Locator Instance => _instance ??= new Locator();
    }
}