using System;
using System.Threading.Tasks;
using UnityEngine;

namespace Core
{
    internal static class Application
    {
        public static async void Run()
        {
            try
            {
                await Task.Yield();
            }
            catch (Exception e)
            {
                Debug.LogError($"Application run with error: {e}");
            }
        }
    }
}