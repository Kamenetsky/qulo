
using System.Collections.Generic;
using Core.Backend;
using SceneManagement;
using SceneManagement.Scriptable;
using UnityEngine;

namespace Core.Authentication
{
    [DisallowMultipleComponent]
    internal sealed class Authentication : MonoBehaviour, IInitializable
    {
        [Header("Settings")] 
        [SerializeField] private SceneSO _selfScene;
        
        [Header("Debugger Scene Settings")] 
        [SerializeField] private SceneSO _developmentPlugScene;
        [SerializeField] private SceneSO _developmentConsoleScene;
        [SerializeField] private SceneSO _developmentCheatConsoleScene;
        
        [Header("Game Manager Scenes Settings")] 
        [SerializeField] private List<SceneSO> _gameManagerScenes;

        [Header("Location Scenes Settings")] 
        [SerializeField] private List<SceneSO> _locationScenes;

        private static SceneLoader _sceneLoader => SceneLoader.Instance;

        private void Awake() => Locator.Instance.Add(this);

        public async void Initialize()
        {
            var scenesToLoad = new List<SceneSO>();

            scenesToLoad.AddRange(_gameManagerScenes);

#if UNITY_EDITOR || ENABLE_DEV_BUILD
#if !ENABLE_CREATIVES_BUILD
            scenesToLoad.Add(_developmentPlugScene);
#endif
            scenesToLoad.Add(_developmentCheatConsoleScene);
#endif

            scenesToLoad.AddRange(_locationScenes);
            
            await _sceneLoader.LoadMultipleAsync(scenesToLoad, true);
            
            SceneLoader.Unload(_selfScene);
        }
    }
}