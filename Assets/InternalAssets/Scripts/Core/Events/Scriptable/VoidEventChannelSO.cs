﻿
using Core.Scriptable;
using UnityEngine;
using UnityEngine.Events;

namespace Core.Events.Scriptable
{
	/// <summary>
	/// This class is used for Events that have no arguments (Example: Exit game event)
	/// </summary>
	[CreateAssetMenu(fileName = "NewVoidEvent", menuName = "Game/Events/Void Event Channel")]
	internal sealed class VoidEventChannelSO : DescriptionBaseSO
	{
		public UnityAction OnEventRaised;

		public void RaiseEvent() => OnEventRaised?.Invoke();
	}
}