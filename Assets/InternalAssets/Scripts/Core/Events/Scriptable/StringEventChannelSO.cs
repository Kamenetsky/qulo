
using Core.Scriptable;
using UnityEngine;
using UnityEngine.Events;

namespace Core.Events.Scriptable
{
    /// <summary>
    /// This class is used for Events that have one string argument.
    /// Example: An Loading change state, where the string is the description of state.
    /// </summary>
    [CreateAssetMenu(fileName = "NewStringEvent", menuName = "Game/Events/String Event Channel")]
    internal sealed class StringEventChannelSO : DescriptionBaseSO
    {
        public UnityAction<string> OnEventRaised;

        public void RaiseEvent(string value) => OnEventRaised?.Invoke(value);
    }
}