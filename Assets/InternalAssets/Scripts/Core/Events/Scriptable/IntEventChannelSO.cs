
using Core.Scriptable;
using UnityEngine;
using UnityEngine.Events;

namespace Core.Events.Scriptable
{
    /// <summary>
    /// This class is used for Events that have one int argument.
    /// Example: An Achievement unlock event, where the int is the Achievement ID.
    /// </summary>
    [CreateAssetMenu(fileName = "NewIntEvent", menuName = "Game/Events/Int Event Channel")]
    internal sealed class IntEventChannelSO : DescriptionBaseSO
    {
        public UnityAction<int> OnEventRaised;

        public void RaiseEvent(int value) => OnEventRaised?.Invoke(value);
    }
}
