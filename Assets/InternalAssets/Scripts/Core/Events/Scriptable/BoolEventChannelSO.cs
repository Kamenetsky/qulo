﻿
using Core.Scriptable;
using UnityEngine;
using UnityEngine.Events;

namespace Core.Events.Scriptable
{
	/// <summary>
	/// This class is used for Events that have a bool argument.
	/// Example: An event to toggle a UI interface
	/// </summary>
	[CreateAssetMenu(fileName = "NewBoolEvent", menuName = "Game/Events/Bool Event Channel")]
	internal sealed class BoolEventChannelSO : DescriptionBaseSO
	{
		public UnityAction<bool> OnEventRaised;

		public void RaiseEvent(bool value) => OnEventRaised?.Invoke(value);
	}
}