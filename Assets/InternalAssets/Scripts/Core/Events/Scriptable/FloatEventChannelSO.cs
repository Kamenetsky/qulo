﻿
using Core.Scriptable;
using UnityEngine;
using UnityEngine.Events;

namespace Core.Events.Scriptable
{
	/// <summary>
	/// This class is used for Events that have one int argument.
	/// Example: An Achievement unlock event, where the int is the Achievement ID.
	/// </summary>
	[CreateAssetMenu(fileName = "NewFloatEvent", menuName = "Game/Events/Float Event Channel")]
	internal sealed class FloatEventChannelSO : DescriptionBaseSO
	{
		public UnityAction<float> OnEventRaised;
	
		public void RaiseEvent(float value) => OnEventRaised?.Invoke(value);
	}
}