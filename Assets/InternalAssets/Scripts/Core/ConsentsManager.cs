﻿
using UnityEngine;

namespace Core.Authentication.Consents
{
    [DisallowMultipleComponent]
    internal sealed class ConsentsManager : MonoBehaviour
    {
        private void Start()
        {
            Locator.Instance.Get<Authentication>().Initialize();
        }

        private void ShowTermsOfServiceDialog()
        {

        }

        private void TermsOfServiceDialogOnClosed()
        {

        }

        private void ShowGDPRConsentDialog()
        {

        }
        
        private void GDPRConsentDialogOnClosed()
        {

        }

        private void UnityAnalyticsButtonClicked()
        {

        }
    }
}