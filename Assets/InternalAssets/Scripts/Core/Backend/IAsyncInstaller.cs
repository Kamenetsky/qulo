﻿
using Cysharp.Threading.Tasks;

namespace Core.Backend
{
    internal interface IAsyncInstaller
    {
        public UniTask InstallAsync();
    }
}