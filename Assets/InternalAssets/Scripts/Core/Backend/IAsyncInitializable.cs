﻿
using Cysharp.Threading.Tasks;

namespace Core.Backend
{
    internal interface IAsyncInitializable
    {
        public UniTask InitializeAsync();
    }
}