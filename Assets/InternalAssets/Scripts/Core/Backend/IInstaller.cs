
namespace Core.Backend
{
    internal interface IInstaller
    {
        public void Install();
    }
}