
namespace Core.Backend
{
    internal interface IInitializable
    {
        public void Initialize();
    }
}