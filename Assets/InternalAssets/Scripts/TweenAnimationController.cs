﻿
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Utilities.TweenAnimations
{
    public sealed class TweenAnimationController : MonoBehaviour
    {
        private Dictionary<string, BaseTweenAnimation> _animations = new();

        public void AddAnimation(BaseTweenAnimation animation)
        {
            if (_animations.ContainsKey(animation.AnimationName))
            {
                Debug.LogError($"Animation with the name {animation.AnimationName} already exists.");
                return;
            }

            _animations.Add(animation.AnimationName, animation);
        }

        public void RemoveAnimation(string animationName)
        {
            if (_animations.ContainsKey(animationName))
            {
                _animations.Remove(animationName);
            }
        }

        public void PlayAnimation(string animationName)
        {
            if (_animations.TryGetValue(animationName, out BaseTweenAnimation animation))
            {
                animation.Play();
            }
            else
            {
                Debug.LogError($"Animation with the name {animationName} not found.");
            }
        }

        public List<BaseTweenAnimation> GetAnimations() => _animations.Values.ToList();
    }
}