
using DG.Tweening;
using UnityEngine;

namespace Utilities.TweenAnimations
{
    internal sealed class ScaleTweenAnimation : BaseTweenAnimation
    {
        [SerializeField] private Vector3 _startScale = Vector3.one;
        [SerializeField] private Vector3 _endScale = Vector3.one * 2;

        public override void Play()
        {
            
        }

        public override Tween CreateTween(GameObject target)
        {
            return null; /*target.transform.DOScale(_endScale, Duration).SetEase(AnimationCurve).From(_startScale).SetRelative();*/
        }

        protected override Tween CreateTweenImpl(GameObject target)
        {
            return null;
        }
    }
}