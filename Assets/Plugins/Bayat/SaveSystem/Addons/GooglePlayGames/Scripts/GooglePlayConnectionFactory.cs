﻿#if UNITY_ANDROID
using System.Collections;
using System.Collections.Generic;

using GooglePlayGames.BasicApi;
using GooglePlayGames.BasicApi.SavedGame;

using UnityEngine;

namespace Bayat.SaveSystem.Storage
{

    /// <summary>
    /// The Google Play Games connection factory.
    /// </summary>
    public class GooglePlayConnectionFactory : IConnectionFactory
    {

        [RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.BeforeSceneLoad)]
        private static void Register()
        {
            ConnectionStringFactory.Register(new GooglePlayConnectionFactory());
        }

        public IStorage CreateStorage(StorageConnectionString connectionString)
        {
            if (connectionString.Prefix == "google-play" || connectionString.Prefix == "googleplay" || connectionString.Prefix == "google-play-games" || connectionString.Prefix == "googleplaygames")
            {
                string encodingName = connectionString.Get("encoding", StorageBase.DefaultTextEncodingName, "text-encoding", "encoding-name");
                bool useBase64 = connectionString.GetBoolean("usebase64", false, "use-base64");

                var storage = new GooglePlaySavedGameStorage(encodingName, useBase64);
                storage.WriteUiTitle = connectionString.Get("writeuititle", "Choose a slot to save", "write-ui-title");
                storage.ReadUiTitle = connectionString.Get("readuititle", "Choose a slot to load", "read-ui-title");
                storage.DeleteUiTitle = connectionString.Get("deleteuititle", "Choose a slot to delete", "delete-ui-title");
                storage.SavedGameOpenMode = (GooglePlaySavedGameStorage.OpenMode)connectionString.GetInteger("openmode", 0, "open-mode");
                storage.SavedGameDataSource = (DataSource)connectionString.GetInteger("datasource", 0, "data-source");
                storage.ResolutionStrategy = (ConflictResolutionStrategy)connectionString.GetInteger("conflictresolutionstrategy", 0, "conflict-resolution-strategy", "resolution-strategy");
                storage.ShowCreateSaveUI = connectionString.GetBoolean("showcreatesaveui", true, "show-create-save-ui", "show-create-save");
                storage.ShowDeleteSaveUI = connectionString.GetBoolean("showdeletesaveui", true, "show-delete-save-ui", "show-delete-save");
                storage.PrefetchDataOnConflict = connectionString.GetBoolean("prefetchdataonconflict", true, "prefetch-data-on-conflict", "prefetch");
                storage.MaxDisplayedSavedGames = (uint)connectionString.GetInteger("maxdisplayedsavedgames", 3, "max-displayed-saved-games", "max-displayed");
                return storage;
            }

            return null;
        }

    }

}
#endif